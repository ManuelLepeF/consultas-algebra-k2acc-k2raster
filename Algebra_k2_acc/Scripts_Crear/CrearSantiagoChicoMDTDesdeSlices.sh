#!/bin/bash
# Para crear Santiago Chico Tamaño MDT500 desde Slices.
for valores in 9 6 5 4 3 2 1 0
do
    echo "------------ SantiagoChicoMDT ${valores} ----"
    ./K2TreeAccum/buildfrommatriz Input/SantiagoChicoMDT/SantiagoChicoMDT_${valores}/SantiagoChicoMDT_${valores}.slices Output/SantiagoChicoMDT/SantiagoChicoMDT_${valores}/SantiagoChicoMDT_${valores} 4001 5841
    ./Experiments/TestMemory Output/SantiagoChicoMDT/SantiagoChicoMDT_${valores}/SantiagoChicoMDT_${valores}
done