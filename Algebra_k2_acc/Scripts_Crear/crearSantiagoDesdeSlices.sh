#!/bin/bash

echo "Creando Santiago Chico..."
for valores in 9 8 7 6 ${valores} 4 3 2 1 0
do
    ./K2TreeAccum/buildfrommatriz Input/RasterRealesChile/SantiagoChico/SantiagoChico_${valores}/SantiagoChico_${valores}.slices Output/RasterRealesChile/SantiagoChico/SantiagoChico_${valores}/SantiagoChico_${valores} ${valores}964 6819
    ./Experiments/TestMemory Output/RasterRealesChile/SantiagoChico/SantiagoChico_${valores}/SantiagoChico_${valores}
done

echo "Creando Santiago Mediano..."
for valores in 9 8 7 6 5 4 3 2 1 0
do
    ./K2TreeAccum/buildfrommatriz Input/RasterRealesChile/SantiagoMediano/SantiagoMediano_${valores}/SantiagoMediano_${valores}.slices Output/RasterRealesChile/SantiagoMediano/SantiagoMediano_${valores}/SantiagoMediano_${valores} 10603 11399
    ./Experiments/TestMemory Output/RasterRealesChile/SantiagoMediano/SantiagoMediano_${valores}/SantiagoMediano_${valores}
done

echo "Creando Santiago Grande..."
for valores in 9 8 7 6 5 4 3 2 1 0
do
    ./K2TreeAccum/buildfrommatriz Input/RasterRealesChile/SantiagoGrande/SantiagoGrande_${valores}/SantiagoGrande_${valores}.slices Output/RasterRealesChile/SantiagoGrande/SantiagoGrande_${valores}/SantiagoGrande_${valores} 26375 28332
    ./Experiments/TestMemory Output/RasterRealesChile/SantiagoGrande/SantiagoGrande_${valores}/SantiagoGrande_${valores}
done