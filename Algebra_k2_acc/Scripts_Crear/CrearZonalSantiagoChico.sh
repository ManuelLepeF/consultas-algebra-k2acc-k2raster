#!/bin/bash

# Para crear zonas random de tamaño de Santiago Chico con diferentes cantidades de zonas

./CreateRasterZonalAndK2TreeAcc.sh 5964 6819 10 10
./K2TreeAccum/testGetValue Output/10/10 Input/10/10.slices

./CreateRasterZonalAndK2TreeAcc.sh 5964 6819 50 50
./K2TreeAccum/testGetValue Input/50 Output/50.slices

./CreateRasterZonalAndK2TreeAcc.sh 5964 6819 100 100
./K2TreeAccum/testGetValue Output/100/100 Input/100/100.slices

./CreateRasterZonalAndK2TreeAcc.sh 5964 6819 200 200
./K2TreeAccum/testGetValue Output/200/200 Input/200/200.slices

./CreateRasterZonalAndK2TreeAcc.sh 5964 6819 500 500
./K2TreeAccum/testGetValue Output/500/500 Input/500/500.slices

./CreateRasterZonalAndK2TreeAcc.sh 5964 6819 1000 1000
./K2TreeAccum/testGetValue Output/1000/1000 Input/1000/1000.slices