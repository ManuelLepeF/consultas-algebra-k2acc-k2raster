#!/bin/bash

echo "Creando Valdivia Chico..."
for valores in 9 8 7 6 5 4 3 2 1 0
do
    echo "---------------- Valdivia Chico ${valores} ----"
    ./K2TreeAccum/buildfrommatriz Input/RasterRealesChile/ValdiviaChico/ValdiviaChico_${valores}/ValdiviaChico_${valores}.slices Output/RasterRealesChile/ValdiviaChico/ValdiviaChico_${valores}/ValdiviaChico_${valores} 5964 6819
    ./Experiments/TestMemory Output/RasterRealesChile/ValdiviaChico/ValdiviaChico_${valores}/ValdiviaChico_${valores}
done

echo "Creando Valdivia Mediano..."
for valores in 9 8 7 6 5 4 3 2 1 0
do
    echo "------------ Valdivia Mediano ${valores} ----------------------------------"
    ./K2TreeAccum/buildfrommatriz Input/RasterRealesChile/ValdiviaMediano/ValdiviaMediano_${valores}/ValdiviaMediano_${valores}.slices Output/RasterRealesChile/ValdiviaMediano/ValdiviaMediano_${valores}/ValdiviaMediano_${valores} 10603 11399
    ./Experiments/TestMemory Output/RasterRealesChile/ValdiviaMediano/ValdiviaMediano_${valores}/ValdiviaMediano_${valores}
done

echo "Creando Valdivia Grande..."
for valores in 9 8 7 6 5 4 3 2 1 0
do
    echo "---------------- Valdivia Grande ${valores} ----"
    ./K2TreeAccum/buildfrommatriz Input/RasterRealesChile/ValdiviaGrande/ValdiviaGrande_${valores}/ValdiviaGrande_${valores}.slices Output/RasterRealesChile/ValdiviaGrande/ValdiviaGrande_${valores}/ValdiviaGrande_${valores} 26375 28332
    ./Experiments/TestMemory Output/RasterRealesChile/ValdiviaGrande/ValdiviaGrande_${valores}/ValdiviaGrande_${valores}
done
