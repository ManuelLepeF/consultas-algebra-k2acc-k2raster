#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#define uint unsigned int

int findValuepoints(int points[],int numberValues ,int value){

    int min = 0;
	int max = numberValues;

	for (int  i = 0; i <= max; i++){
        if (points[i] == value){
            return i;
        }
        
    }
	return -1; //SI NO SE ENCUENTRA
}

int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}

int main(int argc, char ** argv) {
    
    if(argc < 5) {
		printf("Use: %s <Numero de filas> <Numero de columnas> <Numero de Valores distintos> <Intervalo de valores> \n", argv[0]);
		exit(1);
	}
    uint nRows = atoi(argv[1]);
	uint nCols = atoi(argv[2]);
    int nValues = atoi(argv[3]);
    int intervaloValores = atoi(argv[4]);

    // Creación del vector points
    int points[nValues];
    for (int i = 0; i < nValues; i++){
        points[i] = -1;
    }

    // Se crea o abre el archivo txt donde se guardara el raster original
    FILE *fp;
 	fp = fopen ( "raster.txt", "w");

    int aux=0;
    int indicePoints =0;
    int ** matriz = (int **)malloc (nRows*sizeof(int *));
    srand(time(NULL));
    for (int x = 0; x < nRows; x++) {
        matriz[x] =  (int *)malloc(nCols*sizeof(int *));
        for (int y = 0; y < nCols; y++) {
            if (indicePoints == nValues){
                int valor =  rand() % intervaloValores;
                int indice = findValuepoints(points,indicePoints-1,valor);
                if (indice == -1){
                    matriz[x][y] = points[rand() % indicePoints-1];
                    fprintf(fp, "%d ", matriz[x][y]);
                }else{
                    matriz[x][y] = valor;
                    fprintf(fp, "%d ", matriz[x][y]);
                }
            
            }else{
                matriz[x][y] =  rand() % intervaloValores;
                fprintf(fp, "%d ", matriz[x][y]);
                int indice = findValuepoints(points,indicePoints-1,matriz[x][y]);
                if (indice == -1){
                    points[indicePoints]=matriz[x][y];
                    indicePoints ++;
                    aux ++;
                }   
            }
        }
        fprintf(fp, "\n");
	}
    fclose (fp);
    qsort(points,nValues, sizeof(int), cmpfunc);

    for (int x = 0; x < nRows; x++) {
        for (int y = 0; y < nCols; y++) {
            matriz[x][y]=findValuepoints(points,aux,matriz[x][y]);
        }
	}


    int * pointsLast = (int *)malloc((aux+3)*sizeof(int *));
    pointsLast [0] = aux;
    pointsLast [1] = nRows;
    pointsLast [2] = nCols;

    for (int i = 0; i < nValues; i++){
        pointsLast[i+3] = points[i];
    }
    char * fooo = (char *) malloc(2048*sizeof(char));
    fooo = "raster.slices";
	FILE * fsl = fopen(fooo, "w+");

    for (int x = 0; x < nRows; x++) {
        fwrite(matriz[x], sizeof(uint), nCols, fsl);
    }

    char * namePoints = (char *) malloc(2048*sizeof(char));
    namePoints = "raster.points";
	FILE * filePoints = fopen(namePoints, "w+");
    fwrite(pointsLast, sizeof(uint), aux+3, filePoints);
    fclose(fsl);

    return 0;
}