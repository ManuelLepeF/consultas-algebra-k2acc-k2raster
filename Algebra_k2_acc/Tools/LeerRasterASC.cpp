#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/foreach.hpp>

using namespace std;

int nRows;
int nColum;
int ** matriz;
int indicePoints;
int bitDerecha;
int * vectorPointAux = (int *) malloc(0 * sizeof(int));

int compare (const void * a, const void * b){
  return ( *(int*)a - *(int*)b );
}

int findValuepoints(int numberValues ,int value){

    int min = 0;
	int max = numberValues;

	for (int  i = 0; i <= max; i++){
        if (vectorPointAux[i] == value){
            return i;
        }
        
    }
	return -1; //SI NO SE ENCUENTRA
}
void LeerAsc(string nombreAsc){
    fstream ficheroEntrada;
    string frase;
    ficheroEntrada.open ( nombreAsc.c_str() , ios::in);
    /* SE VERIFICA SI SE PUDO ABRIR EL ARCHIVO ASC */
    if (ficheroEntrada.is_open()) {
        getline (ficheroEntrada,frase);
        nColum = std::stoi(frase);
        getline (ficheroEntrada,frase);
        nRows = std::stoi(frase);
        getline (ficheroEntrada,frase);
        int NOData = std::stoi(frase);
        /* SE LLENA LA MATRIZ DE 0 PARA EVITAR VIOLACION DE SEGMENTO */
        matriz = (int **) malloc (nRows*sizeof(int *));
        for (int x = 0; x < nRows; x++) {
            matriz[x] =  (int *)malloc(nColum*sizeof(int *));
            for (int j = 0; j < nColum; j++){
                 matriz[x][j] = 0;
            }
        }
        /* LECTURA DEL ARCHIVO ASC FILA POR FILA */
        int fila = 0;
        indicePoints =0;
        while (! ficheroEntrada.eof() ) {
            getline (ficheroEntrada,frase);
            typedef std::vector<std::string> Tokens;
            Tokens tokens;
            boost::split( tokens, frase, boost::is_any_of(" ") );
            //std::cout << tokens.size() << " tokens" << std::endl;
            int columna =0; 
            BOOST_FOREACH( const std::string& i, tokens ) {
                //std::cout <<  i << std::endl;
                if ( i.compare(" ")!=0 && i.compare("")!=0 && i.compare("\n")!=0 ){
                    try {
                        int a = std::stoi(i);
                        /* CASO DE LEER UNA CELDA QUE NO SE HAY DETERMINADO EL VALOR */                 
                        if (a!=NOData){
                            a = a >> bitDerecha;
                            int indice = findValuepoints(indicePoints-1,a);
                            if ( indice== -1){
                                vectorPointAux = (int *) realloc(vectorPointAux,(indicePoints+1) * sizeof(int));
                                vectorPointAux[indicePoints] = a;
                                matriz[fila][columna] =a;
                                indicePoints ++;
                            }else{
                                matriz[fila][columna]=a;
                            }
                        }else{
                            int indice = findValuepoints(indicePoints-1,a);
                            if (indice == -1){
                                vectorPointAux = (int *) realloc(vectorPointAux,(indicePoints+1) * sizeof(int));
                                vectorPointAux[indicePoints] = 0;
                                matriz[fila][columna] =0;
                                indicePoints ++;
                            }else{
                                matriz[fila][columna] =0;
                            }
                        }
                        columna++;
                    }catch(std::exception const & e){
                        cout<<"Se intento insertar un caracter que se se puede transformar en entero: " << e.what() <<endl;
                    }
                    
                }
                
            }
            fila ++;
        }
            
    }else{
        printf("\n No se pudo abrir el raster ASC\n");
    }
    

}

void CrearGuardarPointYCrearSlicesDefinitivo(){
    qsort (vectorPointAux, indicePoints, sizeof(int), compare);
    for (int i = 0; i < nRows; i++){
        for (int j = 0; j < nColum; j++){
            matriz[i][j] = findValuepoints(indicePoints-1,matriz[i][j]);
            //printf("%d ", matriz[i][j]);
        }
    }
    int * pointsLast = (int *)malloc((indicePoints+3)*sizeof(int *));
    pointsLast [0] = indicePoints;
    pointsLast [1] = nRows;
    pointsLast [2] = nColum;

    for (int i = 0; i < indicePoints; i++){
        pointsLast[i+3] = vectorPointAux[i];
    }
    printf("Número de valores distintos en el ráster: %d\n",pointsLast[0]);
    FILE * filePoints = fopen("raster.points", "w+");
    fwrite(pointsLast, sizeof(uint), indicePoints+3, filePoints);
    printf("\nArchivo binario .points guardado con exito.\n");
    fclose(filePoints);
    free(pointsLast);
    free(vectorPointAux);
    
}

void GuardarBinarioSlices(){
    FILE * fsl = fopen("raster.slices", "w+");   
    for (int x = 0; x < nRows; x++) {
        fwrite(matriz[x], sizeof(int), nColum, fsl);
        free(matriz[x]);
    }
    free(matriz);
    fclose(fsl);
    printf("\nArchivo binario .slices guardado con exito\n");
}

int main (int argc, char ** argv) {
    bitDerecha = atoi(argv[2]);
    
    /* LEER RASTER EN ARCHIVO ASC*/
    LeerAsc(argv[1]);
    /* CREA EL .POINTS Y .SLICES FINAL */
    CrearGuardarPointYCrearSlicesDefinitivo();
    /* GUARDA ARCHIVO BINARIO .SLICES */
    GuardarBinarioSlices();

    return 0;
}