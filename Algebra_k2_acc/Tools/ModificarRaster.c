#include <stdio.h>
#include <stdlib.h>
#include "../K2TreeAccum/useK2TreeAcum.h"

int nRows;
int nColum;
int ** matriz;
int indicePoints;
int bitDerecha;
int * vectorPointAux;

int compare (const void * a, const void * b){
  return ( *(int*)a - *(int*)b );
}

int findValuepoints(int numberValues ,int value){

    int min = 0;
	int max = numberValues;

	for (int  i = 0; i <= max; i++){
        if (vectorPointAux[i] == value){
            return i;
        }
        
    }
	return -1; //SI NO SE ENCUENTRA
}

void ModificaRaster(char * nombreK2acc){
    
    //**************************************//
    //  CARGA LA ESTRUCTURA EN MEMORIA     //
    //************************************//
    printf("Cargando estructura en memoria...\n");
	SPK2TREEACUM * sp = loadk2TreeAcum(nombreK2acc);
	nRows = sp->rows;
	nColum = sp->columns;
    printf("Filas: %d\n",nRows);
    printf("Columnas: %d\n",nColum);

    //********************************************************//
    //  LEE LA MATRIZ DESDE LA K2-ACC Y MODIFICA CADA CELDA  //
    //******************************************************//
    vectorPointAux = (int *) malloc(0 * sizeof(int));
    matriz = (int **) malloc (nRows*sizeof(int *));
    for (int x = 0; x < nRows; x++){
        matriz[x] =  (int *)malloc(nColum*sizeof(int *));
        for (int y = 0; y < nColum; y++){
            int valor = getValue(sp , x, y) >> bitDerecha;
            int indice = findValuepoints(indicePoints-1,valor);
            if ( indice== -1){
                vectorPointAux = (int *) realloc(vectorPointAux,(indicePoints+1) * sizeof(int));
                vectorPointAux[indicePoints] = valor;
                matriz[x][y] =valor;
                indicePoints ++;
            }else{
                matriz[x][y]=valor;
            }
        }
        
    }
    destroyRepresentationK2Acc(sp);
    
}

void CrearGuardarPointYCrearSlicesDefinitivo(){
    qsort (vectorPointAux, indicePoints, sizeof(int), compare);
    for (int i = 0; i < nRows; i++){
        for (int j = 0; j < nColum; j++){
            matriz[i][j] = findValuepoints(indicePoints-1,matriz[i][j]);
            //printf("%d ", matriz[i][j]);
        }
    }
    int * pointsLast = (int *)malloc((indicePoints+3)*sizeof(int *));
    pointsLast [0] = indicePoints;
    pointsLast [1] = nRows;
    pointsLast [2] = nColum;

    for (int i = 0; i < indicePoints; i++){
        pointsLast[i+3] = vectorPointAux[i];
    }
    printf("Número de valores distintos en el ráster: %d\n",pointsLast[0]);
    FILE * filePoints = fopen("rasterModificado.points", "w+");
    fwrite(pointsLast, sizeof(uint), indicePoints+3, filePoints);
    printf("\nArchivo binario .points guardado con exito.\n");
    fclose(filePoints);
    free(pointsLast);
    free(vectorPointAux);
    
}

void GuardarBinarioSlices(){
    FILE * fsl = fopen("rasterModificado.slices", "w+");   
    for (int x = 0; x < nRows; x++) {
        fwrite(matriz[x], sizeof(int), nColum, fsl);
        free(matriz[x]);
    }
    free(matriz);
    fclose(fsl);
    printf("\nArchivo binario .slices guardado con exito\n");
}

int main (int argc, char ** argv) {
    if(argc != 3){
        printf("Usar: %s <ruta k2-acc del raster> <Bit de corrimiento>\n", argv[0]);
        return -1;
    }
    bitDerecha = atoi(argv[2]);
    
    /* MODIFICA RASTER DESDE K2ACC*/
    ModificaRaster(argv[1]);
    /* CREA EL .POINTS Y .SLICES FINAL */
    CrearGuardarPointYCrearSlicesDefinitivo();
    /* GUARDA ARCHIVO BINARIO .SLICES */
    GuardarBinarioSlices();

    return 0;
}