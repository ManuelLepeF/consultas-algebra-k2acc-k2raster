#!/bin/bash
echo "Probando consulta del algebra ThresHolding con los raster artificiales"
mkdir ExperimentsResults/ThresHolding
################################################################################################################################################
mkdir ExperimentsResults/ThresHolding/SantiagoChico
echo "------------------Experimentando con SantiagoChic0----------"
for valores in 2 3 4 5 6 9
do
    echo "Experimentando con SantiagoChico_${valores}"
    mkdir ExperimentsResults/ThresHolding/SantiagoChico/$valores
    /usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/RasterRealesChile/SantiagoChico/SantiagoChico_${valores}/SantiagoChico_${valores} NO 1 3 &> ExperimentsResults/ThresHolding/SantiagoChico/$valores/Algebraqueries.txt
    /usr/bin/time -v ./Experiments/TestSpeedGetValue Output/RasterRealesChile/SantiagoChico/SantiagoChico_${valores}/SantiagoChico_${valores} NO 1 3 &> ExperimentsResults/ThresHolding/SantiagoChico/$valores/GetValue_final_si.txt
done
################################################################################################################################################
mkdir ExperimentsResults/ThresHolding/SantiagoMediano
echo "------------------Experimentando con SantiagoMediano----------"
for valores in 9 7 6 5 4 2
do
    echo "Experimentando con SantiagoMediano_${valores}"
    mkdir ExperimentsResults/ThresHolding/SantiagoMediano/$valores
    /usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/RasterRealesChile/SantiagoMediano/SantiagoMediano_${valores}/SantiagoMediano_${valores} NO 1 3 &> ExperimentsResults/ThresHolding/SantiagoMediano/$valores/Algebraqueries.txt
    /usr/bin/time -v ./Experiments/TestSpeedGetValue Output/RasterRealesChile/SantiagoMediano/SantiagoMediano_${valores}/SantiagoMediano_${valores} NO 1 3 &> ExperimentsResults/ThresHolding/SantiagoMediano/$valores/GetValue_final_si.txt

done
################################################################################################################################################
mkdir ExperimentsResults/ThresHolding/SantiagoGrande
echo "------------------Experimentando con SantiagoGrande----------"
for valores in 9 7 6 5 4 3
do
    echo "Experimentando con SantiagoGrande_${valores}"
    mkdir ExperimentsResults/ThresHolding/SantiagoGrande/$valores
    /usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/RasterRealesChile/SantiagoGrande/SantiagoGrande_${valores}/SantiagoGrande_${valores} NO 1 3 &> ExperimentsResults/ThresHolding/SantiagoGrande/$valores/Algebraqueries.txt
    /usr/bin/time -v ./Experiments/TestSpeedGetValue Output/RasterRealesChile/SantiagoGrande/SantiagoGrande_${valores}/SantiagoGrande_${valores} NO 1 3 &> ExperimentsResults/ThresHolding/SantiagoGrande/$valores/GetValue_final_si.txt
done


