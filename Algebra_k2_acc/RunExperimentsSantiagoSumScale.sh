#!/bin/bash
echo "Probando consulta del algebra Sum Scale con los raster artificiales"
mkdir ExperimentsResults/SumScale
################################################################################################################################################
mkdir ExperimentsResults/SumScale/SantiagoChico
echo "------------------Experimentando con SantiagoChic0----------"
for valores in 2 3 4 5 6 9
do
    echo "Experimentando con SantiagoChico_${valores}"
    mkdir ExperimentsResults/SumScale/SantiagoChico/$valores
    /usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/RasterRealesChile/SantiagoChico/SantiagoChico_${valores}/SantiagoChico_${valores} NO 2 3 &> ExperimentsResults/SumScale/SantiagoChico/$valores/Algebraqueries.txt
    /usr/bin/time -v ./Experiments/TestSpeedGetValue Output/RasterRealesChile/SantiagoChico/SantiagoChico_${valores}/SantiagoChico_${valores} NO 2 3 &> ExperimentsResults/SumScale/SantiagoChico/$valores/GetValue_sinCrear.txt
done
################################################################################################################################################
mkdir ExperimentsResults/SumScale/SantiagoMediano
echo "------------------Experimentando con SantiagoMediano----------"
for valores in 9 7 6 5 4 2
do
    echo "Experimentando con SantiagoMediano_${valores}"
    mkdir ExperimentsResults/SumScale/SantiagoMediano/$valores
    /usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/RasterRealesChile/SantiagoMediano/SantiagoMediano_${valores}/SantiagoMediano_${valores} NO 2 3 &> ExperimentsResults/SumScale/SantiagoMediano/$valores/Algebraqueries.txt
    /usr/bin/time -v ./Experiments/TestSpeedGetValue Output/RasterRealesChile/SantiagoMediano/SantiagoMediano_${valores}/SantiagoMediano_${valores} NO 2 3 &> ExperimentsResults/SumScale/SantiagoMediano/$valores/GetValue_sinCrear.txt

done
################################################################################################################################################
mkdir ExperimentsResults/SumScale/SantiagoGrande
echo "------------------Experimentando con SantiagoGrande----------"
for valores in 9 7 6 5 4 3
do
    echo "Experimentando con SantiagoGrande_${valores}"
    mkdir ExperimentsResults/SumScale/SantiagoGrande/$valores
    /usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/RasterRealesChile/SantiagoGrande/SantiagoGrande_${valores}/SantiagoGrande_${valores} NO 2 3 &> ExperimentsResults/SumScale/SantiagoGrande/$valores/Algebraqueries.txt
    /usr/bin/time -v ./Experiments/TestSpeedGetValue Output/RasterRealesChile/SantiagoGrande/SantiagoGrande_${valores}/SantiagoGrande_${valores} NO 2 3 &> ExperimentsResults/SumScale/SantiagoGrande/$valores/GetValue_sinCrear.txt
done
