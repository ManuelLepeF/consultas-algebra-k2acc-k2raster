#!/bin/bash
mkdir ExperimentsResults/mdt500
################################################################################################################################################
echo "Probando consulta del algebra ThresHolding con el raster real mdt500"
mkdir ExperimentsResults/mdt500/ThresHolding
/usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/mdt500/mdt500 NO 1 3 &> ExperimentsResults/mdt500/ThresHolding/Algebraqueries.txt
/usr/bin/time -v ./Experiments/TestSpeedGetValue Output/mdt500/mdt500 NO 1 3 &> ExperimentsResults/mdt500/ThresHolding/GetValue.txt
#################################################################################################################################################
echo "Probando consulta del algebra Suma por un escalar con el raster real mdt500"
mkdir ExperimentsResults/mdt500/SumScale
/usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/mdt500/mdt500 NO 2 3 &> ExperimentsResults/mdt500/SumScale/Algebraqueries.txt
/usr/bin/time -v ./Experiments/TestSpeedGetValue Output/mdt500/mdt500 NO 2 3 &> ExperimentsResults/mdt500/SumScale/GetValue.txt
################################################################################################################################################
echo "Probando consulta del algebra Suma punto a punto con el raster real mdt500 y Santiago Chico"
mkdir ExperimentsResults/mdt500/SumPointPointSantiago
for valores in 9 6 5 4 3 2
do
    echo "------- Santiago chico ${valores} -------------"
    mkdir ExperimentsResults/mdt500/SumPointPointSantiago/${valores}
    for ej in 1 2 3 
    do
        echo "Ejecución: ${ej}.."
        /usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/mdt500/mdt500 Output/SantiagoChicoMDT/SantiagoChicoMDT_${valores}/SantiagoChicoMDT_${valores} 4 1 &> ExperimentsResults/mdt500/SumPointPointSantiago/${valores}/Algebraqueries_${ej}.txt
        /usr/bin/time -v ./Experiments/TestSpeedGetValue Output/mdt500/mdt500 Output/SantiagoChicoMDT/SantiagoChicoMDT_${valores}/SantiagoChicoMDT_${valores} 4 1 &> ExperimentsResults/mdt500/SumPointPointSantiago/${valores}/GetValue_${ej}.txt
    done 
done
################################################################################################################################################
echo "Probando consulta del algebra Suma Zonal con raster real mdt500 y ráster Random"
mkdir ExperimentsResults/mdt500/SumZonalRandom
for valores in 10 50 100 200 500
do
    echo "------- Raster Random ${valores} -------------"
    mkdir ExperimentsResults/mdt500/SumZonalRandom/${valores}
    for ej in 1 2 3
    do
        echo "Ejecución: ${ej}.."
        /usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/mdt500/mdt500 Output/Zonal_mdt500/${valores}/mdt500_${valores} 5 1 &> ExperimentsResults/mdt500/SumZonalRandom/${valores}/Algebraqueries_${ej}.txt
        /usr/bin/time -v ./Experiments/TestSpeedGetValue Output/mdt500/mdt500 Output/Zonal_mdt500/${valores}/mdt500_${valores} 5 1 &> ExperimentsResults/mdt500/SumZonalRandom/${valores}/GetValue_${ej}.txt
    done 
done
################################################################################################################################################
echo "Probando consulta del algebra Suma Zonal con el raster real mdt500 y Santiago Chico"
mkdir ExperimentsResults/mdt500/SumZonalSantiago
for valores in 9 6 5 4 3 2
do
    echo "------- Santiago chico ${valores} -------------"
    mkdir ExperimentsResults/mdt500/SumZonalSantiago/${valores}
    for ej in 1 2 3
    do
        echo "Ejecución: ${ej}.."
        /usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/mdt500/mdt500 Output/SantiagoChicoMDT/SantiagoChicoMDT_${valores}/SantiagoChicoMDT_${valores} 5 1 &> ExperimentsResults/mdt500/SumZonalSantiago/${valores}/Algebraqueries_${ej}.txt
        /usr/bin/time -v ./Experiments/TestSpeedGetValue Output/mdt500/mdt500 Output/SantiagoChicoMDT/SantiagoChicoMDT_${valores}/SantiagoChicoMDT_${valores} 5 1 &> ExperimentsResults/mdt500/SumZonalSantiago/${valores}/GetValue_${ej}.txt
    done 
done
