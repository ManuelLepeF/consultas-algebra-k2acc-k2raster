#!/bin/bash
echo "Probando consulta del algebra SumPointPointValdivia con los raster artificiales SantiagoValdivia"
mkdir ExperimentsResults/SumPointPointValdivia
################################################################################################################################################
mkdir ExperimentsResults/SumPointPointValdivia/SantiagoChico
echo "----------------------------Experimentando con SantiagoChico------------------------"
for Raster_1 in 9 6 5   
do
    for Raster_2 in 8 4 2 1
    do
        echo "-----Experimentando con SantiagoChico ${Raster_1}x${Raster_2}-----"
        mkdir ExperimentsResults/SumPointPointValdivia/SantiagoChico/${Raster_1}x${Raster_2} 
        for ej in 1 2 3
        do
            echo "Ejecución ${ej}"
            /usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/RasterRealesChile/SantiagoChico/SantiagoChico_${Raster_1}/SantiagoChico_${Raster_1} Output/RasterRealesChile/ValdiviaChico/ValdiviaChico_${Raster_2}/ValdiviaChico_${Raster_2} 4 1 &> ExperimentsResults/SumPointPointValdivia/SantiagoChico/${Raster_1}x${Raster_2}/Algebraqueries_${ej}.txt
            /usr/bin/time -v ./Experiments/TestSpeedGetValue Output/RasterRealesChile/SantiagoChico/SantiagoChico_${Raster_1}/SantiagoChico_${Raster_1} Output/RasterRealesChile/ValdiviaChico/ValdiviaChico_${Raster_2}/ValdiviaChico_${Raster_2} 4 1 &> ExperimentsResults/SumPointPointValdivia/SantiagoChico/${Raster_1}x${Raster_2}/GetValue_${ej}.txt
        done
    done
done
################################################################################################################################################
mkdir ExperimentsResults/SumPointPointValdivia/SantiagoMediano
echo "----------------------------Experimentando con SantiagoMediano------------------------"
for Raster_1 in 9 7 6   
do
    for Raster_2 in 9 5 3 2
    do
       
        echo "-----Experimentando con SantiagoMediano ${Raster_1}x${Raster_2}-----"
        mkdir ExperimentsResults/SumPointPointValdivia/SantiagoMediano/${Raster_1}x${Raster_2} 
        for ej in 1 2 3
        do
            echo "Ejecución ${ej}"
            /usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/RasterRealesChile/SantiagoMediano/SantiagoMediano_${Raster_1}/SantiagoMediano_${Raster_1} Output/RasterRealesChile/ValdiviaMediano/ValdiviaMediano_${Raster_2}/ValdiviaMediano_${Raster_2} 4 1 &> ExperimentsResults/SumPointPointValdivia/SantiagoMediano/${Raster_1}x${Raster_2}/Algebraqueries_${ej}.txt
            /usr/bin/time -v ./Experiments/TestSpeedGetValue Output/RasterRealesChile/SantiagoMediano/SantiagoMediano_${Raster_1}/SantiagoMediano_${Raster_1} Output/RasterRealesChile/ValdiviaMediano/ValdiviaMediano_${Raster_2}/ValdiviaMediano_${Raster_2} 4 1 &> ExperimentsResults/SumPointPointValdivia/SantiagoMediano/${Raster_1}x${Raster_2}/GetValue_${ej}.txt
        done
    done
done
################################################################################################################################################
mkdir ExperimentsResults/SumPointPointValdivia/SantiagoGrande
echo "----------------------------Experimentando con SantiagoGrande------------------------"
for Raster_1 in 9 7 6   
do
    for Raster_2 in 9 5 3 2
    do
        echo "-----Experimentando con SantiagoGrande ${Raster_1}x${Raster_2}-----"
        mkdir ExperimentsResults/SumPointPointValdivia/SantiagoGrande/${Raster_1}x${Raster_2} 
        for ej in 1 2 3
        do
            echo "Ejecución ${ej}"
            /usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/RasterRealesChile/SantiagoGrande/SantiagoGrande_${Raster_1}/SantiagoGrande_${Raster_1} Output/RasterRealesChile/ValdiviaGrande/ValdiviaGrande_${Raster_2}/ValdiviaGrande_${Raster_2} 4 1 &> ExperimentsResults/SumPointPointValdivia/SantiagoGrande/${Raster_1}x${Raster_2}/Algebraqueries_${ej}.txt
            /usr/bin/time -v ./Experiments/TestSpeedGetValue Output/RasterRealesChile/SantiagoGrande/SantiagoGrande_${Raster_1}/SantiagoGrande_${Raster_1} Output/RasterRealesChile/ValdiviaGrande/ValdiviaGrande_${Raster_2}/ValdiviaGrande_${Raster_2} 4 1 &> ExperimentsResults/SumPointPointValdivia/SantiagoGrande/${Raster_1}x${Raster_2}/GetValue_${ej}.txt
        done
    done
done
