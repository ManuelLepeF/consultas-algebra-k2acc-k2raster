#!/bin/bash

if [ $# -ne 5 ]; then
    echo "Usar: <$0>  <Numero de filas> <Numero de columnas> <Numero de Valores distintos> <Intervalo de valores> <Nombre raster>"
    exit 1
else
    ./Tools/GenerarRasterInputDos $1 $2 $3 $4
    mkdir Input/$5
    mkdir Output/$5
    mv raster.slices Input/$5/$5.slices
    mv raster.txt Input/$5/$5.txt
    mv raster.points Output/$5/$5.points
    ./K2TreeAccum/buildfrommatriz Input/$5/$5.slices Output/$5/$5 $1 $2
fi