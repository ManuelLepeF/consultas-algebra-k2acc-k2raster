#!/bin/bash
echo "Probando consulta del algebra Sum Point Point con los raster artificiales SantiagoSantiago"
mkdir ExperimentsResults/SumPointPoint
################################################################################################################################################
mkdir ExperimentsResults/SumPointPoint/SantiagoChico
echo "----------------------------Experimentando con SantiagoChico------------------------"
for Raster_1 in 9 6 5   
do
    for Raster_2 in 9 6 5 4 3 2
    do
        if [ $Raster_2 -le $Raster_1 ] 
        then
            echo "-----Experimentando con SantiagoChico ${Raster_1}x${Raster_2}-----"
            mkdir ExperimentsResults/SumPointPoint/SantiagoChico/${Raster_1}x${Raster_2} 
            for ej in 1 2 3
            do
                echo "Ejecución ${ej}"
                /usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/RasterRealesChile/SantiagoChico/SantiagoChico_${Raster_1}/SantiagoChico_${Raster_1} Output/RasterRealesChile/SantiagoChico/SantiagoChico_${Raster_2}/SantiagoChico_${Raster_2} 4 1 &> ExperimentsResults/SumPointPoint/SantiagoChico/${Raster_1}x${Raster_2}/Algebraqueries_${ej}.txt
                /usr/bin/time -v ./Experiments/TestSpeedGetValue Output/RasterRealesChile/SantiagoChico/SantiagoChico_${Raster_1}/SantiagoChico_${Raster_1} Output/RasterRealesChile/SantiagoChico/SantiagoChico_${Raster_2}/SantiagoChico_${Raster_2} 4 1 &> ExperimentsResults/SumPointPoint/SantiagoChico/${Raster_1}x${Raster_2}/GetValue_${ej}.txt
            done
        fi
    done
done
################################################################################################################################################
mkdir ExperimentsResults/SumPointPoint/SantiagoMediano
echo "----------------------------Experimentando con SantiagoMediano------------------------"
for Raster_1 in 9 7 6   
do
    for Raster_2 in 9 7 6 5 4 2
    do
        if [ $Raster_2 -le $Raster_1 ] 
        then
            echo "-----Experimentando con SantiagoMediano ${Raster_1}x${Raster_2}-----"
            mkdir ExperimentsResults/SumPointPoint/SantiagoMediano/${Raster_1}x${Raster_2} 
            for ej in 1 2 3
            do
                echo "Ejecución ${ej}"
                /usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/RasterRealesChile/SantiagoMediano/SantiagoMediano_${Raster_1}/SantiagoMediano_${Raster_1} Output/RasterRealesChile/SantiagoMediano/SantiagoMediano_${Raster_2}/SantiagoMediano_${Raster_2} 4 1 &> ExperimentsResults/SumPointPoint/SantiagoMediano/${Raster_1}x${Raster_2}/Algebraqueries_${ej}.txt
                /usr/bin/time -v ./Experiments/TestSpeedGetValue Output/RasterRealesChile/SantiagoMediano/SantiagoMediano_${Raster_1}/SantiagoMediano_${Raster_1} Output/RasterRealesChile/SantiagoMediano/SantiagoMediano_${Raster_2}/SantiagoMediano_${Raster_2} 4 1 &> ExperimentsResults/SumPointPoint/SantiagoMediano/${Raster_1}x${Raster_2}/GetValue_${ej}.txt
            done
        fi
    done
done
################################################################################################################################################
mkdir ExperimentsResults/SumPointPoint/SantiagoGrande
echo "----------------------------Experimentando con SantiagoGrande------------------------"
for Raster_1 in 9 7 6   
do
    for Raster_2 in 9 7 6 5 4 3
    do
        if [ $Raster_2 -le $Raster_1 ] 
        then
            echo "-----Experimentando con SantiagoGrande ${Raster_1}x${Raster_2}-----"
            mkdir ExperimentsResults/SumPointPoint/SantiagoGrande/${Raster_1}x${Raster_2} 
            for ej in 1 2 3
            do
                echo "Ejecución ${ej}"
                /usr/bin/time -v ./Experiments/TestSpeedAlgebraQueries Output/RasterRealesChile/SantiagoGrande/SantiagoGrande_${Raster_1}/SantiagoGrande_${Raster_1} Output/RasterRealesChile/SantiagoGrande/SantiagoGrande_${Raster_2}/SantiagoGrande_${Raster_2} 4 1 &> ExperimentsResults/SumPointPoint/SantiagoGrande/${Raster_1}x${Raster_2}/Algebraqueries_${ej}.txt
                /usr/bin/time -v ./Experiments/TestSpeedGetValue Output/RasterRealesChile/SantiagoGrande/SantiagoGrande_${Raster_1}/SantiagoGrande_${Raster_1} Output/RasterRealesChile/SantiagoGrande/SantiagoGrande_${Raster_2}/SantiagoGrande_${Raster_2} 4 1 &> ExperimentsResults/SumPointPoint/SantiagoGrande/${Raster_1}x${Raster_2}/GetValue_${ej}.txt
            done
        fi
    done
done