/*
 * query.c
 *
 *  Created on: 15/04/2013
 *      Author: guillermo
 */


#include "query.h"

QUERYLIST *readSQueries(char * filename) {
	FILE * f = fopen(filename, "r");
	QUERYLIST * ret = (QUERYLIST *) malloc(sizeof(QUERYLIST));
	int res = fscanf(f, "%d\n", &ret->nqueries);
	if (res != 1) return NULL;

	ret->queries = (SQUERY *) malloc(ret->nqueries * sizeof(SQUERY));
	int i;
	for (i = 0; i < ret->nqueries; i++) {
		res = fscanf(f, "%d %d\n", &ret->queries[i].row, &ret->queries[i].col);
		if (res != 2) return NULL;
	}
	fclose(f);
	
	return ret;
}
