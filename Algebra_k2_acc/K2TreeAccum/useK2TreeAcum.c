#include "useK2TreeAcum.h"
#include "../K2_Tree_Basic/basic.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

SPK2TREEACUM * loadk2TreeAcum(char * nameFile){
	
	//SE LEE ARCHIVO .POINTS DONDE CONTIENE LA INFORMACIÓN DE LA ESTRUCTURA
	char * nameFilePoints = (char *) malloc(2048*sizeof(char));
	strcpy(nameFilePoints,nameFile);
	strcat(nameFilePoints,".points");
    SPK2TREEACUM * sp = (SPK2TREEACUM *) malloc(sizeof(SPK2TREEACUM));
	FILE * fileK2TreeAcum = fopen (nameFilePoints, "r");

	// NUMERO DE VALORES DISTINTOS QUE HAY EN LA MATRIZ
	FREAD(&sp->numberValues, sizeof(int), 1, fileK2TreeAcum);
	sp->points = (int *) malloc(sp->numberValues * sizeof(int));
	
	// NUMERO DE FILAS Y COLUMNAS EN LA MATRIZ 
	FREAD(&sp->rows, sizeof(int), 1, fileK2TreeAcum);
	FREAD(&sp->columns, sizeof(int), 1, fileK2TreeAcum);

	// VECTOR DONDE SE ENCUENTRA ASOCIADA CADA INDICE AL VALOR REAL 
	FREAD(sp->points, sizeof(int), sp->numberValues, fileK2TreeAcum);
	
	// VECTOR DE ARBOLES K2TREE 
    sp->vectorTrees = malloc((sp->numberValues-1)*sizeof(MREP*));

    // STRING AUXILIAR PARA GUARDAR NOMBRE DE ARCHIVO TEMPORAL
    char * basename = (char *) malloc(2048*sizeof(char));

    // SE RECUPERA CADA ARBOL DESDE EL ARCHIVO CORRESPONDIENTE
    for (int i = 0; i <= (sp->numberValues-2); i++){
        sprintf(basename, "%s-%04d", nameFile, i);
        sp->vectorTrees[i] = loadRepresentation(basename);
    }
	free(nameFilePoints);
	fclose(fileK2TreeAcum);
	free(basename);
    return sp;
}

// RECIBE LA ESTRUCURA COMPACTA K2-TREE-ACUM, FILA Y COLUMNA DE LA MATRIZ. 
uint getValue(SPK2TREEACUM * sp , int row, int colum){

	// VERIFICA QUE LAS FILAS Y COLUMNAS NO SE SALGAN DEL RANGO DE LA MATRIZ
	if(row >= sp->rows || colum >= sp->columns)
		return -1;
	
	int min = 0;
	int max = sp->numberValues -2;
	int mid = -1;
	int res = 0;
	while(min <= max) {
		mid = (max + min) / 2;
		res = compact2CheckLinkQuery(sp->vectorTrees[mid],row,colum);
		if (res) {
			max = mid -1;
		} else {
			min = mid + 1;
		}
	}
	if (!res) mid++;
	return (mid <= sp->numberValues - 1) ? sp->points[mid] : -999;


	/*
	// RECORRIDO SECUENCIAL PARA BUSCAR EL VALOR
	for(int i=0;i<=(sp->numberValues-2);i++){
		if(compact2CheckLinkQuery(sp->vectorTrees[i],row,colum))
			return sp->points[i];
	}

	return sp->points[sp->numberValues-1];
	*/

}

// BUSCA LA POSICION EN EL QUE SE ENCUENTRA EL ARBOL K2-TREE PARA EL VALOR DADO
int findPositionValue(int * points,int numberValues ,int value){
	int min = 0;
	int max = numberValues;
	int mid = -1;

	while (min<=max){
		mid = (min+max)/2;
		if (points[mid]==value) 
			return mid;
		else if(points[mid] > value) max = mid-1;
			 else min = mid+1;
	}
	
	return -1; //SI NO SE ENCUENTRA
}


// ENCUENTRA LAS CELDAS QUE CONTIENEN UN VALOR
uint ** getCellsOneValue(SPK2TREEACUM * sp ,int value ){
	int position = findPositionValue(sp->points,sp->numberValues,value);
	if(position == -1) return NULL;
	if(position == sp->numberValues-1 || position == 0 ){
		uint ** result = (uint **)malloc (sp->rows*sizeof(uint *));
		//RETORNA MATRIZ DONDE LAS CELDAS CON VALOR 1 SON LAS QUE CONTIENEN EL VALOR REAL
		for (int i = 0; i < sp->rows; i++){
			result[i] = (uint *) malloc (sp->columns*sizeof(uint));
			for (int j = 0; j < sp->columns; j++){
				if(position == 0)
					result[i][j] = compact2CheckLinkQuery(sp->vectorTrees[position],i,j);
				else
					result[i][j] = 1 - compact2CheckLinkQuery(sp->vectorTrees[position-1],i,j);
			}
		}
		return result;
	}else{
		uint ** result = (uint **)malloc (sp->rows*sizeof(uint *));
		//RETORNA MATRIZ DONDE LAS CELDAS CON VALOR 1 SON LAS QUE CONTIENEN EL VALOR REAL
		for (int i = 0; i < sp->rows; i++){
			result[i] = (uint *) malloc (sp->columns*sizeof(uint));
			for (int j = 0; j < sp->columns; j++){
				int rest = compact2CheckLinkQuery(sp->vectorTrees[position],i,j) - compact2CheckLinkQuery(sp->vectorTrees[position-1],i,j);
				result[i][j] = rest;
			}
		}
		return result;
	}	
}

uint ** getCellsRangeValue(SPK2TREEACUM * sp , int valueInitial, int valueEnd){
	int positionInitial = findPositionValue(sp->points,sp->numberValues,valueInitial);
	int positionEnd = findPositionValue(sp->points,sp->numberValues,valueEnd);

	if(positionInitial == -1 || positionEnd == -1) return NULL;
	uint ** result = (uint **)malloc (sp->rows*sizeof(uint *));
	//RETORNA MATRIZ DONDE LAS CELDAS CON VALOR 1 SON LAS QUE CONTIENEN EL VALOR REAL DENTRO DEL RANGO
	for (int i = 0; i < sp->rows; i++){
		result[i] = (uint *) malloc (sp->columns*sizeof(uint));
		for (int j = 0; j < sp->columns; j++){
			int rest;
			if(positionEnd == sp->numberValues-1 || positionInitial == 0 ){
				if(positionEnd == sp->numberValues-1 && positionInitial == 0 ){
					rest = 1;
				}else{
					if(positionEnd == sp->numberValues-1) 
						rest = 1 - compact2CheckLinkQuery(sp->vectorTrees[positionInitial-1],i,j); 
					else
						rest =compact2CheckLinkQuery(sp->vectorTrees[positionEnd],i,j);	
				}
			}else{
				rest = compact2CheckLinkQuery(sp->vectorTrees[positionEnd],i,j) - compact2CheckLinkQuery(sp->vectorTrees[positionInitial-1],i,j);
			}
			result[i][j] = rest;
		}
	}
	return result;
}

void printMatriz(SPK2TREEACUM * sp){
	for (int x = 0; x < sp->rows; x++) {
        for (int y = 0; y < sp->columns; y++) {
			int v = getValue(sp , x, y);
			printf("%d ", v);
        }
		printf("\n");
	}
}

void destroyRepresentationK2Acc(SPK2TREEACUM * sp){
	if (sp->points)
		free(sp->points);
	if (sp->vectorTrees!=NULL){
		for (int i = 0; i < sp->numberValues-1; i++){
			if (sp->vectorTrees[i]!=NULL)
				destroyRepresentation(sp->vectorTrees[i]);
		}
		free(sp->vectorTrees);
	}
}