#include "../K2_Tree_Basic/kTree.h"

typedef struct {
	int numberValues;
	int * points;
	int rows;
    int columns;
    MREP ** vectorTrees;
} SPK2TREEACUM;

SPK2TREEACUM * loadk2TreeAcum(char * nameFile);
uint getValue(SPK2TREEACUM * sp , int row, int colum);
int findPositionValue(int * points,int numberValues ,int value);
uint ** getCellsOneValue(SPK2TREEACUM * sp , int value);
uint ** getCellsRangeValue(SPK2TREEACUM * sp , int valueInitial, int valueEnd);

void printMatriz(SPK2TREEACUM * sp);
void destroyRepresentationK2Acc(SPK2TREEACUM * sp);