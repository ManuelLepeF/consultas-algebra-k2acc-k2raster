#include <stdio.h>
#include <stdlib.h>
#include "useK2TreeAcum.h"

int main(int argc, char ** argv) {

	if(argc < 2) {
		printf("Usar: %s <Nombre Base Estructura> <Nombre Base Estructura slices> \n", argv[0]);
		exit(1);
	}
	
    // CARGA LA ESTRUCTURA EN MEMORIA
	char * nameStruct = argv[1];
	SPK2TREEACUM * sp = loadk2TreeAcum(nameStruct);
	

	// LECTURA DE LA MATRIZ OBTENIENDO NUMERO DE NODES Y EDGES 
	FILE *fileMatriz;
	fileMatriz = fopen(argv[2], "r");
	int *matriz = malloc(sp->rows *sp->columns*sizeof(uint));
	FREAD(matriz, sp->rows*sp->columns, sizeof(uint), fileMatriz);
	fclose(fileMatriz);

	printf("Verificando...\n");
	int f = 0;
	for (int x = 0; x < sp->rows; x++) {
        for (int y = 0; y < sp->columns; y++) {
			int v = getValue(sp , x, y);
			if(v!=sp->points[matriz[f]]){
				printf("\nFracaso\n");
				printf("Valor: %d Original: %d [%d][%d]\n",v,sp->points[matriz[f]],x,y);
				return 0;
			}
			f++;
        }
	}
	free(matriz);
	destroyRepresentationK2Acc(sp);
	printf("Exito!\n");
	return 0;
}
