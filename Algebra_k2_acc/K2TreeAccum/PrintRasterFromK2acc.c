#include <stdio.h>
#include <stdlib.h>
#include "useK2TreeAcum.h"

int main(int argc, char ** argv) {

	if(argc != 2) {
		printf("Usar: %s <Nombre Base Estructura> \n", argv[0]);
		exit(1);
	}
	
    // CARGA LA ESTRUCTURA EN MEMORIA
	char * nameStruct = argv[1];
	SPK2TREEACUM * sp = loadk2TreeAcum(nameStruct);
    // SE RECORRE EL RASTER OBTENIENDO EL VALOR POR CADA CELDA
	for (int x = 0; x < sp->rows; x++) {
        for (int y = 0; y < sp->columns; y++) {
			getValue(sp , x, y);
			printf("%d ",getValue(sp , x, y));
        }
        printf("\n");
	}
	destroyRepresentationK2Acc(sp);
	return 0;
}
