#include <stdio.h>
#include <stdlib.h>
#include "useK2TreeAcum.h"

int main(int argc, char ** argv) {

	if(argc < 2) {
		printf("Usar: %s <Nombre Estructura> <Nombre archivo salida> \n", argv[0]);
		exit(1);
	}
	
    //**************************************//
    //  CARGA LA ESTRUCTURA EN MEMORIA     //
    //************************************//
    printf("Cargando estructura en memoria...\n");
	char * nameStruct = argv[1];
	SPK2TREEACUM * sp = loadk2TreeAcum(nameStruct);
	uint nRows = sp->rows;
	uint nCols = sp->columns;
    printf("Filas: %d\n",nRows);
    printf("Columnas: %d\n",nCols);

    //*******************************************//
    //  RECUPERA LA MATRIZ DESDE LA ESTRUCTURA  //
    //*****************************************//
    printf("Recuperando matriz desde la estructura %s...\n",argv[1]);
    int ** matriz = (int **)malloc (nRows*sizeof(int *));
    for (int x = 0; x < nRows; x++) {
        matriz[x] =  (int *)malloc(nCols*sizeof(int *));
        for (int y = 0; y < nCols; y++) {
            matriz[x][y] =  getValue(sp , x, y);
        }
	}
    destroyRepresentationK2Acc(sp);

    //*************************************//
    //  GUARDA LA MATRIZ EN ARCHIVO BIN   //
    //***********************************//
    printf("Guardando Bin en: %s...\n",argv[2]);
	FILE * outputFile = fopen(argv[2], "w+");
    for (int x = 0; x < nRows; x++) {
        fwrite(matriz[x], sizeof(int), nCols, outputFile);
    }
    fclose(outputFile);
	free(matriz);
    printf("Operacion realizada con exito.\n");
	return 0;
}
