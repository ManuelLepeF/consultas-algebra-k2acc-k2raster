#include <stdio.h>
#include <stdlib.h>
#include "useK2TreeAcum.h"

int main(int argc, char ** argv) {

	if(argc < 1) {
		printf("Usar: %s <Nombre Base Estructura> ", argv[0]);
		exit(1);
	}
	
    printf("Cargando estructura: %s \n",argv[1]);
    // CARGA LA ESTRUCTURA EN MEMORIA
	char * nameStruct = argv[1];
	SPK2TREEACUM * sp = loadk2TreeAcum(nameStruct);
	printf("Filas: %d \n",sp->rows);
	printf("Columnas: %d \n",sp->columns);
	printf("Número de valores distintos: %d \n",sp->numberValues);
	printf("\nValor mitad points: %d \n\n",sp->points[sp->numberValues/2]);
	printf("Valor máximo: %d \n",sp->points[sp->numberValues-1]);
	printf("Valor mínimo: %d \n",sp->points[0]);
	destroyRepresentationK2Acc(sp);
	return 0;
}
