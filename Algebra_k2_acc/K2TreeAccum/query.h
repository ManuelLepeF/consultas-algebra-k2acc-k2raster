/*
 * query.h
 *
 *  Created on: 15/04/2013
 *      Author: guillermo
 */

#ifndef QUERY_H_
#define QUERY_H_


#include <stdlib.h>
#include <stdio.h>

typedef struct sQuery {
	int row;
	int col;
} SQUERY;

typedef struct sql {
	int nqueries;
	SQUERY * queries;
} QUERYLIST;



QUERYLIST * readSQueries(char * filename);

#endif /* QUERY_H_ */
