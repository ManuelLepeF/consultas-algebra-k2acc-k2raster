#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "../K2_Tree_Basic/kTree.h"


int main(int argc, char ** argv) {

    // SE VERIFICAN LOS PARAMETROS
	if (argc < 5) {
		fprintf(stderr,
				"USAGE: %s < Matriz > < Name file output > < Number rows > <Number columns> \n",
				argv[0]);
		return (-1);
	}
    
    uint nRows = atoi(argv[3]);
	uint nCols = atoi(argv[4]);

    // LECTURA DE LA MATRIZ OBTENIENDO NUMERO DE NODES Y EDGES 
	FILE *fileMatriz;
	fileMatriz = fopen(argv[1], "r");
	int *matriz = malloc(nRows*nCols*sizeof(uint));
	FREAD(matriz, nRows*nCols, sizeof(uint), fileMatriz);
	fclose(fileMatriz);
    
    // SE OBTIENE EL VALOR MÁXIMO (INTERVALOS DE VALORES EN LA MATRIZ)
    int max = -1;
    //printf("MATRIZ: \n");
	for (int i = 0; i < nRows* nCols; i++) {
        //printf("%d ",matriz[i]);
		if (matriz[i] > max) max = matriz[i];
	}
	printf("\nMAX VALUE: %d\n", max);
    
    // NUMEROS DE NODOS
    uint nodes; 
    if(nRows > nCols) 
        nodes = nRows;
    else
        nodes = nCols;

    for (int i=0;i<max;i++){
        ulong nEdges = 0;
	    {
		    for (int r = 0; r < nRows* nCols; r++) {
			    if (matriz[r] >= 0 && matriz[r] <=i) {
                    nEdges++;
                }
	    	}
	    }

        uint *xedges = (uint *)malloc(sizeof(uint)*nEdges);
	    uint *yedges = (uint *)malloc(sizeof(uint)*nEdges);
	    uint cedg = 0;
        uint nodes_read=0;

        // SE INSERTAN LAS ARISTAS (REFACTORIZAR POR FAVOR!!)
        for (int f = 0; f < nRows*nCols; f++) {
            for(int j =0;j < nCols;j++){
                if (matriz[f] >= 0 && matriz[f] <=i) {
                    xedges[cedg]=nodes_read;
                    yedges[cedg]=j;
                    cedg++;
                }
                f++;
            }
            f--;
            nodes_read++;
	    }

        uint max_level = floor(log(nodes)/log(K));
        /* ESTO ESS */
        if(floor(log(nodes)/log(K))==(log(nodes)/log(K))) {
        	max_level=max_level-1;
        }
        MREP * KT; 
        KT = compactCreateKTree(xedges, yedges, nodes,nEdges,max_level);
        char * basename = (char *) malloc(2048*sizeof(char));
        sprintf(basename, "%s-%04d", argv[2], i);
        saveRepresentation(KT, basename);
        destroyRepresentation(KT);
        free(xedges);
        free(yedges);
        free(basename);
    }

    printf("Estructura creada exitosamente \n");

}
