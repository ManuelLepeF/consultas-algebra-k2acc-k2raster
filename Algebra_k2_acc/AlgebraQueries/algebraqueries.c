#include "algebraqueries.h"

MREP * copyKT(MREP * KT){
	MREP * copyResult = (MREP *) malloc(sizeof( MREP));
	if(copyResult == NULL){
		printf("Problema al reservar memoria del MREP.\n");
		return NULL;
	}
	// SE COPIA OTROS DATOS DEL K2-TREE 
	copyResult->btl_len = KT->btl_len;
	copyResult->bt_len = KT->bt_len;
	copyResult->maxLevel =KT->maxLevel;
	copyResult->numberOfNodes = KT->numberOfNodes;
	copyResult->numberOfEdges = KT->numberOfEdges;
	copyResult->info = (uint *)malloc(sizeof(uint)*MAX_INFO);
	copyResult->element = (uint *)malloc(sizeof(uint)*MAX_INFO);	
	copyResult->basex = (uint *)malloc(sizeof(uint)*MAX_INFO);
	copyResult->basey = (uint *)malloc(sizeof(uint)*MAX_INFO);
	copyResult->iniq = -1;
	copyResult->finq =-1;
	copyResult->info2[0] = (uint *)malloc(sizeof(uint)*MAX_INFO);
	copyResult->info2[1] = (uint *)malloc(sizeof(uint)*MAX_INFO);
	copyResult->div_level_table = (uint *)malloc(sizeof(uint)*(copyResult->maxLevel+1));
	for(int i=0;i<=copyResult->maxLevel;i++)
		copyResult->div_level_table[i]=KT->div_level_table[i];
	
	// SE COPIA EL BITRANK 
	copyResult->btl = (bitRankW32Int *) malloc(sizeof(struct sbitRankW32Int));
	copyResult->btl->n=KT->btl->n;
	uint n2 = copyResult->btl_len;
	copyResult->btl->data = (uint *) malloc(sizeof( uint) *(n2/W+1));
	for (int i = 0; i < n2/W+1 ; i++){
		copyResult->btl->data[i] = KT->btl->data[i];
	}
	copyResult->btl->owner = KT->btl->owner;
	copyResult->btl->integers =  KT->btl->integers;
	copyResult->btl->factor = KT->btl->factor;
	copyResult->btl->b = KT->btl->b;
	copyResult->btl->s = KT->btl->s;
	uint n = copyResult->btl->n;
	uint s = copyResult->btl->s;
	copyResult->btl->Rs = (uint*)malloc(sizeof(uint)*(n/s+1));
	for (int i = 0; i < n/s+1; i++){
		copyResult->btl->Rs[i] = KT->btl->Rs[i];
	}
	
	
	return copyResult;

}

SPK2TREEACUM * copyK2TreeAcc(SPK2TREEACUM *sp){
	SPK2TREEACUM * spCopy = (SPK2TREEACUM *) malloc(sizeof(SPK2TREEACUM));
	spCopy->numberValues = sp->numberValues;
	spCopy->rows = sp->rows;
	spCopy->columns = sp->columns;
	spCopy->points = (int *) malloc(spCopy->numberValues * sizeof(int));
	spCopy->vectorTrees = (MREP**)malloc((spCopy->numberValues-1)*sizeof(MREP*));
	for (int i = 0; i < spCopy->numberValues; i++){
		spCopy->points[i] = sp->points[i];
		if (i!=spCopy->numberValues -1){
			spCopy->vectorTrees[i] = ktree_deep_copy(sp->vectorTrees[i]);
		}
		
	}
	return spCopy;
}
void insertionSort(int * sum,int * raster1, int * raster2, int n){ 
    int sumKey,raster1Key, raster2Key, j; 
    sumKey = sum[n];
	raster1Key = raster1[n];
	raster2Key = raster2[n];
	j = n - 1;

	while (j >= 0 && sum[j] > sumKey) { 
		sum[j + 1] = sum[j]; 
		raster1[j + 1] = raster1[j];
		raster2[j + 1] = raster2[j];
		j = j - 1; 
	}
	
	sum[j + 1] = sumKey; 
	raster1[j + 1] = raster1Key;
	raster2[j + 1] = raster2Key;
	
	
}

void InsertionSortZonalSum(int * point, MREP ** vectorTree, MREP * lastKT, int n){
	int pointKey, j;
	MREP * ktKey= NULL;
	pointKey = point[n];
	if (lastKT!=NULL){
		ktKey= lastKT;
	}else{
		ktKey = vectorTree[n];
	}
	j = n - 1;

	while (j >= 0 && point[j] > pointKey) { 
		point[j + 1] = point[j]; 
		if (lastKT==NULL ){
			vectorTree[j + 1] = vectorTree[j];
		}else{
			if (j+1!=n){
				vectorTree[j + 1] = vectorTree[j];
			}
		}
		j = j - 1; 
	}
	point[j + 1] = pointKey;
	if (!(j+1 == n && lastKT!=NULL)){
		vectorTree[j + 1] = ktKey;
	}
	
}

void PrintKT(MREP * KT, int row, int column){
	for (int x = 0; x < row; x++) {
		for (int y = 0; y < column; y++) {
			int v = compact2CheckLinkQuery(KT,x,y);
			printf("%d ", v);
		}
		printf("\n");
	}
}


SPK2TREEACUM * ThresHolding(SPK2TREEACUM * sp, int value){
	/* BUSCA EL INDICE DONDE SE ENCUENTRA LA ESTRUCTURA K2-TREE PARA EL VALOR DADO */
	int indice = findPositionValue(sp->points,sp->numberValues,value);

	/* SE CREA K2-TREE-ACCUM DE SALIDA */
	SPK2TREEACUM * spOut = (SPK2TREEACUM *) malloc(sizeof(SPK2TREEACUM));
	spOut->numberValues = 2;
	spOut->rows = sp->rows;
	spOut->columns = sp->columns;
	spOut->points = (int *) malloc(spOut->numberValues * sizeof(int));
	spOut->points[0] = 0;
	spOut->points[1] = 1;
	spOut->vectorTrees = (MREP**) malloc(1*sizeof(MREP*));
	
	/* Caso esquina cuando el valor es el valor mas pequeño */
	if(indice !=0)
		spOut->vectorTrees[0] = ktree_deep_copy(sp->vectorTrees[indice-1]);
	else
		spOut->vectorTrees[0] = ktree_deep_copy(sp->vectorTrees[indice]);

	return spOut;
}

SPK2TREEACUM * SumScale(SPK2TREEACUM * sp, int value){
	
	/* SE COPIA LA K2-TREE-ACC */
	SPK2TREEACUM * spOut = copyK2TreeAcc(sp);
	// SE SUMA CADA VALOR REAL POR EL ESCALAR
	for (int i = 0; i < spOut->numberValues; i++){
		spOut->points[i] = spOut->points[i]+value;
	}
	
	return spOut;
}

SPK2TREEACUM * MultiplicationScale(SPK2TREEACUM * sp, int value){
	
	/* SE COPIA LA K2-TREE-ACC */
	SPK2TREEACUM * spOut = copyK2TreeAcc(sp);
	// SE MULTIPLICA CADA VALOR REAL POR EL ESCALAR
	for (int i = 0; i < spOut->numberValues; i++){
		spOut->points[i] = spOut->points[i]*value;
	}
	
	return spOut;
}


SPK2TREEACUM * SumPointPoint(SPK2TREEACUM * spOne, SPK2TREEACUM * spTwo){

	/* DECLARACIÓN DE LOS VECTORES QUE SE USARAN PARA LA COMBINATORIA */
	int * sum = (int *) malloc((spOne->numberValues * spTwo->numberValues) * sizeof(int));
	int * raster1 = (int *) malloc((spOne->numberValues * spTwo->numberValues) * sizeof(int));
	int * raster2 = (int *) malloc((spOne->numberValues * spTwo->numberValues) * sizeof(int));
	

	/* COMBINATORIA DE LA SUMA DE LOS 2 VECTORES POINTS*/
	int indice = 0;
	for (int i=0;i<spOne->numberValues;i++){
		for (int j = 0; j < spTwo->numberValues; j++){
			sum[indice] = spOne->points[i] + spTwo->points[j];
			raster1[indice] = spOne->points[i];
			raster2[indice] = spTwo->points[j];
			/* SE ORDENAN LOS VECTORES */
			insertionSort(sum,raster1,raster2, indice);
			indice ++;
		}
	}
	
	/* VARIABLES QUE SE UTILIZAN PARA LA CONSTRUCCIÓN DE CADA k2-TREE */
	int sizeVectorAux = spOne->numberValues * spTwo->numberValues;
	int * vectorPointAux = (int *) malloc(0 * sizeof(int));
	MREP ** vectorTreesAux = malloc((0)*sizeof(MREP*));
	int valueSum;
	int indiceVector = 0;
	valueSum = sum[0];
	MREP * UnionI = NULL;

	/* CONSTRUCCIÓN DE LOS K2-TREE */
	for (int i = 0; i < sizeVectorAux; i++){	
		//printf("\n------------Va en el %d de %d-------------\n",i,sizeVectorAux-1);
		/* SE VERIFICA SI NO QUEDAN MAS COMBINATORIA PARA EL VALOR ACTUAL */
		if(sum[i] != valueSum){
			/* SI NO ES EL PRIMER K2-TREE SE REALIZA LA UNION CON EL K2-TREE ANTERIOR */
			if(indiceVector!=0){
				MREP * UnionKT = ktree_union(vectorTreesAux[indiceVector-1],UnionI);
				/* SI EL K2-TREE RESULTANTES SOLO TIENES 1 NO SE 
				   GUARDA YA QUE CORRESPONDERIA AL VALOR MAYOR DEL RASTER
				   RESULTANTE Y SE TERMINARIA LA CONSTRUCCIÓN */
				if (UnionKT->numberOfEdges==UnionKT->numberOfNodes*UnionKT->numberOfNodes){
					destroyRepresentation(UnionKT);
					vectorPointAux = (int *) realloc(vectorPointAux,(indiceVector+1) * sizeof(int));
					vectorPointAux[indiceVector] = valueSum;
					break;
				}
				/*  SI EL K2-TREE CREADO ES DISTINTO AL K2-TREE ANTERIOR SE GUARDA, CASO CONTRARIO 
					EL VALOR NO APARECE EN EL RASTER RESULTANTE POR LO TANTO NO SE GURADA */
				//MREP * diferencia = ktree_difference(UnionKT,vectorTreesAux[indiceVector-1]);
				if (UnionKT->numberOfEdges != vectorTreesAux[indiceVector-1]->numberOfEdges){
					vectorTreesAux = realloc(vectorTreesAux,(indiceVector+1)*sizeof(MREP*));
					vectorTreesAux[indiceVector] = UnionKT;
					vectorPointAux = (int *) realloc(vectorPointAux,(indiceVector+1) * sizeof(int));
					vectorPointAux[indiceVector] = valueSum;
					indiceVector ++;
				}else{
					destroyRepresentation(UnionKT);
				}
				destroyRepresentation(UnionI);
				UnionI = NULL;
			}else{
				/* PARA EL PRIMER K2-TREE */
				if (UnionI->numberOfEdges!=0){
					vectorTreesAux = realloc(vectorTreesAux,(indiceVector+1)*sizeof(MREP*));
					//printf("\nEdges: %d \n",UnionI->numberOfEdges);
					vectorTreesAux[indiceVector] = UnionI;
					vectorPointAux = (int *) realloc(vectorPointAux,(indiceVector+1) * sizeof(int));
					vectorPointAux[indiceVector] = valueSum;
					indiceVector ++;
				}
			}
			/* SE ACTUALIZA EL VALOR PARA CREAR UN NUEVO K2-TREE */
			valueSum = sum[i];
			UnionI = NULL;
		}
		
		/* SE ENCUENTRA LA POSICIÓN DE LOS K2-TREE QUE SE DEBEN INTERSECTAR */ 
		int PositionR1 = findPositionValue(spOne->points,spOne->numberValues,raster1[i]);
		int PositionR2 = findPositionValue(spTwo->points,spTwo->numberValues,raster2[i]);
		MREP *intersection= NULL;
		
		/* SE VERIFICAN SI LOS VALORES SON LOS MAYORES */
		if (PositionR1 != spOne->numberValues-1 && PositionR2 != spTwo->numberValues-1){
			//printf("\nIndice R1: %d Edges R1: %d Indice R2: %d Edges R2: %d \n",PositionR1,spOne->vectorTrees[PositionR1]->numberOfEdges,PositionR2,spTwo->vectorTrees[PositionR2]->numberOfEdges );
			intersection= ktree_intersection(spOne->vectorTrees[PositionR1],spTwo->vectorTrees[PositionR2]);
		}else{
			if(PositionR1 == spOne->numberValues-1 && PositionR2 != spTwo->numberValues-1){
				intersection = ktree_deep_copy(spTwo->vectorTrees[PositionR2]);
			}else{
				if (PositionR1 != spOne->numberValues-1 && PositionR2 == spTwo->numberValues-1){
					intersection = ktree_deep_copy(spOne->vectorTrees[PositionR1]);
				}else{
					/* CUANDO LOS DOS VALORES SON LOS MAYOR DE CADA RASTER,
					   EL K2-TREE RESULTANTE SON SOLO 1 POR LO TANTO NO SE 
					   GUARDA YA QUE CORRESPONDERIA AL VALOR MAYOR DEL RASTER
					   RESULTANTE Y SE TERMINARIA LA CONSTRUCCIÓN */
					vectorPointAux = (int *) realloc(vectorPointAux,(indiceVector+1) * sizeof(int));
					vectorPointAux[indiceVector] = valueSum;
					break;
				}
			}
		}

		/* SE REALIZA LA UNION */
		if(UnionI!=NULL){
			UnionI = ktree_union(UnionI,intersection);
			destroyRepresentation(intersection);
		}else{
			UnionI = intersection;
		}
		
		/* CUANDO LLEGA AL ULTIMO VALOR */
		if(i+1==sizeVectorAux){
			vectorPointAux = (int *) realloc(vectorPointAux,(indiceVector+1) * sizeof(int));
			vectorPointAux[indiceVector] = valueSum;
		}
	}

	free(sum);
	free(raster1);
	free(raster2);
	/* SE CREA K2-TREE-ACCUM RESULTANTE */
	SPK2TREEACUM * spSum = (SPK2TREEACUM *) malloc(sizeof(SPK2TREEACUM));
	spSum->numberValues = indiceVector+1;
	spSum->rows = spOne->rows;
	spSum->columns = spOne->columns;
	spSum->points = vectorPointAux;
	vectorPointAux = NULL;
	spSum->vectorTrees = vectorTreesAux;
	vectorTreesAux = NULL;
	return spSum;
}

SPK2TREEACUM * ZonalSum(SPK2TREEACUM * spInput, SPK2TREEACUM * spZonal){
	/* RESERVA MEMORIA PARA K2-TREE-ACCUM DE SALIDA */
	SPK2TREEACUM * zonalSum = (SPK2TREEACUM *) malloc(sizeof(SPK2TREEACUM));
	zonalSum->numberValues = spZonal->numberValues;
	zonalSum->rows = spZonal->rows;
	zonalSum->columns = spZonal->columns;
	zonalSum->points = (int *) malloc(zonalSum->numberValues * sizeof(int));
	zonalSum->vectorTrees = malloc((zonalSum->numberValues-1)*sizeof(MREP*));
	zonalSum->vectorTrees[0] = ktree_deep_copy(spZonal->vectorTrees[0]);// probar de puntero
	zonalSum->points[0] = spZonal->points[0];
	/* RECORRE LOS K2-TREE DEL RASTER ZONA */
	for (int i = 1; i < spZonal->numberValues; i++){
		//printf("\nVa en el: %d\n", i);
		MREP * differenceKT = NULL;
		if (i!=spZonal->numberValues-1){
				/* SE HACE LA DIFERENCIA */
				differenceKT = ktree_difference(spZonal->vectorTrees[i],spZonal->vectorTrees[i-1]);
		}else{
				/* COMO ES EL ÚLTIMO K2-TREE SE HACE EL COMPLEMENTO DEL K2TREE-1 */
				differenceKT = ktree_complement(spZonal->vectorTrees[i-1]);
		}
		int SumPointTemporal = 0;
		int NumberEdgesPrevious = 0;
		/* SE SUMAN TODOS LOS K2-TREE */
		for (int j = 0; j < spInput->numberValues; j++){
			MREP * intersectionKT = NULL;
			if (j!=spInput->numberValues-1){
				intersectionKT = ktree_intersection(differenceKT,spInput->vectorTrees[j]);
			}else{
				intersectionKT = differenceKT;
			}
			SumPointTemporal = SumPointTemporal + (intersectionKT->numberOfEdges - NumberEdgesPrevious)*spInput->points[j];
			NumberEdgesPrevious = intersectionKT->numberOfEdges;
			/* SI ES EL ULTIMO K2-TREE NO SE DEBE LIBERAR YA QUE ES LA DIFERENCIA*/
			if(j!=spInput->numberValues-1)
				destroyRepresentation(intersectionKT);
		}

		/* INSERTAR ORDENADO */
		zonalSum->points[i] = SumPointTemporal;
		if (i!=spZonal->numberValues-1){
			zonalSum->vectorTrees[i] = differenceKT;
			InsertionSortZonalSum(zonalSum->points,zonalSum->vectorTrees,NULL,i);
		}else{
			MREP * lastKT = differenceKT;
			InsertionSortZonalSum(zonalSum->points,zonalSum->vectorTrees,lastKT,i);
		}
		
	}

	/*UNION PARA CONSTRUIR LOS K2-TREE ACUMULATIVO*/
	for ( int j = 1; j < zonalSum->numberValues-1; j++){
		MREP * ktAux = ktree_union(zonalSum->vectorTrees[j],zonalSum->vectorTrees[j-1]);
		destroyRepresentation(zonalSum->vectorTrees[j]);
		zonalSum->vectorTrees[j] = ktAux;
	}
	
	return zonalSum;
	
}
