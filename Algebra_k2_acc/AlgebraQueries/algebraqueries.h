#include <stdio.h>
#include <stdbool.h> 
#include <stdlib.h>
#include "../K2_Tree_Basic/ktree_utils.h"
#include "../K2TreeAccum/useK2TreeAcum.h"
#include "../K2TreeAccum/query.h"
#include "../kt_setOperations/ktree_algebraic.h"

void insertionSort(int * sum,int * raster1, int * raster2, int n);
void InsertionSortZonalSum(int * point, MREP ** vectorTree, MREP * lastKT, int n);
void PrintKT(MREP * KT, int row, int column);
MREP * copyKT(MREP * KT);
SPK2TREEACUM * copyK2TreeAcc(SPK2TREEACUM *sp);
SPK2TREEACUM * ThresHolding(SPK2TREEACUM * sp, int value);
SPK2TREEACUM * SumScale(SPK2TREEACUM * sp, int value);
SPK2TREEACUM * MultiplicationScale(SPK2TREEACUM * sp, int value);
SPK2TREEACUM * SumPointPoint(SPK2TREEACUM * spOne, SPK2TREEACUM * spTwo);
SPK2TREEACUM * ZonalSum(SPK2TREEACUM * spInput, SPK2TREEACUM * spZonal);

