#!/bin/bash

if [ $# -ne 4 ]; then
    echo "Usar: <$0>  <Numero de filas> <Numero de columnas> <Numero de zonas diferentes> <Nombre raster>"
    exit 1
else
    ./Tools/GenerarRasterInputDos $1 $2 $3 $3
    mkdir Input/$4
    mkdir Output/$4
    mv raster.slices Input/$4/$4.slices
    mv raster.txt Input/$4/$4.txt
    mv raster.points Output/$4/$4.points
    ./K2TreeAccum/buildfrommatriz Input/$4/$4.slices Output/$4/$4 $1 $2
fi