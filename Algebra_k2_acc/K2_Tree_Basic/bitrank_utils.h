/*
 *  bitrank_utils.h
 *  2014-10-22. Miguel Rodríguez Penabad
 *
 *  Helper functions for bitranks (bitrankw32int)
 */

#ifndef BITRANK_UTILS_H
#define BITRANK_UTILS_H

#include "bitrankw32int.h"

/*
 * bitrank_deep_copy: Performs a "deep copy" of a bitRankW32Int pointer. 
 * Sizes of data and Rs fields are taken from "load" function
 * in bitrankw32int.c
 * The additional len parameter lets us copy a bitmap that has the rank
 * build only partially (ex: k2tree with T:L bitmap, ranks only over T)
 * 
 * TODO: Move this function (with permission) to bitrankw32int.h/.c
 */ 
 bitRankW32Int * bitrank_deep_copy(bitRankW32Int *src, uint len);

#endif
