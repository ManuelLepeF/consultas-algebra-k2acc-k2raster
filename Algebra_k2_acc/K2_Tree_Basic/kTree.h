#ifndef KTREE_H
#define KTREE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bitrankw32int.h"
#include "basic.h"
//#define MAX_INFO 1024*1024+10
#define MAX_INFO 1024*1024*32

/* Possible values in "3-valued bits" bitmap of k2tree with 
 * compression of ones */
#define ZERO 0
#define ONE  1
#define ALLONES 2


typedef struct node
{
    int data;
    struct node** child;
   	char isallones;
   	char parentallones;
		char grandallones;
}NODE;


typedef struct matrixRep
{
    bitRankW32Int * btl;		//Bitmap representando el arbol T:L
    uint bt_len;						//Numero de bits de T
    uint btl_len;				//Numero de bits de T:L
    uint * bt2;							//Bitmap T'
	uint bt2_len;
    int maxLevel;			//Nivel maximo del arbol
    uint numberOfNodes;
    ulong numberOfEdges;
    uint * div_level_table;
    uint * info;
    uint * info2[2];
    uint * element;
    uint * basex;
    uint * basey;
    uint * allones;
    int iniq;
    int finq;
}MREP;


#define K 2
uint numberNodes;
uint numberLeaves;
uint numberTotalLeaves;



MREP * compactCreateKTree(uint * xedges, uint *yedges, uint numberOfNodes,ulong numberOfEdges, uint maxl);


uint * compactAdjacencyList(MREP * rep, int x);
uint * compact2AdjacencyList(MREP * rep, int x);
uint * compactInverseList(MREP * rep, int y);
uint ** compactRangeQuery(MREP * rep, uint p1, uint p2, uint q1, uint q2);
uint compactCheckLinkQuery(MREP * rep, uint p, uint q);
uint compact2CheckLinkQuery(MREP * rep, uint p, uint q);
uint compactCheckRangeQuery(MREP * rep, uint p1, uint p2, uint q1, uint q2);

MREP * loadRepresentation(char * basename);
void saveRepresentation(MREP * rep, char * basename);
void destroyRepresentation(MREP * rep);

/* used in buildk2tree*/
NODE * createKTree(int maxlevels);
void insertNode(NODE * root, int x, int y);
MREP * createRepresentation(NODE * root, uint numberOfNodes,ulong numberOfEdges);

/* Converts k2tree1 into an adjacency list*/
int * compactFullDecompression(MREP * rep);


/* used in bitmaparray*/
//uint exp_pow(uint base, uint pow);
ulong exp_pow(ulong base, ulong pow);

#endif
