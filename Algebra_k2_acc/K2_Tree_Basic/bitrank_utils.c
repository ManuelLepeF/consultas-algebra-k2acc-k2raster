/*
 *  bitrank_utils.c
 *  2014-10-22. Miguel Rodríguez Penabad
 *
 *  Helper functions for bitranks (bitrankw32int)
 *  Functions described in header file (bitrank_utils.h)
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "bitrankw32int.h"


bitRankW32Int * bitrank_deep_copy(bitRankW32Int *src, uint len){
	bitRankW32Int *dest = malloc(sizeof(bitRankW32Int));
    dest->owner = src->owner;
    dest->integers = src->integers;
    dest->factor = src->factor;
    dest->b = src->b;
    dest->s =src->s;
    dest->n = src->n;   

	//we copy "len" bits regardless of src->n content
    uint bytes_data = sizeof( uint) *(len/W+1);
    dest->data = (uint *) malloc(bytes_data);
    memcpy(dest->data, src->data, bytes_data);
    //Rs uses src->n, allowing for ranks over part of the whole bitmap
    uint bytes_Rs = sizeof(uint)*(dest->n/dest->s+1);
    dest->Rs = (uint*)malloc(bytes_Rs);
    memcpy(dest->Rs, src->Rs, bytes_Rs);
    return dest;
}
