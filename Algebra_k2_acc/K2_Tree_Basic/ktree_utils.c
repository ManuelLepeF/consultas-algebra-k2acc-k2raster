/*
 *  ktree_utils.c
 *  2014-11-26. Miguel Rodríguez Penabad
 *
 * Helper functions for K2Trees. Function desciptions in ktree_utils.h
 */


#include <stdio.h>
#include <stdlib.h>

#include "kTree.h"
#include "bitrank_utils.h"


/**********************************************************************/
void ktree_display_info(MREP *tree){
	if (!tree){
			printf("Empty tree\n");
			return;
	}
	
	uint i;
	
	printf("Number of nodes: %u\n", tree->numberOfNodes);
	printf("Number of edges: %lu\n", tree->numberOfEdges);

	printf("Max Level: %d\t", tree->maxLevel);
	printf("div_level_table: ");
	for (i=0;i<=tree->maxLevel;i++) printf("%d ", tree->div_level_table[i]);
	printf("\n");
	
	printf("\nBTL length: %u (Only BT: %u)\n", tree->btl_len, tree->bt_len);
	printf("BT2 length: %u\n", tree->bt2_len);
	
	//Internal tree: bt + bn (bitrankw32int)
	printf("Internal tree: ");

	printf("\nBT: ");
	for (i=0; i < tree->bt_len; i++){
		printf("%d", isBitSet(tree->btl, i)!=0);
		if ( !((i+1) % 4) ) printf(" ");
	}
	
	printf("\nbt2: ");
	int j;
	for (j=0; j < tree->bt2_len; j++){
		printf("%d", bitget(tree->bt2, j));
		if ( !((j+1) % 4) ) printf(" ");
	}
	
	//leaves: starts when BT finishes
	printf("\nLeaves: ");
	for (; i < tree->btl_len; i++){
		printf("%d",  isBitSet(tree->btl, i)!=0);
		if ( !((i+1) % 4) ) printf(" ");
	}
	
		
	printf("\n");
}


/**********************************************************************/
MREP * ktree_empty(){
	MREP * dest = (MREP *) malloc(sizeof(MREP));
		
	dest->btl = NULL;
    dest->btl_len = 0; 
	dest->bt_len = 0;
    
    dest->bt2 = NULL;
    dest->bt2_len = 0; 

    dest->maxLevel = 0;			
    dest->numberOfNodes = 0;
    dest->numberOfEdges = 0;
    
    dest->iniq = -1;
    dest->finq = -1;
    
	dest->div_level_table = NULL;
    dest->info = (uint *)malloc(sizeof(uint)*MAX_INFO);
	dest->info2[0] = (uint *)malloc(sizeof(uint)*MAX_INFO);
	dest->info2[1] = (uint *)malloc(sizeof(uint)*MAX_INFO);
	dest->element = (uint *)malloc(sizeof(uint)*MAX_INFO);	
	dest->basex = (uint *)malloc(sizeof(uint)*MAX_INFO);
	dest->basey = (uint *)malloc(sizeof(uint)*MAX_INFO);
	dest->allones = (uint *)malloc(sizeof(uint)*MAX_INFO);
    
    return (MREP *) dest;	
}

MREP * ktree_deep_copy(MREP *src){
	if (!src) 
		return (MREP *) NULL;
		
	MREP * dest = (MREP *) malloc(sizeof(MREP));
		
	//btl has btl_len bits, but rank strcuture over bt_len. 
    dest->bt_len = src->bt_len;
    dest->btl_len = src->btl_len;
    dest->btl = bitrank_deep_copy(src->btl, src->btl_len);
    
    //bt2 is just an array. Lenghts taken from kTree.c
    uint bytes_bt2 = (src->bt2_len/W + 1) * sizeof(uint);
    dest->bt2_len = src->bt2_len;
    dest->bt2 = (uint *) malloc(bytes_bt2);
	memcpy(dest->bt2, src->bt2, bytes_bt2);

    dest->maxLevel =src->maxLevel;			
    dest->numberOfNodes = src->numberOfNodes;
    dest->numberOfEdges = src->numberOfEdges;
    
    dest->iniq = src->iniq;
    dest->finq = src->iniq;
    
    //The following fields are used to perform queries. 
    //We just reserve memory for them, and copy data only
    //for div_level_table.
	dest->div_level_table = (uint *)malloc(sizeof(uint)*(dest->maxLevel+1));
	memcpy(dest->div_level_table, src->div_level_table, sizeof(uint)*(dest->maxLevel+1));
    dest->info = (uint *)malloc(sizeof(uint)*MAX_INFO);
	dest->element = (uint *)malloc(sizeof(uint)*MAX_INFO);	
	dest->basex = (uint *)malloc(sizeof(uint)*MAX_INFO);
	dest->basey = (uint *)malloc(sizeof(uint)*MAX_INFO);
	dest->allones = (uint *)malloc(sizeof(uint)*MAX_INFO);
	dest->info2[0] = (uint *)malloc(sizeof(uint)*MAX_INFO);
	dest->info2[1] = (uint *)malloc(sizeof(uint)*MAX_INFO);
    
    return (MREP *) dest;

}

/**********************************************************************/
int get3valBit(MREP *tree, int i){
		int b = (isBitSet(tree->btl, i) != 0) ;

		if (i < tree->bt_len){
			if ((b==0) && bitget(tree->bt2, rank0(tree->btl, i))) 
				b= 2; /*ALLONES=2*/
		}
		return b;
}

