#include "kTree.h"
#include "ktree_utils.h" /*get3valBit*/

typedef struct edgeinfo
{
    uint xval;
    uint yval;
    uint kval;
}tedge;


typedef struct QUEUEOFFCONS
{
    uint offsetL;
    uint offsetR;
    struct QUEUEOFFCONS *link;

}QUEUEOFFCONS;


typedef struct QUEUECONS
{
    NODE *element;
    struct QUEUECONS *link;

}QUEUECONS;

QUEUEOFFCONS * finalQUEUEOFFCONS;
QUEUECONS *finalQUEUECONS;


void AddItem2 (MREP *rep, int elem, int cantx,int canty, int allonesinbranch) {
  	if(rep->iniq!=-1){
	    rep->finq++;
			rep -> element[rep->finq] = elem;
			rep -> basex[rep->finq] = cantx;
			rep -> basey[rep->finq] = canty;
			rep -> allones[rep->finq] = allonesinbranch;
			
		}
		else{
			rep->iniq=0;
			rep->finq=0;
			rep -> element[rep->iniq] = elem;
			rep -> basex[rep->iniq] = cantx;
			rep -> basey[rep->iniq] = canty;
			rep -> allones[rep->iniq] = allonesinbranch;
		}
}

void RemoveItem2 (MREP * rep) {

	rep->iniq++;
}


QUEUEOFFCONS * AddItemOFFCONS (QUEUEOFFCONS * listpointer, uint offsetL, uint offsetR) {

  	if(listpointer!=NULL){
	    QUEUEOFFCONS * lp = (QUEUEOFFCONS  *) malloc (sizeof (struct QUEUEOFFCONS));
	  	finalQUEUEOFFCONS -> link = lp;
			lp -> link = NULL;
			lp -> offsetL = offsetL;
			lp -> offsetR = offsetR;
			finalQUEUEOFFCONS = lp;
			return listpointer;
		}
		else{
			listpointer = (QUEUEOFFCONS  *) malloc (sizeof (struct QUEUEOFFCONS));
			listpointer -> link = NULL;
			listpointer -> offsetL = offsetL;
			listpointer -> offsetR = offsetR;

			finalQUEUEOFFCONS = listpointer;
			return listpointer;
		}
}

QUEUEOFFCONS * RemoveItemOFFCONS (QUEUEOFFCONS * listpointer) {
    QUEUEOFFCONS * tempp;
    tempp = listpointer -> link;
    	free (listpointer);
    return tempp;
}

QUEUECONS * AddItemCONS (QUEUECONS * listpointer, NODE * elem) {

  	if(listpointer!=NULL){
	    QUEUECONS * lp = (QUEUECONS  *) malloc (sizeof (struct QUEUECONS));
	  	finalQUEUECONS -> link = lp;
			lp -> link = NULL;
			lp -> element = elem;
			finalQUEUECONS = lp;
			return listpointer;
		}
		else{
			listpointer = (QUEUECONS  *) malloc (sizeof (struct QUEUECONS));
			listpointer -> link = NULL;
			listpointer -> element = elem;
			finalQUEUECONS = listpointer;
			return listpointer;
		}
}

QUEUECONS * RemoveItemCONS (QUEUECONS * listpointer) {
    QUEUECONS * tempp;
//    printf ("Element removed is %d\n", listpointer -> dataitem);
    tempp = listpointer -> link;
    free (listpointer);
    return tempp;
}



//~ uint exp_pow(uint base, uint pow){
	//~ uint i, result = 1;
	//~ for(i=0;i<pow;i++)
		//~ result*=base;
	//~ return result;
//~ }

ulong exp_pow(ulong base, ulong pow){
	ulong i, result = 1;
	for(i=0;i<pow;i++)
		result*=base;
	return result;
}


void loadBitArray(uint * size, uint **ba, FILE * f) {
	fread(size, sizeof(uint), 1, f);
	*ba = (uint *) malloc(((*size)+31)/32 * sizeof(uint));
	fread(*ba, sizeof(uint), ((*size)+31)/32, f);
}
void saveBitArray(uint size, uint *ba, FILE *f) {
	fwrite(&size, sizeof(uint), 1, f);
	fwrite(ba, sizeof(uint), (size + 31)/32, f);
}

inline uint mybitget(uint * array, uint pos) {
	return bitget(array, pos);
}




int max_Level;


/* if(rep->btl)... added otherwise empty k2tree1s break this function*/
void saveRepresentation(MREP * rep, char * basename){
  char *filename = (char *) malloc(sizeof(char)*(strlen(basename)+6));
  strcpy(filename,basename);
  strcat(filename,".kt1");
  FILE * ft = fopen(filename,"w");
  if (!ft){
	  printf("Error opening output file %s\n", filename);
	  exit(-2);
  }

  fwrite(&(rep->numberOfNodes),sizeof(uint),1,ft);
  fwrite(&(rep->numberOfEdges),sizeof(ulong),1,ft);
  fwrite(&(rep->maxLevel),sizeof(uint),1,ft);

	uint s,n, n2, factor;
	if (rep->btl && rep->numberOfEdges){
		s=rep->btl->s;
		n=rep->btl->n;
		factor = rep->btl->factor;
		n2 = rep-> btl_len;
	} else{
		s=0;
		n=0;
		factor=20; //always using 20 in createBitRankW32Int, so store 20
		n2=0;
	}
  fwrite (&(n2),sizeof(uint),1,ft);
  
  fwrite (&(n),sizeof(uint),1,ft);

  fwrite (&(factor),sizeof(uint),1,ft);
  if (rep->btl  && rep->numberOfEdges){
	fwrite (rep->btl->data,sizeof(uint),n2/W+1,ft);
	fwrite (rep->btl->Rs,sizeof(uint),n/s+1,ft);
	saveBitArray(rep->bt2_len, rep->bt2, ft);
  }
  fclose(ft);
      
  free(filename);

}

MREP * loadRepresentation(char * basename){
	MREP * rep;
	int i;
	rep = (MREP *) malloc(sizeof(struct matrixRep));

	char *filename = (char *) malloc(sizeof(char)*(strlen(basename)+5));
  strcpy(filename,basename);
  strcat(filename,".kt1");
  FILE * ft = fopen(filename,"r");
  if (!ft){
  		fprintf(stderr, "Error opening k2tree1 file %s\n", filename);
		exit(-1);
}
  
  fread(&(rep->numberOfNodes),sizeof(uint),1,ft);
  fread(&(rep->numberOfEdges),sizeof(ulong),1,ft);
  fread(&(rep->maxLevel),sizeof(uint),1,ft);

  fread (&(rep-> btl_len),sizeof(uint),1,ft);
  
  //Do NOT read anything else if we are dealing with an empty tree.
  if (rep->numberOfEdges){
		  rep->btl = (bitRankW32Int *) malloc(sizeof(struct sbitRankW32Int));

		  fread (&(rep->btl->n),sizeof(uint),1,ft);
		  rep->btl->b=32;    
		  uint b=rep->btl->b;                        
		  fread (&(rep->btl->factor),sizeof(uint),1,ft);
		  
		  rep->btl->s=b*rep->btl->factor;
		  uint s=rep->btl->s;
		  uint n= rep->btl->n;
		  uint n2 = rep->btl_len;
		  rep->btl->integers = n/W;
		  rep->bt_len = n;
		  rep->btl->data= (uint *) malloc(sizeof( uint) *(n2/W+1));
		  fread (rep->btl->data,sizeof(uint),n2/W+1,ft);
		  rep->btl->owner = 1;
		  rep->btl->Rs=(uint*)malloc(sizeof(uint)*(n/s+1));
		  fread (rep->btl->Rs,sizeof(uint),n/s+1,ft);
		  loadBitArray(&(rep->bt2_len),&rep->bt2,ft);
  } else{
		rep->btl = NULL;
		rep->bt_len = 0;
		rep->bt2 = NULL;
		rep->bt2_len = 0;
  }
  
  fclose(ft);
  free(filename);


	rep->info = (uint *)malloc(sizeof(uint)*MAX_INFO);
	rep->element = (uint *)malloc(sizeof(uint)*MAX_INFO);	
	rep->basex = (uint *)malloc(sizeof(uint)*MAX_INFO);
	rep->basey = (uint *)malloc(sizeof(uint)*MAX_INFO);
	rep->allones = (uint *)malloc(sizeof(uint)*MAX_INFO);
	rep->iniq = -1;
	rep->finq =-1;
	
	rep->info2[0] = (uint *)malloc(sizeof(uint)*MAX_INFO);
	
	rep->info2[1] = (uint *)malloc(sizeof(uint)*MAX_INFO);
	
	
	rep->div_level_table = (uint *)malloc(sizeof(uint)*(rep->maxLevel+1));
	for(i=0;i<=rep->maxLevel;i++)
		rep->div_level_table[i]=exp_pow(K,rep->maxLevel-i);
	return rep;
}

void destroyRepresentation(MREP * rep){
	if (rep->btl) destroyBitRankW32Int(rep->btl);
	free(rep->bt2);
	if(rep->info!=NULL)
		free(rep->info);
	if (rep->info2){
		if(rep->info2[0]!=NULL)
			free(rep->info2[0]);
		if(rep->info2[1]!=NULL)
			free(rep->info2[1]);
	}
	if(rep->element)
		free(rep->element);
	if(rep->basex)
		free(rep->basex);
	if(rep->basey)
		free(rep->basey);
	if(rep->div_level_table)
		free(rep->div_level_table);
	//if(rep->allones)
	//	free(rep->allones);
	free(rep);
}

uint * compactAdjacencyList(MREP * rep, int x){
	int isallones;
	rep->iniq=-1;
	rep->finq=-1;
	uint posInf;
	uint totalAdyNodes =0;
	int i, k, j, queuecont, conttmp,node,div_level,xrelat;
	AddItem2(rep,0,0,x,0);
	queuecont = 1;
	for(i=0;i<rep->maxLevel;i++){
		conttmp = 0;
		div_level = rep->div_level_table[i];

		for(k=0;k<queuecont;k++){
				for(j=0;j<K;j++){
					xrelat = rep->basey[rep->iniq];
					node = xrelat/div_level*K + j;
					node += rep->element[rep->iniq];
					isallones = rep->allones[rep->iniq]|| (!isBitSet(rep->btl, node) && mybitget(rep->bt2, rank0(rep->btl, node)));
					if(isallones||isBitSet(rep->btl,node)){
						conttmp++;
						AddItem2(rep,isallones?0:(rank(rep->btl,node)*K*K),rep->basex[rep->iniq]+j*div_level,rep->basey[rep->iniq]%div_level,isallones);
					}
				}
			
			RemoveItem2(rep);
		}
		queuecont = conttmp;
	}
	while(rep->iniq<=rep->finq){
		posInf = rep->element[rep->iniq];
		isallones = rep->allones[rep->iniq];
		for(i=0;i<K;i++){
			if(isallones||bitget(rep->btl->data,posInf+(i+(x%K)*K))){
				totalAdyNodes++;
				rep->info[totalAdyNodes]=rep->basex[rep->iniq]+i;
			}
		}
		RemoveItem2(rep);
	}
	rep->info[0]=totalAdyNodes;
	return rep->info;
}


uint * compactInverseList(MREP * rep, int y){
	rep->iniq=-1;
	rep->finq=-1;
	uint posInf;
	int isallones;

	uint totalAdyNodes =0;
	int i, k, j, queuecont, conttmp,node,div_level,yrelat;

	AddItem2(rep,0,y,0,0);
	queuecont = 1;
	for(i=0;i<rep->maxLevel;i++){
		conttmp = 0;
		div_level = rep->div_level_table[i];
		for(k=0;k<queuecont;k++){
				for(j=0;j<K;j++){
					yrelat = rep->basex[rep->iniq];
					node = K*j+yrelat/div_level;
					node += rep->element[rep->iniq];
					isallones = rep->allones[rep->iniq] || (!isBitSet(rep->btl, node) && mybitget(rep->bt2, rank0(rep->btl, node)));

					if(isallones||isBitSet(rep->btl,node)){
						conttmp++;
						AddItem2(rep,isallones?0:(rank(rep->btl,node)*K*K),rep->basex[rep->iniq]%div_level,rep->basey[rep->iniq]+j*div_level,isallones);
					}
				}
			
			RemoveItem2(rep);
		}
		queuecont = conttmp;
	}
	while(rep->iniq<=rep->finq){
		posInf = rep->element[rep->iniq];
		isallones = rep->allones[rep->iniq]; 
		for(i=0;i<K;i++){
			if(isallones||bitget(rep->btl->data,posInf+(i*K+(y%K)))){
				totalAdyNodes++;
				rep->info[totalAdyNodes]=rep->basey[rep->iniq]+i;
			}
		}
		RemoveItem2(rep);
	}

	rep->info[0]=totalAdyNodes;
	return rep->info;
}

/*
void recursiveAdjacencyList(MREP * rep, uint node, uint basex, uint basey, uint level){
	uint posInf;
	int i, j,div_level,xrelat,newnode;
	if(level<rep->maxLevel){	
		div_level = exp_pow(K,rep->maxLevel-level);
		for(j=0;j<K;j++){
				xrelat = basey;
				newnode = xrelat/div_level*K + j;
				newnode += node;
				if(isBitSet(rep->btl,newnode)){
					if(level<rep->maxLevel-1)
						recursiveAdjacencyList(rep,rank(rep->btl,newnode)*K*K,basex+j*div_level,basey%div_level,level+1);
					else
						{
							posInf = rank(rep->btl,newnode)*K*K;
							for(i=0;i<K;i++){
								if(bitget(rep->btl->data,posInf+(i+(basey%K)*K))){
									rep->info[0]++;
									rep->info[rep->info[0]]=basex+i;
								}
							}
						}			
				}
		}
	}
}


uint * compact2AdjacencyList(MREP * rep, int x){

	rep->info[0]=0;
	recursiveAdjacencyList(rep,0,0,x,0);
	return rep->info;
	
}
*/
/* TODO FALTA POR ADAPTAR PARA UNOS COMPACTOS */

void recursiveRangeQuery(MREP * rep,uint p1, uint p2, uint q1, uint q2, uint dp, uint dq,uint x,int l);

void recursiveRangeQuery(MREP * rep,uint p1, uint p2, uint q1, uint q2, uint dp, uint dq,uint x,int l){

	uint posInf, leaf,i,j,y,p1new,p2new,q1new,q2new;

	int divlevel,newnode;

	divlevel = rep->div_level_table[l+1];

	if(l==rep->maxLevel){
		leaf = x+i*p1;
	
		if(bitget(rep->btl->data,leaf)){

			for(i=p1;i<=p2;i++){
				for(j=q1;j<=q2;j++){
					leaf=x+j;
					if(bitget(rep->btl->data,leaf)){
						(rep->info2)[0][0]=(rep->info2)[0][0]+1;
						(rep->info2)[0][(rep->info2)[0][0]]=dp+i;
						(rep->info2)[1][(rep->info2)[0][0]]=dq+j;

						//~ if ( (rep->info2)[0][0] >= MAX_INFO) {printf("MAX_INFO EXCEDIDO: %ul\n", (rep->info2)[0][0]); exit(1);}
						//~ if ( (rep->info2)[0][(rep->info2)[0][0]] < 0) {printf("INFO2 Deben ser LONG\n"); exit(1);}
						//~ if ( (rep->info2)[1][(rep->info2)[0][0]] < 0) {printf("INFO2 Deben ser LONG\n"); exit(1);}
						
					}
				}
				leaf+=K;
			}
		}
		return;
	}

	if((x==-1)||(bitget(rep->btl->data,x))){
 		y = (rank(rep->btl,x))*K*K;
		for(i=p1/divlevel;i<=p2/divlevel;i++){
			p1new=0;
			if(i==p1/divlevel)
				p1new=p1 % divlevel;
			p2new=divlevel-1;
			if(i==p2/divlevel)
				p2new=p2 % divlevel;
					
			for(j=q1/divlevel;j<=q2/divlevel;j++){
				q1new=0;
				if(j==q1/divlevel)
					q1new=q1 % divlevel;
		 				
				q2new=divlevel-1;
				if(j==q2/divlevel)
					q2new=q2 % divlevel;

			
				newnode = y+K*i+j;
				//newnode += x;
				recursiveRangeQuery(rep,p1new,p2new,q1new,q2new,dp + divlevel*i,dq+divlevel*j,y+K*i+j,l+1);	 
			}
		}
	}
	else{

		if(bitget(rep->bt2, rank0(rep->btl, x))&1){

			for(i=p1;i<=p2;i++){
				for(j=q1;j<=q2;j++){
				
					(rep->info2)[0][0]=(rep->info2)[0][0]+1;
					(rep->info2)[0][(rep->info2)[0][0]]=dp+i;
					(rep->info2)[1][(rep->info2)[0][0]]=dq+j;

						//~ if ( (rep->info2)[0][0] >= MAX_INFO) {printf("MAX_INFO EXCEDIDO: %ul\n", (rep->info2)[0][0]); exit(1);}
						//~ if ( (rep->info2)[0][(rep->info2)[0][0]] < 0) {printf("INFO2 Deben ser LONG\n"); exit(1);}
						//~ if ( (rep->info2)[1][(rep->info2)[0][0]] < 0) {printf("INFO2 Deben ser LONG\n"); exit(1);}


				}
			}			
		}
				

		return;
	}

/*
	int i,j,leaf;
	uint y, divlevel, p1new, p2new, q1new, q2new;
	if(l==rep->maxLevel){	
		leaf = x+i*p1;
		for(i=p1;i<=p2;i++){
			for(j=q1;j<=q2;j++){
				leaf=x+j;
		
				if(bitget(rep->btl->data,leaf)){
					(rep->info2)[0][0]=(rep->info2)[0][0]+1;
					(rep->info2)[0][(rep->info2)[0][0]]=dp+i;
					(rep->info2)[1][(rep->info2)[0][0]]=dq+j;
				}
			}
			leaf+=K;
		}
			
	}
		
		
	if(((l==rep->maxLevel-1)&&(bitget(rep->btl->data,x)))){
		y=(rank(rep->btl,x))*K*K;
		
		for(i=p1;i<=p2;i++){
 				for(j=q1;j<=q2;j++){
	 				recursiveRangeQuery(rep,0,0,0,0,dp + i,dq+j,y+K*i+j,l+1);	 		
 			}
 			}
 			

		
	}
 		if((x==-1)||((l<rep->maxLevel-1)&&(bitget(rep->btl->data,x)))){
 					y = (rank(rep->btl,x))*K*K;
 			
 			divlevel =	rep->div_level_table[l+1];
 			for(i=p1/divlevel;i<=p2/divlevel;i++){
 				p1new=0;
 				if(i==p1/divlevel)
 					p1new=p1 % divlevel;
 				p2new=divlevel-1;
 				if(i==p2/divlevel)
 					p2new=p2 % divlevel;
 				 				
 				for(j=q1/divlevel;j<=q2/divlevel;j++){
	 				q1new=0;
	 				if(j==q1/divlevel)
	 					q1new=q1 % divlevel;
	 				
	 				q2new=divlevel-1;
	 				if(j==q2/divlevel)
	 					q2new=q2 % divlevel;
	 				recursiveRangeQuery(rep,p1new,p2new,q1new,q2new,dp + divlevel*i,dq+divlevel*j,y+K*i+j,l+1);	 		
 			}
 			}
 			
 		}
 		*/
 	}
		
uint ** compactRangeQuery(MREP * rep, uint p1, uint p2, uint q1, uint q2){
	rep->info2[0][0]=0;
	recursiveRangeQuery(rep, p1,p2,q1,q2,0,0,-1,-1);
	return rep->info2;
	
}



/* for "getCell" queries; "p" is Y axis, "q" is X axis */
uint recursiveCheckLinkQuery(MREP * rep, uint p, uint q, uint node, uint level);
uint recursiveCheckLinkQuery(MREP * rep, uint p, uint q, uint node, uint level){
	uint posInf;

	int div_level,newnode;

	div_level = rep->div_level_table[level];
		
	newnode = p/div_level*K + q/div_level;
	newnode += node;
	//if there is a one, go down recursively unless next level is a leave
	if(isBitSet(rep->btl,newnode)){
			if(level<rep->maxLevel-1)
				return recursiveCheckLinkQuery(rep,p%div_level,q%div_level,rank(rep->btl,newnode)*K*K,level+1);
			else{
				posInf = (rank(rep->btl,newnode))*K*K;
				if(bitget(rep->btl->data,posInf+(q%K+(p%K)*K))){ 
					return 1;
				}
			return 0;				
			}
				
	}
	//BT2 contains a 1 for "full of ones" and 0 for "full o zeroes", 
	//no need to go further down
	return bitget(rep->bt2, rank0(rep->btl, newnode))&1;		
	
}


uint compact2CheckLinkQuery(MREP * rep, uint p, uint q){

	return recursiveCheckLinkQuery(rep,p,q,0,0);

	
}



/* TODO FALTA POR ADAPTAR PARA UNOS COMPACTOS */
uint recursiveCheckRangeQuery(MREP * rep,uint p1, uint p2, uint q1, uint q2, uint dp, uint dq,uint x,int l);

uint recursiveCheckRangeQuery(MREP * rep,uint p1, uint p2, uint q1, uint q2, uint dp, uint dq,uint x,int l){
	int i,j,leaf, result;
	uint y, divlevel, p1new, p2new, q1new, q2new;
	if(l==rep->maxLevel){	
		leaf = x+i*p1;
		for(i=p1;i<=p2;i++){
			for(j=q1;j<=q2;j++){
				leaf=x+j;
		
				if(bitget(rep->btl->data,leaf)){
					return 1;

				}
			}
			leaf+=K;
		}
		return 0;
			
	}
		
		
	if(((l==rep->maxLevel-1)&&(bitget(rep->btl->data,x)))){
		
		divlevel = rep->div_level_table[l+1];
		if((p1==0)&&(p2==0)&&(q1==divlevel-1)&&(q2==divlevel-1))
					return 1;
		y=rank(rep->btl,x)*K*K;
		
		for(i=p1;i<=p2;i++){
 				for(j=q1;j<=q2;j++){
	 				result = recursiveCheckRangeQuery(rep,0,0,0,0,dp + i,dq+j,y+K*i+j,l+1);	 		
 					if(result)
 						return 1;
 				}
 			}
 			return 0;
		
	}
	///else{ //internal node
 		if((x==-1)||((l<rep->maxLevel-1)&&(bitget(rep->btl->data,x)))){
				divlevel =	rep->div_level_table[l+1];
				if((p1==0)&&(p2==0)&&(q1==divlevel-1)&&(q2==divlevel-1))
					return 1;
 				y = (rank(rep->btl,x))*K*K;
 			
 			

 			for(i=p1/divlevel;i<=p2/divlevel;i++){
 				p1new=0;
 				if(i==p1/divlevel)
 					p1new=p1 % divlevel;

 				p2new=divlevel-1;
 				if(i==p2/divlevel)
 					p2new=p2 % divlevel;

 				 				
 				for(j=q1/divlevel;j<=q2/divlevel;j++){
	 				q1new=0;
	 				if(j==q1/divlevel)
	 					q1new=q1 % divlevel;

	 				
	 				q2new=divlevel-1;
	 				if(j==q2/divlevel)
	 					q2new=q2 % divlevel;

	 				result = recursiveCheckRangeQuery(rep,p1new,p2new,q1new,q2new,dp + divlevel*i,dq+divlevel*j,y+K*i+j,l+1);	 		

 					if(result)
 						return 1;
 			}
 			}
 			
 		}
 		return 0;
}



uint compactCheckRangeQuery(MREP * rep, uint p1, uint p2, uint q1, uint q2){
	return recursiveCheckRangeQuery(rep,p1,p2,q1,q2,0,0, -1, -1);

	
}

uint compactCheckLinkQuery(MREP * rep, uint p, uint q){

	uint posInf;
	uint prelat, qrelat;
	
	int i, node=0,div_level;

	for(i=0;i<rep->maxLevel;i++){

		div_level = rep->div_level_table[i];
		
		prelat = p/div_level;
		qrelat = q/div_level;

		node = node+prelat*K + qrelat;
		if(isBitSet(rep->btl,node)==0){
			return mybitget(rep->bt2, rank0(rep->btl, node))&&1;
		}
		node=rank(rep->btl,node)*K*K;
	}
	
	posInf = node;
	if(bitget(rep->btl->data,posInf+(q%K+(p%K)*K))){
		return 1;
	}
		
	return 0;
	
}			
	
void tablevel(int level) {
	int i;
	for (i = 0; i < level-2; i++) {
		printf("\t");
	}
}

void setParentAllOnes(NODE * node, int level, char valparent, char valgrand) {
	node->parentallones = valparent;
	node->grandallones = valgrand;
	if (node->child != NULL) {
		int i, kvalue;
		char newvalparent = node->isallones;
		char newvalgrand = node->parentallones;
		kvalue = K;
		for (i = 0; i < kvalue * kvalue; i++) {
			setParentAllOnes(node->child[i], level + 1, newvalparent, newvalgrand);
		}
	}
}

int nallones[20];

int checkIsAllOnes(NODE * node, int level) {
	int kvalue;
	if (level < max_Level) {
		int i;
		if (node->child == NULL) {
			node->isallones = 0;
			return 0;
		}

		node->isallones = 1;
		kvalue = K;
		for (i = 0; i < kvalue * kvalue; i++) {
			node->isallones = checkIsAllOnes(node->child[i], level + 1) && node->isallones;
		}
	} else {
		node->isallones = (node->data == 0xf);
	}
	return node->isallones;
}

#define R2(n)     n,     n + 2*64,     n + 1*64,     n + 3*64
#define R4(n) R2(n), R2(n + 2*16), R2(n + 1*16), R2(n + 3*16)
#define R6(n) R4(n), R4(n + 2*4 ), R4(n + 1*4 ), R4(n + 3*4 )

static const unsigned char flipByteTable[256] =
{
    R6(0), R6(2), R6(1), R6(3)
};


void flipBytes(uint * data, uint len) {
	unsigned char * bdata = (unsigned char *) data;
	int lengthbytes = (len + 7) / 8;
	int i;
	printf("flipping. len = %d, lenbytes = %d\n", len, lengthbytes);
	for (i = 0; i < lengthbytes; i++) {
		bdata[i] = flipByteTable[bdata[i]];
	}


}

NODE * createKTree(int maxlevels){
	NODE * n = (NODE *) malloc(sizeof(struct node));
	n->child=NULL;
	n->data=0;
	max_Level = maxlevels;
	numberNodes =0;
	numberLeaves = 0;
	numberTotalLeaves=0;
	return n;
}

MREP * createRepresentation(NODE * root, uint numberOfNodes,ulong numberOfEdges){
	checkIsAllOnes(root, 0);
	setParentAllOnes(root, 0, 0, 0);
	MREP * rep;
	rep = malloc(sizeof(struct matrixRep));
	rep->maxLevel = max_Level;
	rep->numberOfNodes = numberOfNodes;
	rep->numberOfEdges = numberOfEdges;
	uint bits_BTL_len = numberNodes+numberTotalLeaves+numberLeaves*K*K;
	bitRankW32Int *BTL,*BT2;
	uint * bits_BTL = (uint*)malloc(sizeof(uint)*((bits_BTL_len+W-1)/W));

	uint * bits_BT2 = (uint*) malloc(sizeof(uint) * ((bits_BTL_len + W - 1) / W));
	uint * bits_BT2_final = (uint*) malloc(sizeof(uint) * ((bits_BTL_len + W - 1) / W));

	uint bits_LI_len = numberLeaves*K*K;
	uint * bits_LI = (uint*)malloc(sizeof(uint)*((bits_LI_len+W-1)/W));


	uint i, k, j, queuecont, conttmp,node, pos=0;
	//uint div_level;
	
	for(i=0; i<(W-1+bits_BTL_len)/W;i++){
		bits_BTL[i]=0;
		bits_BT2[i] = 0;
		bits_BT2_final[i] = 0;
	}
		for(i=0; i<(W-1+bits_LI_len)/W;i++)
		bits_LI[i]=0;
		
	char isroot=1;
	QUEUECONS * q=NULL;
	finalQUEUECONS = q;
	q = AddItemCONS(q,root);
	queuecont = 1;
	for(i=0;i<max_Level;i++){
		conttmp = 0;
		//div_level = exp_pow(K,max_Level-i);

		for(k=0;k<queuecont;k++){


			if(q->element->child!=NULL){
				if (q->element->isallones)
					bitset(bits_BT2, pos);
				else {

				for(j=0;j<K*K;j++){
					node = j;

					conttmp++;

					q=AddItemCONS(q,q->element->child[node]);
					
					}
					if(!isroot){
						bitset(bits_BTL,pos);

				}
			}
			free(q->element->child);
			}

				
			if(!isroot)
				pos++;
				
			isroot=0;

			free(q->element);
			q = (QUEUECONS *)RemoveItemCONS(q);
		}
		queuecont = conttmp;
	}

	uint pos_inf=0;

	while(q!=NULL){
		if(((q->element)->data)){
			if (q->element->isallones)
				bitset(bits_BT2, pos);
			else {
				bitset(bits_BTL,pos);

				for(i=0;i<K*K;i++){
					if(((q->element)->data)&(0x1<<i)){
						bitset(bits_LI,pos_inf);
					}
					pos_inf++;
				}
			}
		}
		pos++;
				
		free(q->element);
		q = (QUEUECONS *)RemoveItemCONS(q);
	}

	BTL = createBitRankW32Int(bits_BTL, pos , 1, 20); 
	BT2 = createBitRankW32Int(bits_BT2, pos, 1, 20);

	int size_bt2_final = rank0(BTL, BTL->n);
	int cur=0;
	for (i = 0; i < BTL->n; i++) {
		if (!isBitSet(BTL, i)) {
			if (cur != rank0(BTL, i)) {
				printf("rank0 fails: for %d got %d, expected %d\n", i, rank0(BTL, i), cur);
			}
			if (isBitSet(BT2, i))
				bitset(bits_BT2_final, cur);
			cur++;
		}

	}


	rep->btl = BTL;
	rep->bt_len = pos;

	destroyBitRankW32Int(BT2);

	rep->bt2 = bits_BT2_final;
	rep->bt2_len = size_bt2_final;

	
	for(i=0;i<pos_inf;i++){
		if(bitget(bits_LI,i))
			bitset(bits_BTL,pos);
		pos++;
	}
	rep-> btl_len=pos;

	rep->info = NULL;
	rep->info2[0] = NULL;
	rep->info2[1] = NULL;
	rep->element = NULL;
	rep->basex = NULL;
	rep->basey = NULL;
	rep->div_level_table = NULL;
	rep->allones = NULL;

	

	return rep;
}

void insertNode(NODE * root, int x, int y){
	uint l=0, i,node, div_level;
	NODE * n = root;

	while(l<=max_Level){

		div_level = exp_pow(K,max_Level-l);

		node = (x / div_level)*K+y /div_level;
		if(l==max_Level){

			if(n->data==0){
				numberLeaves++;
			}
			n->data=n->data|(0x1<<node);//1;

		}
		else{
			if(n->child==NULL){

				if(l<max_Level-1)
					numberNodes+=K*K;
				else
					numberTotalLeaves+=K*K;

				n->child = (NODE **)malloc(sizeof(NODE *)*K*K);
				for(i=0;i<K*K;i++){
					n->child[i]=(NODE *) malloc(sizeof(struct node));
					n->child[i]->child=NULL;
					n->child[i]->data=0;
				}
			}

			n = n->child[node];
			
		} 
		

		x = x % div_level;
		y = y % div_level;
		l++;
	}
	
}




/*Funciones para convertir el k2-tree en una lista de adyacencia */
void recursiveDecompression(MREP * rep, uint * tmp_in, uint * tmp_out, ulong * tmp_p, ulong * sizein, ulong * pointerL, uint dp, uint dq,uint x,int l);

//Comparamos con rep->numNodes para no descomprimir nada fuera del tamaño válido del k2-tree1
//entre "numberOfNodes" y la potencia K^n a la que se expande la matriz para cerar el k2-tree1.
//Quizás no sería necesario, pero la implementación actual del complemento deja unos en la
//zona "de desbordamiento".
void recursiveDecompression(MREP * rep, uint * tmp_in, uint * tmp_out, ulong * tmp_p, ulong * sizein, ulong * pointerL, uint dp, uint dq,uint x,int l){
	int i,j;
	uint y, divlevel;
    
	//Si estamos fuera de la zona válida, salir sin más.
	if ((dp >= rep->numberOfNodes) || (dq >= rep->numberOfNodes))
		return;
	
	if(x==-1){ //Nodo RAIZ
 		//y = (rank(rep->btl,x))*K*K;
 		y = pointerL[l+1];
		pointerL[l+1]+=K*K;
 		divlevel =	rep->div_level_table[l+1];
		for(i=0;i<K;i++){
 			for(j=0;j<K;j++){
				recursiveDecompression(rep,tmp_in, tmp_out, tmp_p,sizein, pointerL, dp + divlevel*i,dq+divlevel*j,y+K*i+j,l+1);	 		
 			}
 		}
 	} else 	
	//HOJA con uno => añadir el uno
	if(l==rep->maxLevel){	
		if ((dp < rep->numberOfNodes) &&(dq < rep->numberOfNodes)){
			if(bitget(rep->btl->data,x)){
					tmp_in[*tmp_p]=dp;
					tmp_out[*tmp_p]=dq;
					sizein[dp]++;
					(*tmp_p)++;
				}
			}
	} else
	//zona "GRIS" penúltimo nivel: recursivo. Este if podría omitirse probablemente y usar el último cambiando la condición...	
	if(((l==rep->maxLevel-1)&&(get3valBit(rep,x)==1))){
		//y=rank(rep->btl,x)*K*K;
		y = pointerL[l+1];
		pointerL[l+1]+=K*K;
		for(i=0;i<K;i++){
 				for(j=0;j<K;j++){
					if ((dp+i<rep->numberOfNodes) &&(dq+j<rep->numberOfNodes))
						recursiveDecompression(rep,tmp_in, tmp_out, tmp_p,sizein,pointerL, dp + i,dq+j,y+K*i+j,l+1);	 		
 			}
		}
	} else
	//Más ZONA GRIS previo al penúltimo nivel
 	if((l<rep->maxLevel-1)&&(get3valBit(rep,x)==1)){
 		//y = (rank(rep->btl,x))*K*K;
 		y = pointerL[l+1];
		pointerL[l+1]+=K*K;
 		divlevel =	rep->div_level_table[l+1];
		for(i=0;i<K;i++){
 			for(j=0;j<K;j++){
				if ((dp + divlevel*i < rep->numberOfNodes) &&(dq+divlevel*j < rep->numberOfNodes))
					recursiveDecompression(rep,tmp_in, tmp_out, tmp_p,sizein,pointerL, dp + divlevel*i,dq+divlevel*j,y+K*i+j,l+1);	 		
 			}
 		}
 	} else 	
 	//Zona NEGRA: Hai que añadir una submatriz de unos.
	if((l<=rep->maxLevel-1)&&(get3valBit(rep,x)==2)){
 		divlevel =	rep->div_level_table[l];
 		//printf("X: %d (%d,%d), Level: %d, DIVLEVEL: %d\n",x, dp, dq, l, divlevel); //exit(9);
 		for(i=0;i<divlevel;i++){
 			for(j=0;j<divlevel;j++){
					if ((dp+i<rep->numberOfNodes) &&(dq+j<rep->numberOfNodes)){
						tmp_in[*tmp_p]=dp+i;
						tmp_out[*tmp_p]=dq+j;
						sizein[dp+i]++;
						(*tmp_p)++;
					}
			}
		}
	}

 	}
		
int * compactFullDecompression(MREP * rep){

	int i;
	
	//No edges? empty adjacency list.
	if (rep->numberOfEdges==0){
		int *lista = (int *) malloc(sizeof(int)*(rep->numberOfNodes));
		for (i=0;i<rep->numberOfNodes;i++)
			lista[i] = -(i+1);
		return lista;
	}
	
	//We have some edges. Decompress the tree and build the list
	ulong p = 0;
	uint * in = (uint *) malloc(sizeof(uint)*rep->numberOfEdges);
	uint * out = (uint *) malloc(sizeof(uint)*rep->numberOfEdges);
	ulong * sizein = (ulong *) malloc(sizeof(ulong)*(rep->numberOfNodes+1));

	for (i=0;i<rep->numberOfNodes;i++)
		sizein[i]=0;

	//Pointers for each level, to avoid using rank in recursiveDecompression
	ulong * pointerL = (ulong *) malloc(sizeof(ulong)*(rep->numberOfNodes+1));
	pointerL[0]=0;
	pointerL[1]=K*K;
	for (i=2;i<=rep->maxLevel;i++){
		pointerL[i]=(rank(rep->btl,pointerL[i-1]-1)+1)*K*K;
		
	}

	recursiveDecompression(rep, in, out, &p, sizein, pointerL, 0,0,-1,-1);
	
	int *lista = (int *) malloc(sizeof(int)*(rep->numberOfEdges+rep->numberOfNodes));

	ulong suma=0;
	ulong tmp = 0;
	for (i=0;i<rep->numberOfNodes;i++){
		tmp = sizein[i];
		lista[suma]=-(i+1);
		sizein[i]=suma+1;
		suma = suma + tmp+1;
	}

	sizein[rep->numberOfNodes] = suma;
	for(i=0;i<rep->numberOfEdges;i++){
		lista[sizein[in[i]]] = out[i]+1;
		sizein[in[i]]++;
	}

	return lista;
	
}



MREP * compactCreateKTree(uint * xedges, uint *yedges, uint numberOfNodes,ulong numberOfEdges, uint maxl){
	MREP * rep;
	rep = malloc(sizeof(struct matrixRep));
	rep->maxLevel = maxl;
	rep->numberOfNodes = numberOfNodes;
	rep->numberOfEdges = numberOfEdges;
	
	rep->div_level_table=0;
	rep->info=0;
	rep->info2[0]=0;
	rep->info2[1]=0;
	rep->element=0;
	rep->basex=0;
	rep->basey=0;

	ulong nedges = numberOfEdges;

	uint sizebt = 0;
	
	uint i,k,j,z,queuecont, conttmp,div_level;

	uint offsetL=0; 
	uint offsetR=0;

	uint postotal=0, postotal2=0;
	tedge * tedges =(tedge *) malloc(sizeof(tedge)*nedges);

	
	for(i=0;i<nedges;i++){
		tedges[i].xval=xedges[i];
		tedges[i].yval=yedges[i];
	}

	QUEUEOFFCONS * q = NULL;
	finalQUEUEOFFCONS = q;
	
	
	uint * counterK = (uint *)malloc(sizeof(uint)*K*K);
	uint * boundariesK = (uint *)malloc(sizeof(uint)*(K*K+1));
	uint * pointerK = (uint *)malloc(sizeof(uint)*(K*K));

	uint tempk,tempx,tempy;


	bitRankW32Int *BTL;
	bitRankW32Int *BT2;
	
//	printf("numberOfNodes: %d , edges: %d, maxl: %d\n",numberOfNodes, nedges,maxl);
	uint * bits_BTL=(uint *) malloc(sizeof(uint)*1);
	bits_BTL[0]=0;

	uint * bits_BT2=(uint *) malloc(sizeof(uint)*1);
	bits_BT2[0]=0;
	
	q = AddItemOFFCONS(q,0,nedges);
	queuecont = 1;
	for(i=0;i<maxl;i++){
		conttmp = 0;
		div_level = exp_pow(K,maxl-i);		
//		fprintf(stderr,"i %d maxl %d divlevel %d nodes: %d nedges %d, %d %d\n",i,maxl,div_level,numberOfNodes,nedges,postotal, postotal2);

		for(k=0;k<queuecont;k++){
				sizebt +=K*K;			
			
			offsetL = q->offsetL;
			offsetR = q->offsetR;
			

			for(j=0;j<K*K;j++){
				counterK[j]=0;
				pointerK[j]=0;
			}
				
			for(j=offsetL;j<offsetR;j++){
				tedges[j].kval= (tedges[j].xval / div_level)*K+tedges[j].yval /div_level;
				tedges[j].xval=tedges[j].xval%div_level;
				tedges[j].yval=tedges[j].yval%div_level;
				 
				counterK[tedges[j].kval]++;
			}
			
			boundariesK[0]=offsetL;			
			for(j=0;j<K*K;j++){
				boundariesK[j+1]=boundariesK[j]+counterK[j];
				pointerK[j]=boundariesK[j];

				if(boundariesK[j+1]!=boundariesK[j]){
					
					if(counterK[j]==div_level*div_level){
					
						bitset(bits_BT2,postotal2);
						postotal2++;
						bits_BT2 = (uint *) realloc(bits_BT2,(postotal2+1) * sizeof(int));
						bits_BT2[postotal2] = 0;
					
					}
					else{
						conttmp++;
						q=AddItemOFFCONS(q,boundariesK[j],boundariesK[j+1]);
				
						bitset(bits_BTL,postotal);
						
					}	
					
				}
				else{
					postotal2++;
					bits_BT2 = (uint *) realloc(bits_BT2,(postotal2+1) * sizeof(int));
					bits_BT2[postotal2] = 0;
				}
				postotal++;
				bits_BTL = (uint *) realloc(bits_BTL,(postotal+1) * sizeof(int));
				bits_BTL[postotal] = 0;

			}

			for(z=0;z<K*K;z++){
				//if(boundariesK[z+1]<boundariesK[z]+div_level*div_level){
				while(pointerK[z]<boundariesK[z+1]){
					if(tedges[pointerK[z]].kval!=z){
						tempk = tedges[pointerK[z]].kval;
						tempx = tedges[pointerK[z]].xval;
						tempy = tedges[pointerK[z]].yval;

						while(tedges[pointerK[tempk]].kval==tempk){
							pointerK[tempk]++;
						}
							
						tedges[pointerK[z]].kval = tedges[pointerK[tempk]].kval;
						tedges[pointerK[z]].xval = tedges[pointerK[tempk]].xval;
						tedges[pointerK[z]].yval = tedges[pointerK[tempk]].yval;

						tedges[pointerK[tempk]].kval= tempk;
						tedges[pointerK[tempk]].xval= tempx;
						tedges[pointerK[tempk]].yval= tempy;
						pointerK[tempk]++;
											

					}
					else
						pointerK[z]++;
					
				}
				//}
			}
			
			for(z=offsetL;z<offsetR-1;z++)
				if(tedges[z].kval>tedges[z+1].kval){
					fprintf(stderr,"divlevel: %d error: %d\n",div_level,z);
					exit(-1);
				}
				
			q = (QUEUEOFFCONS *)RemoveItemOFFCONS(q);
		}
		queuecont = conttmp;
	}


	rep->bt_len = postotal;
	rep->bt2_len = postotal2;
	uint counttotal=0;
	while(q!=NULL){
			
			offsetL = q->offsetL;
			offsetR = q->offsetR;

			for(j=0;j<K*K;j++){
				counterK[j]=0;
			}
			
			div_level = K;	
			for(j=offsetL;j<offsetR;j++){
				tedges[j].xval = tedges[j].xval%K;
				tedges[j].yval = tedges[j].yval%K;
				tedges[j].kval= tedges[j].xval *K+tedges[j].yval;
				counterK[tedges[j].kval]++;
			}
		
			
			for(j=0;j<K*K;j++){

				if(counterK[j]>0){
					conttmp++;
					counttotal++;

						bitset(bits_BTL,postotal);
				}
				postotal++;
				bits_BTL = (uint *) realloc(bits_BTL,(postotal+1) * sizeof(int));
				bits_BTL[postotal] = 0;
				
				if(counterK[j]>1){
					fprintf(stderr,"error\n");
					exit(-1);
				}

			}

		q = (QUEUEOFFCONS *)RemoveItemOFFCONS(q);
	}

	free(counterK);
	free(boundariesK);
	free(pointerK);
	free(tedges);
	rep->btl_len = postotal;

	uint bytes_data = sizeof( uint) *(rep->btl_len/W+1);
    uint * dataDefinitive = (uint *) malloc(bytes_data);
    memcpy(dataDefinitive, bits_BTL, bytes_data);
	free(bits_BTL);
	BTL = createBitRankW32Int(dataDefinitive, rep->bt_len , 1, 20); 
	
	//free(q);
	//free(bits2);
	//free(BT2);
	//free(bits_BT2);
	
	rep->btl = BTL;
	

	uint bytes_data_2 = sizeof( uint) *((rep->bt2_len+31)/32);
	uint * bt2_definitive = (uint *) malloc(bytes_data_2);
    memcpy(bt2_definitive, bits_BT2, bytes_data_2);
	free(bits_BT2);
	
	rep->bt2 = bt2_definitive;
			
	return rep;
}
