/*
 *  ktree_utils.h
 *  2014-11-26. Miguel Rodríguez Penabad
 *
 * Helper functions for K2Trees. 
 */

#ifndef KTREE_UTILS_H
#define KTREE_UTILS_H

#include "kTree.h"

/* ktree_display_info: Shows info (sizes and bitmaps) of a K2tree.  */
void ktree_display_info(MREP *tree);

/* ktree_empty: allocates memory, initializes values for a k2tree,
 * and returns it.  */
MREP * ktree_empty();


/* ktree_deep_copy: Performs a "deep copy" (including all elements, not
 * pointers) of a k2tree, and returns it.  */
MREP * ktree_deep_copy(MREP *src);


/*get3valBit: gets the "3-valued bit" value in the full bitmap.
 Possible return values are:
 0 (ZERO): empty subtree (all zeroes)
 1 (ONE): the subtree has some 1's
 2 (ALLONES): full subtree (all ones)
 */
int get3valBit(MREP *tree, int i);


/* isLeaf: Returns true if index i in tree is a leaf */
#define isLeaf(tree,  i) ( (i) >= ((tree)->bt_len) )

#endif
