#!/bin/bash

echo "Transformando Vandivia Mediano..."
mkdir OutputBin/ValdiviaMediano
for valores in 9 8 7 6 5 4 3 2 1 0
do
    mkdir OutputBin/ValdiviaMediano/ValdiviaMediano_${valores}
    ./K2TreeAccum/TransformarToBin Output/RasterRealesChile/ValdiviaMediano/ValdiviaMediano_${valores}/ValdiviaMediano_${valores} OutputBin/ValdiviaMediano/ValdiviaMediano_${valores}/ValdiviaMediano_${valores}.bin
done

echo "Transformando Vandivia Grande..."
mkdir OutputBin/ValdiviaGrande
for valores in 9 8 7 6 5 4 3 2 1 0
do
    mkdir OutputBin/ValdiviaGrande/ValdiviaGrande_${valores}
    ./K2TreeAccum/TransformarToBin Output/RasterRealesChile/ValdiviaGrande/ValdiviaGrande_${valores}/ValdiviaGrande_${valores} OutputBin/ValdiviaGrande/ValdiviaGrande_${valores}/ValdiviaGrande_${valores}.bin
done

echo "Transformando Zonal mdt..."
mkdir OutputBin/Zonal_mdt500
for valores in 10 50 100 200 500
do
    mkdir OutputBin/Zonal_mdt500/${valores}
    ./K2TreeAccum/TransformarToBin Output/Zonal_mdt500/${valores}/mdt500_${valores} OutputBin/Zonal_mdt500/${valores}/mdt500_${valores}.bin
done