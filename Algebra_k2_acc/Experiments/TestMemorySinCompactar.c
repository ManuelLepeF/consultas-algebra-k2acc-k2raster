#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "sys/types.h"
#include "sys/sysinfo.h"
#define FREAD(p,s,n,f) {int v = fread(p,s,n,f); if (v != n) {printf("FREAD failed!\n");exit(-1);}}

int get_memory_usage_kb(long* vmrss_kb, long* vmsize_kb)
{
    /* Get the the current process' status file from the proc filesystem */
    FILE* procfile = fopen("/proc/self/status", "r");

    long to_read = 8192;
    char buffer[to_read];
    int read = fread(buffer, sizeof(char), to_read, procfile);
    fclose(procfile);

    short found_vmrss = 0;
    short found_vmsize = 0;
    char* search_result;

    /* Look through proc status contents line by line */
    char delims[] = "\n";
    char* line = strtok(buffer, delims);

    while (line != NULL && (found_vmrss == 0 || found_vmsize == 0) )
    {
        search_result = strstr(line, "VmRSS:");
        if (search_result != NULL)
        {
            sscanf(line, "%*s %ld", vmrss_kb);
            found_vmrss = 1;
        }

        search_result = strstr(line, "VmSize:");
        if (search_result != NULL)
        {
            sscanf(line, "%*s %ld", vmsize_kb);
            found_vmsize = 1;
        }

        line = strtok(NULL, delims);
    }

    return (found_vmrss == 1 && found_vmsize == 1) ? 0 : 1;
}


int main(int argc, char ** argv) {
 
	FILE *fileMatriz;
	fileMatriz = fopen(argv[1], "r");
	int *matriz = malloc(atoi(argv[2])*atoi(argv[3])*sizeof(uint));
	FREAD(matriz, atoi(argv[2])*atoi(argv[2]), sizeof(uint), fileMatriz);
	fclose(fileMatriz);
    int ** matriz1 = (int **)malloc (atoi(argv[2]) *sizeof(int *));
    int aux = 0;
    for (int i = 0; i < atoi(argv[2]); i++){
        matriz1[i] = (int *)malloc (atoi(argv[3])*sizeof(int *));
        for (int j = 0; j < atoi(argv[3]); j++){
            matriz1[i][j] = matriz[aux];
            aux++;
        }
    }
	
    free(fileMatriz);
    free(matriz);

    long vmrss, vmsize;
    get_memory_usage_kb(&vmrss, &vmsize);
    printf("Sin compact Current memory usage: VmRSS = %6ld KB, VmSize = %6ld KB\n", vmrss, vmsize);
    
    return 0;

}
