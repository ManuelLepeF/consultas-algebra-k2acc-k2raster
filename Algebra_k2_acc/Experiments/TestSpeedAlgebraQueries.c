#include "../AlgebraQueries/algebraqueries.h"
#include <time.h>
#include <sys/time.h>
/* retorna "a - b" en segundos */
double timeval_diff(struct timeval *a, struct timeval *b)
{
  return
    (double)(a->tv_sec + (double)a->tv_usec/1000000) -
    (double)(b->tv_sec + (double)b->tv_usec/1000000);
}

int main(int argc, char ** argv) {

	if(argc < 5) {
		printf("Use: %s < Nombre Base Estructura 1 > < Nombre Base Estructura 2 > <Numero de consulta a ejecutar> <Iteraciones>\n", argv[0]);
		exit(1);
	}
    int opcion = atoi(argv[3]);
    int iteraciones = atoi(argv[4]);
    double promedio =0;
    struct timeval t_ini, t_fin;
    switch (opcion){
        case 1: 
            printf("Probando ThresHolding...\n");
            for (int i = 0; i < iteraciones; i++){
                SPK2TREEACUM * spOne = loadk2TreeAcum(argv[1]);
                double t = 0;
                printf("Ejecutando %d de 20...\n",i+1);
                gettimeofday(&t_ini, NULL);
                SPK2TREEACUM * spOut = ThresHolding(spOne,spOne->points[spOne->numberValues/2]);
                gettimeofday(&t_fin, NULL);
                t = timeval_diff(&t_fin, &t_ini);
                t *= 1000; // to milliseconds
                promedio +=t;
                printf("La ejecución %d termino en: %f\n",i+1,t);
                //printMatriz(spOut);
                destroyRepresentationK2Acc(spOne);
                destroyRepresentationK2Acc(spOut);
            }
            printf("La consulta del algebra se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)promedio/iteraciones);
            break;
        case 2: 
            printf("Probando suma por un escalar...\n");
            for (int i = 0; i < iteraciones; i++){
                SPK2TREEACUM * spOne = loadk2TreeAcum(argv[1]);
                double t = 0;
                printf("Ejecutando %d de 20...\n",i+1);
                gettimeofday(&t_ini, NULL);
                SPK2TREEACUM * spOut = SumScale(spOne,20);
                gettimeofday(&t_fin, NULL);
                t = timeval_diff(&t_fin, &t_ini);
                t *= 1000; // to milliseconds
                promedio +=t;
                printf("La ejecución %d termino en: %f\n",i+1,t);
                //printMatriz(spOut);
                destroyRepresentationK2Acc(spOne);
                destroyRepresentationK2Acc(spOut);
            }
            printf("La consulta del algebra se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)promedio/iteraciones);
            break;
        case 3:
            printf("Probando multiplicación por un escalar...\n");
            for (int i = 0; i < iteraciones; i++){
                SPK2TREEACUM * spOne = loadk2TreeAcum(argv[1]);
                double t = 0;
                printf("Ejecutando %d de 20...\n",i+1);
                gettimeofday(&t_ini, NULL);
                SPK2TREEACUM * spOut = MultiplicationScale(spOne,10);
                gettimeofday(&t_fin, NULL);
                t = timeval_diff(&t_fin, &t_ini);
                t *= 1000; // to milliseconds
                promedio +=t;
                printf("La ejecución %d termino en: %f\n",i+1,t);
                //printMatriz(spOut);
                destroyRepresentationK2Acc(spOne);
                destroyRepresentationK2Acc(spOut);
            }
            printf("La consulta del algebra se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)promedio/iteraciones);
            break;
        case 4:
            printf("Probando suma punto a punto...\n");
            for (int i = 0; i < iteraciones; i++){
                SPK2TREEACUM * spOne = loadk2TreeAcum(argv[1]);
                SPK2TREEACUM * spTwo = loadk2TreeAcum(argv[2]);
                double t = 0;
                printf("Ejecutando %d de 20\n",i+1);
                gettimeofday(&t_ini, NULL);
                SPK2TREEACUM * spOut = SumPointPoint(spOne,spTwo);
                gettimeofday(&t_fin, NULL);
                t = timeval_diff(&t_fin, &t_ini);
                t *= 1000; // to milliseconds
                promedio +=t;
                printf("La ejecución %d termino en: %f\n",i+1,t);
                printMatriz(spOut);
                destroyRepresentationK2Acc(spOne);
                destroyRepresentationK2Acc(spTwo);
                destroyRepresentationK2Acc(spOut);
            }
            
            printf("La consulta del algebra se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)promedio/iteraciones);
            break;
        case 5:
            printf("Probando suma Zonal...\n");
            for (int i = 0; i < iteraciones; i++){
                SPK2TREEACUM * spOne = loadk2TreeAcum(argv[1]);
                //printf("\nRaster Original: \n");
                //printMatriz(spOne);
                SPK2TREEACUM * spTwo = loadk2TreeAcum(argv[2]);
                //printf("\nRaster Zonal: \n");
                //printMatriz(spTwo);
                double t = 0;
                printf("Ejecutando %d de %d\n",i+1, iteraciones);
                gettimeofday(&t_ini, NULL);
                SPK2TREEACUM * spOut = ZonalSum(spOne,spTwo);
                gettimeofday(&t_fin, NULL);
                t = timeval_diff(&t_fin, &t_ini);
                t *= 1000; // to milliseconds
                promedio +=t;
                printf("La ejecución %d termino en: %f\n",i+1,t);
                //printMatriz(spOut);
                destroyRepresentationK2Acc(spOne);
                destroyRepresentationK2Acc(spTwo);
                destroyRepresentationK2Acc(spOut);
            }
            printf("La consulta del algebra se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)promedio/iteraciones);
            break;
        default: 
            printf("Opcion Incorrecta...\n");
    }

	return 0;
}