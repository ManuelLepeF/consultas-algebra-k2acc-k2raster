/* 
    Algortimos para las consultas del algebra sobre datos raster directos en memoria
    principal sin k2-acc.
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include "../AlgebraQueries/algebraqueries.h"

/* retorna "a - b" en segundos */
double timeval_diff(struct timeval *a, struct timeval *b)
{
  return
    (double)(a->tv_sec + (double)a->tv_usec/1000000) -
    (double)(b->tv_sec + (double)b->tv_usec/1000000);
}

bool Verificar(SPK2TREEACUM * spGetValue,SPK2TREEACUM * spPaper ){
    for (int i = 0; i < spPaper->rows; i++){
		for (int j = 0; j < spPaper->columns; j++){
			if (getValue(spGetValue,i,j) != getValue(spPaper,i,j))
                return 1;
		}
	}
    return 0;
}

int **  ThresHoldingNaive(int ** matrix, int sizeRows,int sizeColumns, int value){
	int ** matrixOut = (int **)malloc (sizeRows *sizeof(int *));
    for (int i = 0; i < sizeRows; i++){
        matrixOut[i] = (int *)malloc (sizeColumns *sizeof(int));
		for (int j = 0; j < sizeColumns; j++){
            if (matrix[i][j]>=value){
                matrixOut[i][j] = 1;
            }else{
                matrixOut[i][j] = 0;
            }
		}
	}
	return matrixOut;
}


int ** SumScaleNaive(int ** matrix, int sizeRows,int sizeColumns, int value){
    int ** matrixOut = (int **)malloc (sizeRows *sizeof(int *));
    for (int i = 0; i < sizeRows; i++){
        matrixOut[i] = (int *)malloc (sizeColumns *sizeof(int));
		for (int j = 0; j < sizeColumns; j++){
            matrixOut[i][j] = matrix[i][j]+value;
		}
	}
	return matrixOut;
}

int ** MultiplicationScaleNaive(int ** matrix, int sizeRows,int sizeColumns, int value){
    int ** matrixOut = (int **)malloc (sizeRows *sizeof(int *));
    for (int i = 0; i < sizeRows; i++){
        matrixOut[i] = (int *)malloc (sizeColumns *sizeof(int));
		for (int j = 0; j < sizeColumns; j++){
            matrixOut[i][j] = matrix[i][j]*value;
		}
	}
	return matrixOut;
}

int ** SumPointPointNaive (int ** matrixOne,int ** matrixTwo, int sizeRows,int sizeColumns){
    int ** matrixOut = (int **)malloc (sizeRows *sizeof(int *));
    for (int i = 0; i < sizeRows; i++){
        matrixOut[i] = (int *)malloc (sizeColumns *sizeof(int));
		for (int j = 0; j < sizeColumns; j++){
            matrixOut[i][j] = matrixOne[i][j]+matrixTwo[i][j];
		}
	}
	return matrixOut;
}

int ** SumZonalNaive(int ** matrizOriginal, int ** matrizZonal,int sizeRows,int sizeColumns){
    
    int * vectorAux = (int *)malloc(sizeRows*sizeColumns *sizeof(int *));
    for ( int i = 0; i < sizeRows*sizeColumns; i++){
        vectorAux[i] = 0;
    }

    for (int i = 0; i < sizeRows; i++){
        for (int j = 0; j < sizeColumns; j++){
            vectorAux[matrizZonal[i][j]] += matrizOriginal[i][j];
        }
    }

    int ** matrizOut = (int **)malloc (sizeRows*sizeof(int *));
    for (int i = 0; i < sizeRows; i++){
        matrizOut[i] = (int *)malloc (sizeColumns*sizeof(int *));
        for (int j = 0; j < sizeColumns; j++){
            if (matrizZonal[i][j]!=0){
                matrizOut[i][j] = vectorAux[matrizZonal[i][j]];
            }else{
                matrizOut[i][j] = 0;
            }
        }
    }

    free(vectorAux);
    return matrizOut;
}



int main(int argc, char ** argv) {

	if(argc < 5) {
		printf("Use: %s < Nombre Base Estructura 1 > < Nombre Base Estructura 2 > <Numero de consulta a ejecutar> <Cuantas Ejecutar en suma point points>\n", argv[0]);
		exit(1);
	}
    struct timeval t_ini, t_fin;
    int opcion = atoi(argv[3]);
    int iteraciones = atoi(argv[4]);
    double promedio =0;
    SPK2TREEACUM * spOne = loadk2TreeAcum(argv[1]);
    SPK2TREEACUM * spTwo = loadk2TreeAcum(argv[2]);
    int ** matriz1 = (int **)malloc (spOne->rows *sizeof(int *));
    int ** matriz2 = (int **)malloc (spOne->rows *sizeof(int *));
    for (int i = 0; i < spOne->rows; i++){
        matriz1[i] = (int *)malloc (spOne->columns *sizeof(int *));
        matriz2[i] = (int *)malloc (spOne->columns *sizeof(int *));
        for (int j = 0; j < spOne->columns; j++){
                matriz1[i][j] = getValue(spOne,i,j);
                matriz2[i][j] = getValue(spTwo,i,j);
        }
    }
    switch (opcion){
        case 1: 
            printf("Probando ThresHolding...\n");
            for (int i = 0; i < iteraciones; i++){
                double t = 0;
                printf("Ejecutando %d de 20...\n",i+1);
                gettimeofday(&t_ini, NULL);
                int * * matrixOut = ThresHoldingNaive(matriz1,spOne->rows,spOne->columns,spOne->points[spOne->numberValues/2]);
                gettimeofday(&t_fin, NULL);
                t = timeval_diff(&t_fin, &t_ini);
                promedio +=t;
                printf("La ejecución %d termino en: %f\n",i+1,t);
                for (int i = 0; i < spOne->rows; i++){
                    free(matrixOut[i]); 
                }
                free(matrixOut);
            }
            destroyRepresentationK2Acc(spOne);
            destroyRepresentationK2Acc(spTwo);
            printf("La consulta del algebra se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)(promedio/iteraciones));
            break;
        case 2: 
            printf("Probando suma por un escalar...\n");
            for (int i = 0; i < iteraciones; i++){
                double t = 0;
                printf("Ejecutando %d de 20...\n",i+1);
                gettimeofday(&t_ini, NULL);
                int  ** matrixOut = SumScaleNaive(matriz1,spOne->rows,spOne->columns,20);
                gettimeofday(&t_fin, NULL);
                t = timeval_diff(&t_fin, &t_ini);
                t *= 1000; // to milliseconds
                promedio +=t;
                printf("La ejecución %d termino en: %f\n",i+1,t);
                for (int i = 0; i < spOne->rows; i++){
                    free(matrixOut[i]); 
                }
                free(matrixOut);
            }
            destroyRepresentationK2Acc(spOne);
            destroyRepresentationK2Acc(spTwo);
            printf("La consulta del algebra se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)promedio/iteraciones);
            break;
        case 3:
            printf("Probando multiplicación por un escalar...\n");
            for (int i = 0; i < iteraciones; i++){
                double t = 0;
                printf("Ejecutando %d de 20...\n",i+1);
                gettimeofday(&t_ini, NULL);
                int ** matrixOut = MultiplicationScaleNaive(matriz1,spOne->rows,spOne->columns,20);
                gettimeofday(&t_fin, NULL);
                t = timeval_diff(&t_fin, &t_ini);
                t *= 1000; // to milliseconds
                promedio +=t;
                printf("La ejecución %d termino en: %f\n",i+1,t);
                for (int i = 0; i < spOne->rows; i++){
                    free(matrixOut[i]); 
                }
                free(matrixOut);
            }
            destroyRepresentationK2Acc(spOne);
            destroyRepresentationK2Acc(spTwo);
            printf("La consulta del algebra se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)promedio/iteraciones);
            break;
        case 4:
            printf("Probando suma punto a punto...\n");
            for (int i = 0; i < iteraciones; i++){
                double t = 0;
                printf("Ejecutando %d de 20\n",i+1);
                gettimeofday(&t_ini, NULL);
                int ** matrixOut = SumPointPointNaive(matriz1,matriz2,spOne->rows,spOne->columns);
                gettimeofday(&t_fin, NULL);
                t = timeval_diff(&t_fin, &t_ini);
                t *= 1000; // to milliseconds
                promedio +=t;
                printf("La ejecución %d termino en: %f\n",i+1,t);
                for (int i = 0; i < spOne->rows; i++){
                    free(matrixOut[i]); 
                }
                free(matrixOut);
            }
            destroyRepresentationK2Acc(spOne);
            destroyRepresentationK2Acc(spTwo);
            printf("La consulta del algebra se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)promedio/iteraciones);
            break;
        case 5:
            printf("Probando suma Zonal...\n");
            for (int i = 0; i < iteraciones; i++){
                double t = 0;
                printf("Ejecutando %d de 20\n",i+1);
                gettimeofday(&t_ini, NULL);
                int ** matrixOut = SumZonalNaive(matriz1,matriz2,spOne->rows,spOne->columns);
                gettimeofday(&t_fin, NULL);
                t = timeval_diff(&t_fin, &t_ini);
                t *= 1000; // to milliseconds
                promedio +=t;
                printf("La ejecución %d termino en: %f\n",i,t);    
                 for (int i = 0; i < spOne->rows; i++){
                    free(matrixOut[i]); 
                }
                free(matrixOut);
            }
            destroyRepresentationK2Acc(spOne);
            destroyRepresentationK2Acc(spTwo);
            printf("La consulta del algebra se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)promedio/iteraciones);
            break;
        default: // code to be executed if n doesn't match any cases
            printf("Opcion Incorrecta\n");
    }

	return 0;
}