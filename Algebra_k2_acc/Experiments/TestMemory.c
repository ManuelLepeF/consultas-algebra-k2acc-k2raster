#include "../AlgebraQueries/algebraqueries.h"
#include "sys/types.h"
#include "sys/sysinfo.h"

int get_memory_usage_kb(long* vmrss_kb, long* vmsize_kb)
{
    /* Get the the current process' status file from the proc filesystem */
    FILE* procfile = fopen("/proc/self/status", "r");

    long to_read = 8192;
    char buffer[to_read];
    int read = fread(buffer, sizeof(char), to_read, procfile);
    fclose(procfile);

    short found_vmrss = 0;
    short found_vmsize = 0;
    char* search_result;

    /* Look through proc status contents line by line */
    char delims[] = "\n";
    char* line = strtok(buffer, delims);

    while (line != NULL && (found_vmrss == 0 || found_vmsize == 0) )
    {
        search_result = strstr(line, "VmRSS:");
        if (search_result != NULL)
        {
            sscanf(line, "%*s %ld", vmrss_kb);
            found_vmrss = 1;
        }

        search_result = strstr(line, "VmSize:");
        if (search_result != NULL)
        {
            sscanf(line, "%*s %ld", vmsize_kb);
            found_vmsize = 1;
        }

        line = strtok(NULL, delims);
    }

    return (found_vmrss == 1 && found_vmsize == 1) ? 0 : 1;
}


int main(int argc, char ** argv) {
    
    SPK2TREEACUM * spOne = loadk2TreeAcum(argv[1]);
    long vmrss, vmsize;
    get_memory_usage_kb(&vmrss, &vmsize);
    printf("K2-Tree-Acum Current memory usage: VmRSS = %6ld KB, VmSize = %6ld KB\n", vmrss, vmsize);
    printf("Tamaño:\nFila: %d Columna: %d\nNúmero de valores distintos: %d\n",spOne->rows,spOne->columns,spOne->numberValues);
    destroyRepresentationK2Acc(spOne);

    return 0;
}


