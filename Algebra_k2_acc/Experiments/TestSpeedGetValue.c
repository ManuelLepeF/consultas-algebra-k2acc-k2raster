#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include "../AlgebraQueries/algebraqueries.h"

struct timeval t_ini, t_fin, t_fin_sin_Crear;

/* retorna "a - b" en segundos */
double timeval_diff(struct timeval *a, struct timeval *b){
  return
    (double)(a->tv_sec + (double)a->tv_usec/1000000) -
    (double)(b->tv_sec + (double)b->tv_usec/1000000);
}

/*
# Verifica si el resultado de getvalue es el mismo que los algoritmos del paper
bool Verificar(SPK2TREEACUM * spGetValue,SPK2TREEACUM * spPaper ){
    for (int i = 0; i < spPaper->rows; i++){
		for (int j = 0; j < spPaper->columns; j++){
			if (getValue(spGetValue,i,j) != getValue(spPaper,i,j))
                return 1;
		}
	}
    return 0;
}
*/

int findValuePoint(int * points, int indice, int value){
    int min = 0;
	int max = indice-1;
	int mid = -1;
	while(min <= max) {
		mid = (max + min) / 2;
        if (points[mid] == value){
            return mid;
        }else{
            if (points[mid] > value) {
			    max = mid -1;
		    } else {
			    min = mid + 1;
		    }
        }
	}
	return -1;
}

void insertionSortZonal(int * points, int n){
    int pointKey = points[n];
	int j = n - 1;

	while (j >= 0 && points[j] > pointKey) { 
		points[j + 1] = points[j];
		j = j - 1; 
	}
	points[j + 1] = pointKey;
}

void insertionSortPoints(int * points,int n){
    int pointKey = points[n];
	int j = n - 1;

	while (j >= 0 && points[j] > pointKey) { 
		points[j + 1] = points[j];
		j = j - 1; 
	}
	points[j + 1] = pointKey;

}

SPK2TREEACUM * ThresHoldingGetValue(SPK2TREEACUM * sp, int value){
	uint nNodes;
	if(sp->rows > sp->columns) 
        nNodes = sp->rows;
    else
        nNodes = sp->columns;
	uint *xedges = (uint *)malloc(sizeof(uint)*0);
	uint *yedges = (uint *)malloc(sizeof(uint)*0);
	uint cedg = 0;
	ulong nEdges = 0;

	for (int i = 0; i < sp->rows; i++){
		for (int j = 0; j < sp->columns; j++){
			if (getValue(sp,i,j)<value){
				nEdges ++;
				xedges = (uint *)realloc(xedges,sizeof(uint)*nEdges);
				yedges = (uint *)realloc(yedges,sizeof(uint)*nEdges);
				xedges[cedg] = i;
				yedges[cedg] = j;
				cedg ++;
			}
		}
	}

    gettimeofday(&t_fin_sin_Crear, NULL);

	uint max_level = floor(log(nNodes)/log(K));
	if(floor(log(nNodes)/log(K))==(log(nNodes)/log(K)))
		max_level=max_level-1;

	/* SE CREA K2-TREE-ACCUM DE SALIDA */ 
	SPK2TREEACUM * spOut = (SPK2TREEACUM *) malloc(sizeof(SPK2TREEACUM));
	spOut->numberValues = 2;
	spOut->rows = sp->rows;
	spOut->columns = sp->columns;
	spOut->points = (int *) malloc(spOut->numberValues * sizeof(int));
	spOut->points[0] = 0;
	spOut->points[1] = 1;
	spOut->vectorTrees = (MREP**) malloc(1*sizeof(MREP*));
	spOut->vectorTrees[0] = compactCreateKTree(xedges, yedges, nNodes,nEdges,max_level);
    spOut->vectorTrees[0]->div_level_table = (uint *)malloc(sizeof(uint)*(sp->vectorTrees[0]->maxLevel+1));
    for(int r=0;r<=sp->vectorTrees[0]->maxLevel;r++)
        spOut->vectorTrees[0]->div_level_table[r]=pow(K,sp->vectorTrees[0]->maxLevel-r);
    return spOut;
}


SPK2TREEACUM * SumScaleGetValue(SPK2TREEACUM * sp, int value){
    int * points = (int *) malloc(sp->numberValues * sizeof(int));
    uint nNodes;
	if(sp->rows > sp->columns) 
        nNodes = sp->rows;
    else
        nNodes = sp->columns;
	uint indice = 0;
    int ** matrizAux = (int **)malloc (sp->rows *sizeof(int *));
    for (int i = 0; i < sp->rows; i++){
        matrizAux[i] = (int *)malloc (sp->columns *sizeof(int));
		for (int j = 0; j < sp->columns; j++){
            matrizAux[i][j] = getValue(sp,i,j)+value;
            if (indice != sp->numberValues){
                int indiceFind = findValuePoint(points,indice,matrizAux[i][j]);
			    if (indiceFind<0){
                    points[indice] = matrizAux[i][j];
                    insertionSortPoints(points,indice);
                    indice ++;
                }
            }
		}
	}

    gettimeofday(&t_fin_sin_Crear, NULL);

    MREP ** vectorTrees = malloc((sp->numberValues-1)*sizeof(MREP*));
    for (int l = 0; l < sp->numberValues-1; l++){
        //printf("Creando k2-Tree: %d \n",l);
        uint *xedges = (uint *)malloc(sizeof(uint)*0);
	    uint *yedges = (uint *)malloc(sizeof(uint)*0);
        uint cedg = 0;
	    ulong nEdges = 0;
        for (int i = 0; i < sp->rows; i++){
            for (int j = 0; j < sp->columns; j++){
                if (matrizAux[i][j] <= points[l]){
                    nEdges ++;
				    xedges = (uint *)realloc(xedges,sizeof(uint)*nEdges);
				    yedges = (uint *)realloc(yedges,sizeof(uint)*nEdges);
				    xedges[cedg] = i;
				    yedges[cedg] = j;
				    cedg ++;
                }
            }
	    }
        uint max_level = floor(log(nNodes)/log(K));
	    if(floor(log(nNodes)/log(K))==(log(nNodes)/log(K)))
		    max_level=max_level-1;
        vectorTrees[l] = compactCreateKTree(xedges, yedges, nNodes,nEdges,max_level);
        vectorTrees[l]->div_level_table = (uint *)malloc(sizeof(uint)*(vectorTrees[l]->maxLevel+1));
        for(int r=0;r<=vectorTrees[l]->maxLevel;r++)
		    vectorTrees[l]->div_level_table[r]=pow(K,vectorTrees[l]->maxLevel-r);
        free(xedges);
        free(yedges);
    }
    for (int i = 0; i < sp->rows; i++){
        free(matrizAux[i]);
    }
    free(matrizAux);
    /* SE CREA K2-TREE-ACCUM DE SALIDA */
	SPK2TREEACUM * spOut = (SPK2TREEACUM *) malloc(sizeof(SPK2TREEACUM));
	spOut->numberValues = sp->numberValues;
	spOut->rows = sp->rows;
	spOut->columns = sp->columns;
	spOut->points = points;
	spOut->vectorTrees = vectorTrees;
    
	return spOut;
}


SPK2TREEACUM *  MultiplicationScaleGetValue (SPK2TREEACUM * sp, int value){
    int * points = (int *) malloc(sp->numberValues * sizeof(int));
    uint nNodes;
	if(sp->rows > sp->columns) 
        nNodes = sp->rows;
    else
        nNodes = sp->columns;
	uint indice = 0;
    int ** matrizAux = (int **)malloc (sp->rows *sizeof(int *));
    for (int i = 0; i < sp->rows; i++){
        matrizAux[i] = (int *)malloc (sp->columns *sizeof(int));
		for (int j = 0; j < sp->columns; j++){
            matrizAux[i][j] = getValue(sp,i,j)*value;
            if (indice != sp->numberValues){
                int indiceFind = findValuePoint(points,indice,matrizAux[i][j]);
			    if (indiceFind<0){
                    points[indice] = matrizAux[i][j];
                    insertionSortPoints(points,indice);
                    indice ++;
                }
            }
		}
	}

    gettimeofday(&t_fin_sin_Crear, NULL);
    MREP ** vectorTrees = malloc((sp->numberValues-1)*sizeof(MREP*));
    for (int l = 0; l < sp->numberValues-1; l++){
        //printf("Creando k2-Trees: %d \n",l);
        uint *xedges = (uint *)malloc(sizeof(uint)*0);
	    uint *yedges = (uint *)malloc(sizeof(uint)*0);
        uint cedg = 0;
	    ulong nEdges = 0;
        for (int i = 0; i < sp->rows; i++){
            for (int j = 0; j < sp->columns; j++){
                if (matrizAux[i][j] <= points[l]){
                    nEdges ++;
				    xedges = (uint *)realloc(xedges,sizeof(uint)*nEdges);
				    yedges = (uint *)realloc(yedges,sizeof(uint)*nEdges);
				    xedges[cedg] = i;
				    yedges[cedg] = j;
				    cedg ++;
                }
            }
	    }
        uint max_level = floor(log(nNodes)/log(K));
	    if(floor(log(nNodes)/log(K))==(log(nNodes)/log(K)))
		    max_level=max_level-1;
        vectorTrees[l] = compactCreateKTree(xedges, yedges, nNodes,nEdges,max_level);;
        free(xedges);
        free(yedges);
        vectorTrees[l]->div_level_table = (uint *)malloc(sizeof(uint)*(vectorTrees[l]->maxLevel+1));
        for(int r=0;r<=vectorTrees[l]->maxLevel;r++)
		    vectorTrees[l]->div_level_table[r]=pow(K,vectorTrees[l]->maxLevel-r);
    }

    for (int i = 0; i < sp->rows; i++){
        free(matrizAux[i]);
    }
    free(matrizAux);
    
    /* SE CREA K2-TREE-ACCUM DE SALIDA */
	SPK2TREEACUM * spOut = (SPK2TREEACUM *) malloc(sizeof(SPK2TREEACUM));
	spOut->numberValues = sp->numberValues;
	spOut->rows = sp->rows;
	spOut->columns = sp->columns;
	spOut->points = points;
	spOut->vectorTrees = vectorTrees;
    
	return spOut;
}


SPK2TREEACUM *  SumPointPointGetValue (SPK2TREEACUM * spOne, SPK2TREEACUM * spTwo){
    int * points = (int *) malloc(0 * sizeof(int));
    uint nNodes;
	if(spOne->rows > spOne->columns) 
        nNodes = spOne->rows;
    else
        nNodes = spOne->columns;
	uint indice = 0;
    int ** matrizAux = (int **)malloc (spOne->rows *sizeof(int *));
    for (int i = 0; i < spOne->rows; i++){
        matrizAux[i] = (int *)malloc (spOne->columns *sizeof(int));
		for (int j = 0; j < spOne->columns; j++){
            matrizAux[i][j] = getValue(spOne,i,j)+getValue(spTwo,i,j);
            int indiceFind = findValuePoint(points,indice,matrizAux[i][j]);
            if (indiceFind<0){
                points = (int *) realloc(points,(indice+1) * sizeof(int));
                points[indice] = matrizAux[i][j];
                insertionSortPoints(points,indice);
                indice ++;
            }
            
		}
	}

    gettimeofday(&t_fin_sin_Crear, NULL);

    MREP ** vectorTrees = malloc((indice-1)*sizeof(MREP*));
    for (int l = 0; l < indice-1; l++){
        //printf("Creando k2-Tree: %d \n",l);
        uint *xedges = (uint *)malloc(sizeof(uint)*0);
	    uint *yedges = (uint *)malloc(sizeof(uint)*0);
        uint cedg = 0;
	    ulong nEdges = 0;
        for (int i = 0; i < spOne->rows; i++){
            for (int j = 0; j < spOne->columns; j++){
                if (matrizAux[i][j] <= points[l]){
                    nEdges ++;
				    xedges = (uint *)realloc(xedges,sizeof(uint)*nEdges);
				    yedges = (uint *)realloc(yedges,sizeof(uint)*nEdges);
				    xedges[cedg] = i;
				    yedges[cedg] = j;
				    cedg ++;
                }
            }
	    }
        uint max_level = floor(log(nNodes)/log(K));
	    if(floor(log(nNodes)/log(K))==(log(nNodes)/log(K)))
		    max_level=max_level-1;
        vectorTrees[l] = compactCreateKTree(xedges, yedges, nNodes,nEdges,max_level);
        free(xedges);
        free(yedges);
        vectorTrees[l]->div_level_table = (uint *)malloc(sizeof(uint)*(vectorTrees[l]->maxLevel+1));
        for(int r=0;r<=vectorTrees[l]->maxLevel;r++)
		    vectorTrees[l]->div_level_table[r]=pow(K,vectorTrees[l]->maxLevel-r);
    }

    /* SE CREA K2-TREE-ACCUM DE SALIDA */
	SPK2TREEACUM * spOut = (SPK2TREEACUM *) malloc(sizeof(SPK2TREEACUM));
	spOut->numberValues = indice;
	spOut->rows = spOne->rows;
	spOut->columns = spOne->columns;
	spOut->points = points;
	spOut->vectorTrees = vectorTrees;
    
	return spOut;
}

SPK2TREEACUM *  ZonalSumGetValue(SPK2TREEACUM * spInput, SPK2TREEACUM * spZonal){
    
    int * pointsAux = (int *)malloc(spZonal->numberValues *sizeof(int *));
    for ( int i = 0; i < spZonal->numberValues; i++){
        pointsAux[i] = 0;
    }

    for (int i = 0; i < spInput->rows; i++){
        for (int j = 0; j < spInput->columns; j++){
            int position = getValue(spZonal,i,j);
            if (position!=0){
                pointsAux[position] += getValue(spInput,i,j);
            }
        }
    }

    int * pointsReal = (int *)malloc(spZonal->numberValues *sizeof(int *));
    for (int i = 0; i < spZonal->numberValues; i++){
        pointsReal[i] = pointsAux[i];
        insertionSortZonal(pointsReal,i);
    }

    uint nNodes;
	if(spZonal->rows > spZonal->columns) 
        nNodes = spZonal->rows;
    else
        nNodes = spZonal->columns;
    
    gettimeofday(&t_fin_sin_Crear, NULL);

    MREP ** vectorTrees = malloc((spZonal->numberValues-1)*sizeof(MREP*));
    vectorTrees[0] = copyKT(spZonal->vectorTrees[0]);
    for (int l = 1; l < spZonal->numberValues-1; l++){
        //printf("Creando k2-Tree: %d \n",l);
        uint *xedges = (uint *)malloc(sizeof(uint)*0);
	    uint *yedges = (uint *)malloc(sizeof(uint)*0);
        uint cedg = 0;
	    ulong nEdges = 0;
        for (int i = 0; i < spZonal->rows; i++){
            for (int j = 0; j < spZonal->columns; j++){
                if (pointsAux[getValue(spZonal,i,j)] <= pointsReal[l]){
                    nEdges ++;
				    xedges = (uint *)realloc(xedges,sizeof(uint)*nEdges);
				    yedges = (uint *)realloc(yedges,sizeof(uint)*nEdges);
				    xedges[cedg] = i;
				    yedges[cedg] = j;
				    cedg ++;
                }
            }
	    }
        uint max_level = floor(log(nNodes)/log(K));
	    if(floor(log(nNodes)/log(K))==(log(nNodes)/log(K)))
		    max_level=max_level-1;
        vectorTrees[l] = compactCreateKTree(xedges, yedges, nNodes,nEdges,max_level);
        vectorTrees[l]->div_level_table = (uint *)malloc(sizeof(uint)*(vectorTrees[l]->maxLevel+1));
        for(int r=0;r<=vectorTrees[l]->maxLevel;r++)
		    vectorTrees[l]->div_level_table[r]=pow(K,vectorTrees[l]->maxLevel-r);
        free(xedges);
        free(yedges);
    }
    free(pointsAux);
    /* SE CREA K2-TREE-ACCUM DE SALIDA */
	SPK2TREEACUM * spOut = (SPK2TREEACUM *) malloc(sizeof(SPK2TREEACUM));
	spOut->numberValues = spZonal->numberValues;
	spOut->rows = spZonal->rows;
	spOut->columns = spZonal->columns;
	spOut->points = pointsReal;
	spOut->vectorTrees = vectorTrees;
    return spOut;
}

int main(int argc, char ** argv) {

	if(argc < 4) {
		printf("Use: %s < Nombre Base Estructura 1 > < Nombre Base Estructura 2 > <Numero de consulta a ejecutar> <Iteraciones>\n", argv[0]);
		exit(1);
	}
    int opcion = atoi(argv[3]);
    int iteraciones = atoi(argv[4]);
    double promedio =0, promedio_sin_crear = 0;
    switch (opcion){
        case 1: 
            printf("Probando ThresHolding...\n");
            for (int i = 0; i < iteraciones; i++){
                SPK2TREEACUM * spOne = loadk2TreeAcum(argv[1]);
                double t = 0,t_sin=0;
                printf("Ejecutando %d de 20...\n",i+1);
                gettimeofday(&t_ini, NULL);
                SPK2TREEACUM * spOut = ThresHoldingGetValue(spOne,spOne->points[spOne->numberValues/2]);
                gettimeofday(&t_fin, NULL);
                t_sin = timeval_diff(&t_fin_sin_Crear, &t_ini);
                t_sin *=1000;
                t = timeval_diff(&t_fin, &t_ini);
                t *= 1000; // to milliseconds
                promedio +=t;
                promedio_sin_crear += t_sin;
                printf("La ejecución %d COMPLETA termino en: %f\n",i+1,t);
                printf("La ejecución %d SIN CREAR termino en: %f\n",i+1,t_sin);
                //printMatriz(spOut);
                destroyRepresentationK2Acc(spOne);
                destroyRepresentationK2Acc(spOut);
            }
            printf("La consulta del algebra se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)promedio/iteraciones);
            break;
        case 2: 
            printf("Probando suma por un escalar...\n");
            for (int i = 0; i < iteraciones; i++){
                SPK2TREEACUM * spOne = loadk2TreeAcum(argv[1]);
                double t = 0,t_sin=0;
                printf("Ejecutando %d de 20...\n",i+1);
                gettimeofday(&t_ini, NULL);
                SPK2TREEACUM * spOut = SumScaleGetValue(spOne,20);
                gettimeofday(&t_fin, NULL);
                t_sin = timeval_diff(&t_fin_sin_Crear, &t_ini);
                t_sin *=1000;
                t = timeval_diff(&t_fin, &t_ini);
                t *= 1000; // to milliseconds
                promedio +=t;
                promedio_sin_crear += t_sin;
                printf("La ejecución %d COMPLETA termino en: %f\n",i+1,t);
                printf("La ejecución %d SIN CREAR termino en: %f\n",i+1,t_sin);
                //printMatriz(spOut);
                destroyRepresentationK2Acc(spOne);
                destroyRepresentationK2Acc(spOut);
            }
            printf("La consulta del algebra se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)promedio/iteraciones);
            break;
        case 3:
            printf("Probando multiplicación por un escalar...\n");
            for (int i = 0; i < iteraciones; i++){
                SPK2TREEACUM * spOne = loadk2TreeAcum(argv[1]);
                double t = 0,t_sin=0;
                printf("Ejecutando %d de 20...\n",i+1);
                gettimeofday(&t_ini, NULL);
                SPK2TREEACUM * spOut = MultiplicationScaleGetValue(spOne,10); 
                gettimeofday(&t_fin, NULL);
                t_sin = timeval_diff(&t_fin_sin_Crear, &t_ini);
                t_sin *=1000;
                t = timeval_diff(&t_fin, &t_ini);
                t *= 1000; // to milliseconds
                promedio +=t;
                promedio_sin_crear += t_sin;
                printf("La ejecución %d COMPLETA termino en: %f\n",i+1,t);
                printf("La ejecución %d SIN CREAR termino en: %f\n",i+1,t_sin);
                //printMatriz(spOut);
                destroyRepresentationK2Acc(spOne);
                destroyRepresentationK2Acc(spOut);
            }
            printf("La consulta del algebra se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)promedio/iteraciones);
            break;
        case 4:
            printf("Probando suma punto a punto...\n");
            for (int i = 0; i < iteraciones; i++){
                SPK2TREEACUM * spOne = loadk2TreeAcum(argv[1]);
                SPK2TREEACUM * spTwo = loadk2TreeAcum(argv[2]);
                double t = 0,t_sin=0;
                printf("Ejecutando %d de 20\n",i+1);
                gettimeofday(&t_ini, NULL);
                SPK2TREEACUM * spOut = SumPointPointGetValue(spOne,spTwo); 
                gettimeofday(&t_fin, NULL);
                t_sin = timeval_diff(&t_fin_sin_Crear, &t_ini);
                t_sin *=1000;
                t = timeval_diff(&t_fin, &t_ini);
                t *= 1000; // to milliseconds
                promedio +=t;
                promedio_sin_crear += t_sin;
                printf("La ejecución %d COMPLETA termino en: %f\n",i+1,t);
                printf("La ejecución %d SIN CREAR termino en: %f\n",i+1,t_sin);
                printMatriz(spOut);
                destroyRepresentationK2Acc(spOne);
                destroyRepresentationK2Acc(spTwo);
                destroyRepresentationK2Acc(spOut);
            }
            
            printf("La consulta del algebra se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)promedio/iteraciones);
            break;
        case 5:
            printf("Probando suma Zonal...\n");
            for (int i = 0; i < iteraciones; i++){
                SPK2TREEACUM * spOne = loadk2TreeAcum(argv[1]);
                SPK2TREEACUM * spTwo = loadk2TreeAcum(argv[2]);
                double t = 0, t_sin=0;
                printf("Ejecutando %d de %d\n",i+1, iteraciones);
                gettimeofday(&t_ini, NULL);
                SPK2TREEACUM * spOut = ZonalSumGetValue(spOne,spTwo); 
                gettimeofday(&t_fin, NULL);
                t_sin = timeval_diff(&t_fin_sin_Crear, &t_ini);
                t_sin *=1000;
                t = timeval_diff(&t_fin, &t_ini);
                t *= 1000; // to milliseconds
                promedio +=t;
                promedio_sin_crear += t_sin;
                printf("La ejecución %d COMPLETA termino en: %f\n",i+1,t);
                printf("La ejecución %d SIN CREAR termino en: %f\n",i+1,t_sin);
                //printMatriz(spOut);
                destroyRepresentationK2Acc(spOne);
                destroyRepresentationK2Acc(spOut);
            }
            printf("La consulta del algebra COMPLETA se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)promedio/iteraciones);
            printf("La consulta del algebra SIN CREAR se ejecuto en: %f promedio de 20 ejecuciones...\n",(double)promedio_sin_crear/iteraciones);
            break;
        default:
            printf("Opcion Incorrecta...\n");
            
    }
	return 0;
}