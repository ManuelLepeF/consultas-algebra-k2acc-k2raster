# Consultas Álgebra de mapas K<sup>2</sup>-tree-Acc

Código Proyecto de investigación  donde se  implementan las consultas definidas por el álgebra de mapas sobre datos ráster almacenados en la estructura de datos compacta K<sup>2</sup>-tree-Acc.
# Compilar

Para compilar el código simplemente ubicarse en la raíz del proyecto y ejecutar en la terminal:

```bash
make
```
**Código compilado con gcc versión 7.4.0 (Ubuntu 7.4.0-1ubuntu1~18.04.1). De Preferencia compilar en ubuntu 18.04**




# Directorios
*  **K2_Tree_Basic :** Contiene el código fuente y ejecutables de la estructura base K2-tree
*  **K2TreeAccum :** Contiene el código fuente y ejecutables para construir la estructura K2-Tree-Acc.
*  **AlgebraQueries :** Contiene los algoritmos que responden a las consultas del álgebra de mapa sobre K2-Tree-Acc de manera eficiente.
*  **kt_setOperations :** Contiene los algoritmos que responden a las operaciones de conjunto (Unión, Intersección, diferencia, y complemento) sobre K2-Tree.
*  **ExperimentsResults :** Carpeta donde se guardaran los resultadosde los experimentos.
*  **Input :** Contienen los raster originales sin compactar (slices).
*  **Output :** Contienen los archivos para guardar la estructura K2-Tree-Acc de cada ráster. 
*  **Tools :** Contiene programas para generar raster con números random.
*  **Scripts_Crear:** Scripts para crear los raster desde archivos slices como input (se deben descargar), ejecutar desde la raiz.

# Ejecutables

## K2TreeAccum/buildfrommatriz
Ejecutable que permite crear una estructura K<sup>2</sup>-tree-Acc a partir de una matriz ejecutando el siguiente comando (desde la raiz del proyecto).

```bash
./K2TreeAccum/buildfrommatriz <Ruta matriz de entrada (.slices)> <Ruta donde se guardará la K2-tree-Acc en disco (debe ser la ruta donde esta el archivo .points)> <Números de filas> <Número de Columnas>
```

Para crear una estructura K2-tree-Acc se debe contar con 2 archivos: Un archivo binario con extensión .slices que contiene una matriz de valores enteros en el rango [0, max] y
un archivo .points. Por ejemplo si tenemos la siguiente matriz:


| 3 | 5 | 7 | 3 |
|---|---|---|---|
| 5 | 7 | 3 | 7 |
| 5 | 7 | 3 | 7 |
| 3 | 5 | 7 | 3 |

El archivo .slices quedaría de la siguiente forma (consultar los códigos de la carpeta Tools para obsevar como se crean):  
0 1 2 0 1 2 0 2 1 2 0 2 0 1 2 0

El archivo .points quedaría así:  
3 4 4 3 5 7  
Donde:
*  La primera posición se encuentra el número de valores distintos del raster (3).
*  La segunda y tercera posición contienen el número de filas y columnas respectivamente (4,4).
*  Las siguientes posiciones se encuentran los valores reales del raster ordenados de forma ascendente (3,5,7).

El archivo .points debe estar en la carpeta donde se guardará la K<sup>2</sup>-acc y tener el mismo nombre que se le dará a la K<sup>2</sup>-acc, para después cargar de forma correcta la  K<sup>2</sup>-acc a memoria principal.

Ejemplo de cómo ejecutar buildfrommatriz desde la raíz del proyecto:

```bash
./K2TreeAccum/buildfrommatriz Input/raster4/raster4.slices Output/raster4/raster4 4 4
```
## K2TreeAccum/PrintRasterFromK2acc
Ejecutable que permite imprimir por pantalla el raster utilizando el método GetValue para obtener el valor de cada celda.

```bash
./K2TreeAccum/PrintRasterFromK2acc <Ruta donde se encuentra guardada la K2-acc>
```

Ejemplo:
```bash
./K2TreeAccum/PrintRasterFromK2acc Output/raster4/raster4
```

## K2TreeAccum/testGetValue
Ejecutable que permite verificar si la estructura K<sup>2</sup>-acc se creó de forma correcta a partir del archivo .slices.

```bash
./K2TreeAccum/testGetValue <Ruta donde se encuentra guardada la K2-acc> <Ruta archivo .slices>
```
Ejemplo:

```bash
./K2TreeAccum/testGetValue Output/raster4/raster4 Input/raster4/raster4.slices
```
## Experiments/TestSpeedAlgebraQueries
Permite medir el tiempo que se demora cada consulta del álgebra utilizando los algoritmos del paper

```bash
./Experiments/TestSpeedAlgebraQueries <Ruta donde se encuentra guardada la K2-acc> <Ruta donde se encuentra guardada la K2-acc el segundo k2-acc (en el caso de suma zonal debe ser un K2-acc que represente las zonas> <Número de consulta a ejecutar> <Cuantas ejecuciones se harán>
```
El número de consulta puede ser:
1.  ThresHolding (el número por el cual se hará ThresHolding corresponde al número que se encuentra en la mitad del arreglo points (es un número que aprece en la matriz) revisar código para mas detalles)
2.  Suma por un escalar (el escalar que se sumará es 20, modificar código para cambiar)
3.  Multiplicación por un escalar (el escalar que se multiplicará es 10, modificar código para cambiar)
4.  Suma punto a punto
5.  Suma Zonal

Ejemplo donde se ejecutará 20 veces la consulta Suma por un escalar, para que el programa nos entregue un promedio de velocidad en milisegundos:
```bash
./Experiments/TestSpeedAlgebraQueries Output/raster4/raster4 No_Necesita 2 20
```
## Experiments/TestSpeedGetValue
Permite medir el tiempo que se demora cada consulta del algebra utilizando la función GetValue por cada celda (Estrategia Naive). Se ejecuta igual que la anterior.

```bash
./Experiments/TestSpeedAlgebraQueries <Ruta donde se encuentra guardada la K2-acc> <Ruta donde se encuentra guardada la K2-acc el segundo k2-acc (en el caso de suma zonal debe ser un K2-acc que represente las zonas> <Número de consulta a ejecutar> <Cuantas ejecuciones se harán>
```

Ejemplo:
```bash
./Experiments/TestSpeedGetValue Output/raster4/raster4 Output/raster4Zonal/raster4Zonal 5 20
```
# Descargar raster utilizados 

Link para descargar Input y Output (estructuras creadas en k2-acc) utilizados en los experimentos con esta estructura: [Descargar](https://drive.google.com/drive/folders/1auctXPcHXPVz_FZUQdfTS6GB-KZlhTlk?usp=sharing)

# Scripts

## CreateRasterAndK2TreeAcc.sh

Script para generar un ráster con números random (uniforme) y luego construir una estructura  K<sup>2</sup>-acc a partir del raster generado.

```bash
./CreateRasterAndK2TreeAcc.sh <Numero de filas> <Numero de columnas> <Numero de Valores distintos> <Intervalo de valores> <Nombre raster>
```
El intervalo de valores define el rango en que se encontraran los valores. Por ejemplo si es 100 los valores se encontrarn en el intervalo [0,100]

Ejemplo:
```bash
chmod u+x CreateRasterAndK2TreeAcc.sh
./CreateRasterAndK2TreeAcc.sh 10 10 10 100 prueba
```

## CreateRasterZonalAndK2TreeAcc.sh

Scripts para generar un ráster que representen zonas randomicas y luego construir una estructura  K<sup>2</sup>-tree-Acc a partir de este.

```bash
./CreateRasterZonalAndK2TreeAcc.sh <Numero de filas> <Numero de columnas> <Numero de Zonas> <Nombre raster>
```

Ejemplo:
```bash
chmod u+x CreateRasterZonalAndK2TreeAcc.sh
./CreateRasterZonalAndK2TreeAcc.sh 10 10 10 pruebaZona
```

## RunExperiments.sh

Script que permite realizar todos los experimentos con un raster pasado por argumento. Los resultados quedan guardados en ExperimentsResults/mdt500 por cada consulta.

```bash
chmod u+x RunExperiments.sh
./RunExperiments.sh <Nombre estructura k2-tree-Acc> <Nombre estructura k2-tree-Acc Zonal> <Numero de iteraciones (ejecuciones)>
```
Ojo que las estructuras  K<sup>2</sup>-tree-Acc deben estar guardadas en un carpeta con el mismo nombre dentro de la carpeta Output. Ejemplo:

```bash
./RunExperiments.sh raster4 raster4Zonal 1
```


# Scripts que permiten replicar los resultados del paper.

Para poder ejecutar estos scripts se deben descargar los archivos de las estructuras k2-acc ya creadas, o bien descargar los .slices y .points y crear las estructuras ejecutando buildfrommatriz o los scripts (desde la raíz) que se encuentran en la carpeta Scripts_Crear.

Descargar k2-acc ya creadas (Descargar carpeta Outuput y remplazar a la actual por la descargada: 
Descragar .slices y .point (Descargar carpeta Input y Output y reemplazar a las actuales por las descargadas):


## RunExperimentsMdt500

Script que permite replicar todos los experimentos utilizando el ráster real MDT500.

```bash
chmod u+x RunExperimentsMdt500.sh
./RunExperimentsMdt500.sh
```

## RunExperimentsSantiagoThresHolding

Script que permite replicar los experimentos de la consulta ThresHolding utilizando los raster de Santiago.

```bash
chmod u+x RunExperimentsSantiagoThresHolding.sh
./RunExperimentsSantiagoThresHolding.sh
```

## RunExperimentsSantiagoSumScale

Script que permite replicar los experimentos de la consulta Suma por un escalar utilizando los raster de Santiago.

```bash
chmod u+x RunExperimentsSantiagoSumScale.sh
./RunExperimentsSantiagoSumScale.sh
```

## RunExperimentsSantiagoSantiagoSumPointPoint.sh

Script que permite replicar los experimentos de la consulta Suma punto a punto  utilizando los raster de Santiago con Santiago.

```bash
chmod u+x RunExperimentsSantiagoSantiagoSumPointPoint.sh
./RunExperimentsSantiagoSantiagoSumPointPoint.sh
```
## RunExperimentsSantiagoValdiviaSumPointPoint.sh

Script que permite replicar los experimentos de la consulta Suma punto a punto  utilizando los raster de Santiago con Valdivia.

```bash
chmod u+x RunExperimentsSantiagoValdiviaSumPointPoint.sh
./RunExperimentsSantiagoValdiviaSumPointPoint.sh
```
## RunExperimentsSantiagoSumZonal.sh

Script que permite replicar los experimentos de la consulta SumaZonal utilizando el raster de Santiago chico
```bash
chmod u+x RunExperimentsSantiagoSumZonal.sh
./RunExperimentsSantiagoSumZonal.sh
```

