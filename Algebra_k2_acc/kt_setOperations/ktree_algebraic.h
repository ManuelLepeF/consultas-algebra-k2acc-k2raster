/*
 *  ktree_algebraic.h
 *  
 * Miguel Rodríguez Penabad
 *
 * Functions to perform algebraic operations on k2trees 
 * using compression of ones.
 */

#ifndef KTREE_ALGEBRAIC_H
#define KTREE_ALGEBRAIC_H
#include "../K2_Tree_Basic/kTree.h"
#include "bitmaparray.h"


#define bitcomplement(e,p) (bitget((e),p)? bitclean((e), p): bitset((e), p))

//en bitmaparray.h
//typedef enum {UNION, INTERSECTION, DIFFERENCE, SIMMETRIC_DIFFERENCE} binary_op;



/* ktree_complement: returns a new K2Tree that is the complement of
 * the given one (t) */
MREP *ktree_complement(MREP *t);

/*Binary operation (union, intersecion, difference) as described in
 * the original algorithm*/
MREP *ktree_binary_operation(MREP *ta, MREP *tb, binary_op operation);

/* Binary operations: helper definitions to invoke ktree_binary_operation */
#define ktree_union(ta, tb)  ktree_binary_operation((ta), (tb), UNION)
#define ktree_intersection(ta, tb)  ktree_binary_operation((ta), (tb), INTERSECTION)
#define ktree_difference(ta, tb)  ktree_binary_operation((ta), (tb), DIFFERENCE)
#define ktree_simmdiff(ta, tb)  ktree_binary_operation((ta), (tb), SIMMETRIC_DIFFERENCE)

#endif
