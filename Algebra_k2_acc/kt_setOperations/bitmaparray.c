/*
 *  bitmaparray.c
 *  Miguel Rodríguez Penabad
 *
 *  Helper functions to perform algebraic operations on k2trees 
 *  using compression of ones. These functions manage an array
 *  of uncompressed bitmaps T and T'.
 */

#include "../K2_Tree_Basic/kTree.h" 
#include "../K2_Tree_Basic/ktree_utils.h" 
#include "bitmaparray.h"
#include "math.h"

/**********************************************************************/
/* rank0 in bitrankw32int.c does not work correctly (it lacks "+1")*/
/* bma_rank0 stands for bitmap array rank0 */
uint bma_rank0(bitRankW32Int * br, ulong i) {
	return i + 1 - rank(br, i);
}



/*******************************************************************/
bitmap_array * bitmap_array_init(uint height, ulong numNodes, bitmap_ptr_array pA, bitmap_ptr_array pB, binary_op operation){
	bitmap_array *b = malloc(sizeof(bitmap_array));
	if (!b){printf("ERROR ASIGNANDO MEMORIA\n"); exit(-5);}
	b->height = height;
	b->numEdges = 0;
	b->numNodes = numNodes;
	b->levels = (bitmap_level *) malloc(sizeof(bitmap_level)*(b->height));
	if (!b->levels){printf("ERROR ASIGNANDO MEMORIA\n"); exit(-5);}
	int i;
	
	ulong t_size, tprime_size;
	ulong abits_t, abits_tprime, bbits_t, bbits_tprime;
       
	//Level 0 has no previous positions
	t_size = sizeof (unsigned int) * ((K*K/W)+1);
	tprime_size = sizeof (unsigned int) * ((K*K/W)+1);

	b->levels[0].t=(unsigned int*)malloc(t_size);
	b->levels[0].tsize=t_size;
	b->levels[0].idx.tpos = 0;
	b->levels[0].tprime=(unsigned int*)malloc(tprime_size);
	b->levels[0].tprimesize=tprime_size;
	b->levels[0].idx.tprimepos = 0;


	t_size=1;
	tprime_size=1;

	for (i=1; i < b->height; i++){
		//max theoretical size in this level
		//ulong size = (unsigned long)(sizeof(uint)*(exp_pow(K*K, i)/W)+8); //FIXME!!! +8 should be +1 but...
		//printf("%d^%d %d  Nivel %d => %ld\n", K*K, i, W, i, size);
		
		//Bits in T and T' for k2trees a and b.
		abits_t = pA[i].tpos-pA[i-1].tpos;
		abits_tprime = pA[i].tprimepos-pA[i-1].tprimepos;
		bbits_t = pB[i].tpos-pB[i-1].tpos;
		bbits_tprime = pB[i].tprimepos-pB[i-1].tprimepos;
		
		switch(operation){
//			case UNION:
			default:
					//t_size=(sizeof(uint)*(abits_t+bbits_t)/W)*K+1;
					t_size=sizeof(uint) * ((abits_t+bbits_t)/W + 1);
					//tprime_size=(sizeof(uint)*(abits_tprime+bbits_tprime)/W)*K+1;
					//tprime_size=sizeof(uint) * ((abits_tprime+bbits_tprime)/W + 1);
					tprime_size = t_size * K*K;

					//printf("Nivel %d => %ld en T, %ld en T' \n", i, t_size, tprime_size);
					break;
		}
			
		b->levels[i].t = (unsigned int *) malloc(t_size);
		b->levels[i].tsize=t_size;
		//printf("\n t_size: %d\n",t_size);
		if (!b->levels[i].t){
			printf("ERROR ASIGNANDO MEMORIA a t\n");
			exit(-6);
		}
		if (i < b->height -1) {
			b->levels[i].tprime = (unsigned int *) malloc(tprime_size);
			b->levels[i].tprimesize = tprime_size;
			if (!b->levels[i].tprime){printf("ERROR ASIGNANDO MEMORIA a tprime\n"); exit(-7);}
		} else{
			b->levels[i].tprime = NULL;
			b->levels[i].tprimesize=0;
		}
		b->levels[i].idx.tpos = 0;
		b->levels[i].idx.tprimepos = 0;
	}
	//printf(">>>>>>>>>>>>>>>>>>>>>"); int aa; scanf("%d",&aa);
	return b;
}

/*******************************************************************/
void bitmap_array_destroy(bitmap_array *b){
	uint i;
	for (i=0; i < b->height; i++){
		free(b->levels[i].t);
		//printf("Nivel %d: Liberado T ", i);
		free(b->levels[i].tprime);
		//printf(", Liberado Tprime\n"); fflush(stdout);
	}
	free(b->levels);
	free(b);
	b = NULL;
}

/*******************************************************************/
void bitmap_array_addvalue(bitmap_array *b, uint level, uint value){
	//printf("Añadiendo bit en posición %d de nivel %d \n", b->levels[level].idx.tpos, level);
	
	if (b->levels[level].idx.tpos >= b->levels[level].tsize * 8 -1){
		//printf("realloc para T. Level %d tpos %lu tsize %lu\n", level, b->levels[level].idx.tpos, b->levels[level].tsize); 
		b->levels[level].t =(unsigned int *)  realloc(b->levels[level].t, b->levels[level].tsize*K);
		if (b->levels[level].t == NULL) {
			printf("Error reasingando memoria para T\n"); 
			exit(-11);
		}
		b->levels[level].tsize *= K;
	}
	if ((value==ONE) && (level < b->height -1) && (b->levels[level].idx.tprimepos >= b->levels[level].tprimesize * 8 -1)){
	//	printf("Hai que reasignar memoria para T prime. Level %d tprimepos %lu tprimesize %lu\n", level, b->levels[level].idx.tprimepos, b->levels[level].tprimesize); 
		b->levels[level].tprime = (unsigned int *) realloc(b->levels[level].tprime, b->levels[level].tprimesize*K);
		if (b->levels[level].tprime == NULL) {
	//		printf("Error reasingando memoria para T prime. Level %d tprimepos %lu tprimesize %lu\n", level, b->levels[level].idx.tprimepos, b->levels[level].tprimesize); 
			exit(-11);
		}
		b->levels[level].tprimesize *= K;
	}

	if (value == ONE){
		bitset(b->levels[level].t, b->levels[level].idx.tpos); 
		if (level == b->height - 1)
			b->numEdges++;
		
	} else {
		//printf("bitmap_array value level %d tpos %lu size %lu\n", level,b->levels[level].idx.tpos, b->levels[level].tsize); fflush(stdout); 
		bitclean(b->levels[level].t, b->levels[level].idx.tpos); 
		if (level < b->height - 1){
			if (value==ALLONES){
				bitset(b->levels[level].tprime, b->levels[level].idx.tprimepos);
				b->numEdges += exp_pow(K*K, (b->height - 1) - level);
			} else { //ZERO
				bitclean(b->levels[level].tprime, b->levels[level].idx.tprimepos);
			}
			(b->levels[level].idx.tprimepos)++;
		}
	}
	(b->levels[level].idx.tpos)++;
}

/*******************************************************************/
void bitmap_array_print(bitmap_array *b){
	printf("Bitmap_Array:\n");
	printf("Height: %d\tnumNodes: %d\tnumEdges: %d\n", b->height, b->numNodes, b->numEdges);
	unsigned long i,j;
	for (i = 0; i < b->height; i++){
		printf("Level %d (%d|%d) \nT:", i, b->levels[i].idx.tpos, b->levels[i].idx.tprimepos);
		for (j = 0; j < b->levels[i].idx.tpos; j++){
			 printf("%d", bitget(b->levels[i].t, j));
			 if (((j+1)%4)==0) printf(" ");
		 }
		printf("  |  ");
		for (j = 0; j < b->levels[i].idx.tprimepos; j++) {
			printf("%d", bitget(b->levels[i].tprime, j));
			 if (((j+1)%4)==0) printf(" ");
		 }
		printf("\n");
	}
}

/*******************************************************************/
bitmap_ptr_array bitmap_ptr_array_build(MREP *tree){
	bitmap_ptr_array p = malloc(sizeof(bitmap_ptr)*(tree->maxLevel+1));
	if (!p){printf("ERROR ASIGNANDO MEMORIA\n"); exit(-5);}
	int level=0, i;

	p[0].tpos = 0; p[0].tprimepos=0;
	ulong current=0;
	
	//Initialize in case not all levels are used because of empty/full subtrees.
	//initially used -1, changed to 0 to use ulong
	for(i=1; i <= tree->maxLevel ; i++){
		p[i].tpos = 0; //-1;
		p[i].tprimepos = 0; //-1;
	}
	
	int done = 0;
	ulong rankval=0, prevrankval, nodesToProcess;

	while (!done){
		if (level == 0){
			nodesToProcess = 1;
		} else{
			prevrankval = rankval;
			rankval = rank(tree->btl, current-1);
			nodesToProcess = rankval - prevrankval;
			p[level].tpos = current;
			p[level].tprimepos = current - rankval;
		} 
		current += K*K*nodesToProcess;
		
		if ((level == tree->maxLevel) || !nodesToProcess)
			done = 1;
		level++;
	}
	return p;
}

/**********************************************************************/
void bitmap_ptr_array_destroy(bitmap_ptr_array p){
	free(p);
}

/**********************************************************************/
void bitmap_ptr_array_print(bitmap_ptr_array p, int maxLevel){
	int i;
	printf("Bitmap pointers:\n");
	for(i=0 ; i <= maxLevel ; i++)
		printf("p[%d].tpos=%d\tp[%d].tprimepos=%d\n", i, p[i].tpos, i, p[i].tprimepos);
}


/**********************************************************************/
int getBitInLevel(MREP *tree, bitmap_ptr_array p, int level){

		int b = (isBitSet(tree->btl, p[level].tpos) != 0) ;
		if (p[level].tpos < tree->bt_len){
			if (b==0) {
				if (bitget(tree->bt2, p[level].tprimepos)) 
					b= 2; /*ALLONES=2*/
				p[level].tprimepos++;
			}
		}
		//printf("getBitInLevel(level %d), pos %d, value %d\n", level, p[level].tpos, b);
		p[level].tpos++;
		return b;		
}

/**********************************************************************/
void SkipNodes(MREP *tree, bitmap_ptr_array p, int level, ulong numnodes){
	if ((p[level].tpos == 0)  && (level>0))
		return; /*No bitmap for this level and following*/
	ulong newpos = p[level].tpos + numnodes*K*K-1;
	ulong num_ones=0, num_zeroes=0;
	if (level < tree->maxLevel){
		num_ones = rank(tree->btl, newpos) - rank(tree->btl, p[level].tpos - 1);
		num_zeroes = rank0(tree->btl, newpos) - rank0(tree->btl, p[level].tpos - 1);
	}
	p[level].tpos = newpos+1;
	p[level].tprimepos += num_zeroes;
	if ( (num_ones > 0) && (level < tree->maxLevel))
		SkipNodes(tree, p, level+1, num_ones);
}

/**********************************************************************/
void CopySubtree(bitmap_array *C, MREP *tree, bitmap_ptr_array p, int level, ulong numnodes){

	if (p[level].tpos == -1) 
		return; /*No bitmap for this level and following*/

	ulong newpos = p[level].tpos + numnodes*K*K-1;
	ulong num_ones=0;
	if (level < tree->maxLevel)
		num_ones = rank(tree->btl, newpos) - rank(tree->btl, p[level].tpos - 1);
	int i;
	//bitmap_array_addvalue and getBitInLevel also increment the pointers
	//of C and p respectively
	for(i = p[level].tpos; i <= newpos; i++)
	  bitmap_array_addvalue(C, level, getBitInLevel(tree, p, level));
	if ( (num_ones > 0) && (level < tree->maxLevel))
		CopySubtree(C, tree, p, level+1, num_ones);

	return;
}

/**********************************************************************/
void CopyComplementedSubtree(bitmap_array *C, MREP *tree, bitmap_ptr_array p, int level, ulong numnodes){

	if ((p[level].tpos == 0) && (level>0))
		return; /*No bitmap for this level and following*/

	ulong newpos = p[level].tpos + numnodes*K*K-1;
	ulong num_ones=0;
	if (level < tree->maxLevel)
		num_ones = rank(tree->btl, newpos) - rank(tree->btl, p[level].tpos - 1);
	ulong i;
	//bitmap_array_addvalue and getBitInLevel also increment the pointers
	//of C and p respectively
	for(i = p[level].tpos; i <= newpos; i++){
		int bit = getBitInLevel(tree, p, level);
		if (bit==ALLONES)
			bit = ZERO;
		else if (bit==ZERO) {
			if (level == tree->maxLevel)
				bit = ONE;
			else 
				bit = ALLONES;
		} else { //bit == ONE is complemented only for leaves, "gray areas" remain the same.
			if (level == tree->maxLevel)
				bit = ZERO;
		}
		bitmap_array_addvalue(C, level, bit);
	}
	if ( (num_ones > 0) && (level < tree->maxLevel))
		CopyComplementedSubtree(C, tree, p, level+1, num_ones);

	return;
}
/**********************************************************************/
MREP *ktree_from_bitmap_array(bitmap_array *C){
	MREP *dest = ktree_empty();
	int i;

	//Bitmap lengths
	for (i=0; i < C->height; i++){
		dest->bt_len = dest->btl_len; //Excludes last level (leaves)
		dest->btl_len += C->levels[i].idx.tpos;
		dest->bt2_len += C->levels[i].idx.tprimepos;
	}

	uint * bt2_data = (uint*) malloc(sizeof(uint) * (dest->bt2_len+W-1) / W);
	if (!bt2_data){printf("ERROR ASIGNANDO MEMORIA ktree_from_bitmap_array\n"); exit(-5);}
	uint * btl_data = (uint*) malloc(sizeof(uint) * (dest->btl_len+W-1) / W);
	if (!btl_data){printf("ERROR ASIGNANDO MEMORIA ktree_from_bitmap_array\n"); exit(-5);}
	dest->numberOfNodes = C->numNodes;
	dest->numberOfEdges = C->numEdges;
	dest->maxLevel = C->height - 1;
	dest->div_level_table = (uint *)malloc(sizeof(uint)*(dest->maxLevel+1));
	if (!dest->div_level_table){printf("ERROR ASIGNANDO MEMORIA ktree_from_bitmap_array\n"); exit(-5);}
	for(i=0;i <= dest->maxLevel;i++)
		dest->div_level_table[i]=exp_pow(K,dest->maxLevel-i);


	ulong nedges = 0;

		
	int lev=0;
	ulong curr=0;
	for (lev=0; lev < C->height; lev++){
		for (i=0; i < C->levels[lev].idx.tpos; i++){
			if (bitget(C->levels[lev].t, i))
				bitset(btl_data, curr);
			else
				bitclean(btl_data, curr);
			curr++;
			if ((bitget(C->levels[lev].t, i)) && (lev == C->height -1)) nedges++;
		}	
	}

	//Bitrank structure only for "bt_len" bits.
	dest->btl = createBitRankW32Int(btl_data, dest->bt_len, 1, 20);
	
	curr=0;
	//tprime does not have entries in the last level
	for (lev=0; lev < C->height - 1; lev++){
		for (i=0; i < C->levels[lev].idx.tprimepos; i++){
			if (bitget(C->levels[lev].tprime, i))
				bitset(bt2_data, curr);
			else
				bitclean(bt2_data, curr);
			curr++;
			if (bitget(C->levels[lev].tprime, i)) nedges+=exp_pow(K*K, C->height - 1 - lev);
		}
	}
	dest->bt2 = bt2_data;
	return dest;
}
