/*
 *  bitmaparray.c
 *  Miguel Rodríguez Penabad
 *
 *  Helper functions to perform algebraic operations on k2trees 
 *  using compression of ones. These functions manage an array
 *  of uncompressed bitmaps T and T', as defined in 
 *  [gdebernardo 2014 phd thesis]
 * 
 */
 
#ifndef BITMAP_ARRAY_H
#define BITMAP_ARRAY_H


typedef enum {UNION, INTERSECTION, DIFFERENCE, SIMMETRIC_DIFFERENCE, COMPLEMENT} binary_op;

#include "../K2_Tree_Basic/kTree.h" 

/* Used to build bitmap results (no bitranks here) */

typedef struct _bitmap_ptr{
	ulong tpos, tprimepos; //int=>ulong to avoid errors on big trees
} bitmap_ptr;

typedef bitmap_ptr * bitmap_ptr_array;

typedef struct _bitmap_level{
	uint *t;
	uint *tprime;
	ulong tsize, tprimesize; //current size, might be used to add memory on demand.
	bitmap_ptr idx;
} bitmap_level;


//Used to store (level by level) the result of set operations
//Note: last level = leaves, so tprime and tprimepos will not be used.
//level[x].idx.tpos/tprimepos point to the first empty index and serves
//also as "length" of bitmaps on level x
typedef struct _bitmap_array{
	bitmap_level *levels;
	uint height; //height of the tree including leaves
	ulong numEdges;
	ulong numNodes;
} bitmap_array;

/* Create an empty bitmap_array of "height" levels (including leaves)
 * trying to minimize memory consumption
 */
bitmap_array * bitmap_array_init(uint height, ulong numNodes, bitmap_ptr_array pA, bitmap_ptr_array pB, binary_op operation);

/*Frees all associated memory*/
void bitmap_array_destroy(bitmap_array *b);

/*Print contents, just to check */
void bitmap_array_print(bitmap_array *b);

/* Adds a "3-valued bit" in current position of level of array b 
 * and increases current position */
void bitmap_array_addvalue(bitmap_array *b, uint level, uint value);

/* Build the array of pointers so that each level 
 * poits to the first bit in btl and bt2 */
bitmap_ptr_array bitmap_ptr_array_build(MREP *tree);

/*Frees all associated memory*/
void bitmap_ptr_array_destroy(bitmap_ptr_array p);

/* Displays content of pointers, just to check*/
void bitmap_ptr_array_print(bitmap_ptr_array p, int maxLevel);

/* Gets 3-valued bit of tree (given level, current position of this
 * level in p). It also incements the position in the pointer*/
int getBitInLevel(MREP *tree, bitmap_ptr_array p, int level);

/* Adjusts the pointers in all necessary levels of 'p' to skip
 * 'numnodes' nodes of 'tree' that don't need to be processed */
void SkipNodes(MREP *tree, bitmap_ptr_array p, int level, ulong numnodes);

/* Copies (adds) to 'C' a subtree of 'numnodes' nodes of 'tree' 
 * and adjusts the pointers in 'p' */
void CopySubtree(bitmap_array *C, MREP *tree, bitmap_ptr_array p, int level, ulong numnodes);

/* Copies (adds) to 'C' the complement of a subtree of 'numnodes' nodes of 'tree' 
 * and adjusts the pointers in 'p' (for the difference operation) */
void CopyComplementedSubtree(bitmap_array *C, MREP *tree, bitmap_ptr_array p, int level, ulong numnodes);


/* Builds a k2tree1 from the bitmap_array  */
MREP *ktree_from_bitmap_array(bitmap_array *C);

#endif
