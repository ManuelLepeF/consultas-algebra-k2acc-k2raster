/*
 *  ktree_algebraic.c
 *  Miguel Rodríguez Penabad
 *
 *  Functions to perform algebraic operations on k2trees 
 *  using compression of ones.
 */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "../K2_Tree_Basic/kTree.h"
#include "../K2_Tree_Basic/bitrankw32int.h"
#include "../K2_Tree_Basic/basic.h"
#include "../K2_Tree_Basic/ktree_utils.h"
#include "ktree_algebraic.h"


/*
 * ktree_complement: 
 * Returns a K2tree that is the complement of src.
 * It first creates a copy and then modifies it to complement the k2tree.
 * 
 * We need to modify only bt2 and leaves (btl from index position bt_len)
 * (convert white areas on black ones and viceversa, and complement 
 *  individual cells)
 */
MREP *ktree_complement(MREP *t){
	if (!t)
		return (MREP *) NULL;

	MREP * tree = ktree_deep_copy(t);
	
	//Update the number of edges
	tree->numberOfEdges = tree->numberOfNodes*tree->numberOfNodes - tree->numberOfEdges;

    //Complement only bt2 and leaves
    unsigned long i;
	for (i=0; i < tree->bt2_len; i++)
		bitcomplement(tree->bt2, i);
	for (i=tree->bt_len; i < tree->btl_len; i++){
		bitcomplement(tree->btl->data, i);
	}
	
	return tree;
}



/*********************************************************************/
/* Recursive union operation of two k2trees.                         */
/* It MUST be called from ktree_binary_operation (after initializing */
/* all pointers */
int rec_union(MREP *ta, MREP *tb, bitmap_array *C, bitmap_ptr *pA, bitmap_ptr *pB, int level){
	int sw=0, full=1;
	
	int i, bitA, bitB, t[K*K];

	for(i=0; i < K*K; i++){
		t[i]=0;
		
		bitA = getBitInLevel(ta, pA, level);
		bitB = getBitInLevel(tb, pB, level);
		//printf("Level %d values %d %d\n", level, bitA, bitB);
		if (level < ta->maxLevel){
			if ((bitA==0) && (bitB==0)){
				t[i] = 0;
			} else if ((bitA==0) && (bitB==1)){
				t[i] = 1;
				CopySubtree(C, tb, pB, level + 1, 1);
			} else if ((bitA==0) && (bitB==2)){
				t[i]=2;
			} else if ((bitA==1) && (bitB==0)){
				t[i] = 1;
				CopySubtree(C, ta, pA, level + 1, 1);
			} else if ((bitA==1) && (bitB==1)){
				t[i] = rec_union(ta, tb, C, pA, pB, level + 1);
			} else if ((bitA==1) && (bitB==2)){
				t[i]=2;
				SkipNodes(ta, pA, level + 1, 1);
			} else if ((bitA==2) && (bitB==0)){
				t[i]=2;
			} else if ((bitA==2) && (bitB==1)){
				t[i]=2;
				SkipNodes(tb, pB, level + 1, 1);
			} else if ((bitA==2) && (bitB==2)){
				t[i]=2;
			} 
			full = full && (t[i]==ALLONES);
		} else {
			t[i] = max(bitA, bitB); // For the union, the "OR" or 3-valued bits is max
			full = full && t[i];
		}
		sw = sw || t[i];
		//according to algorithm, increase pointers. It is done in getBitInLevel
	}
	if (sw){
			if (full && (level!=0))
				return ALLONES;
			else {
				for(i=0; i < K*K; i++){
					//printf("Adding level %d value %d\n", level, t[i]);
					bitmap_array_addvalue(C, level, t[i]);
				}
			}
	}
	return sw;
}

/*********************************************************************/
/* Recursive intersection operation of two k2trees.                  */
/* It MUST be called from ktree_binary_operation (after initializing */
/* all pointers */
int rec_intersection(MREP *ta, MREP *tb, bitmap_array *C, bitmap_ptr *pA, bitmap_ptr *pB, int level){
	int sw=0, full=1;
	
	int i, bitA, bitB, t[K*K];

	for(i=0; i < K*K; i++){
		t[i]=0;
		
		bitA = getBitInLevel(ta, pA, level);
		bitB = getBitInLevel(tb, pB, level);
		//printf("Level %d values %d %d\n", level, bitA, bitB);
		if (level < ta->maxLevel){
			if ((bitA==0) && (bitB==0)){
				t[i] = 0;
			} else if ((bitA==0) && (bitB==1)){
				t[i] = 0;
				SkipNodes(tb, pB, level + 1, 1);
			} else if ((bitA==0) && (bitB==2)){
				t[i]=0;
			} else if ((bitA==1) && (bitB==0)){
				t[i] = 0;
				SkipNodes(ta, pA, level + 1, 1);
			} else if ((bitA==1) && (bitB==1)){
				t[i] = rec_intersection(ta, tb, C, pA, pB, level + 1);
			} else if ((bitA==1) && (bitB==2)){
				t[i]=1;
				CopySubtree(C, ta, pA, level + 1, 1);
			} else if ((bitA==2) && (bitB==0)){
				t[i]=0;
			} else if ((bitA==2) && (bitB==1)){
				t[i]=1;
				CopySubtree(C, tb, pB, level + 1, 1);
			} else if ((bitA==2) && (bitB==2)){
				t[i]=2;
			} 
			full = full && (t[i]==ALLONES);
		} else {
			t[i] = min(bitA, bitB); // For the intersection, the "AND" or 3-valued bits is the min
			full = full && t[i];
		}
		sw = sw || t[i];
		//according to algorithm, increase pointers. It is done in getBitInLevel
	}
	if (sw){
			if (full && (level!=0))
				return ALLONES;
			else {
				for(i=0; i < K*K; i++){
					//printf("Adding level %d value %d\n", level, t[i]);
					bitmap_array_addvalue(C, level, t[i]);
				}
			}
	}
	return sw;
}

/*********************************************************************/
/* Recursive difference operation of two k2trees.                  */
/* It MUST be called from ktree_binary_operation (after initializing */
/* all pointers */
int rec_difference(MREP *ta, MREP *tb, bitmap_array *C, bitmap_ptr *pA, bitmap_ptr *pB, int level){
	int sw=0, full=1;
	
	int i, bitA, bitB, t[K*K];

	for(i=0; i < K*K; i++){
		t[i]=0;
		
		bitA = getBitInLevel(ta, pA, level);
		bitB = getBitInLevel(tb, pB, level);
		//printf("Level %d values %d %d\n", level, bitA, bitB);
		if (level < ta->maxLevel){
			if ((bitA==0) && (bitB==0)){
				t[i] = 0;
			} else if ((bitA==0) && (bitB==1)){
				t[i] = 0;
				SkipNodes(tb, pB, level + 1, 1);
			} else if ((bitA==0) && (bitB==2)){
				t[i]=0;
			} else if ((bitA==1) && (bitB==0)){
				t[i] = 1;
				CopySubtree(C, ta, pA, level + 1, 1);
			} else if ((bitA==1) && (bitB==1)){
				t[i] = rec_difference(ta, tb, C, pA, pB, level + 1);
			} else if ((bitA==1) && (bitB==2)){
				t[i]=0;
				SkipNodes(ta, pA, level + 1, 1);
			} else if ((bitA==2) && (bitB==0)){
				t[i]=2;
			} else if ((bitA==2) && (bitB==1)){
				t[i]=1;
				CopyComplementedSubtree(C, tb, pB, level + 1, 1);
			} else if ((bitA==2) && (bitB==2)){
				t[i]=0;
			} 
			full = full && (t[i]==ALLONES);
		} else {
			t[i] = bitA && (!bitB); // Difference = intersection with complement
			full = full && t[i];
		}
		sw = sw || t[i];
		//according to algorithm, increase pointers. It is done in getBitInLevel
	}
	if (sw){
			if (full && (level!=0))
				return ALLONES;
			else {
				for(i=0; i < K*K; i++){
					//printf("Adding level %d value %d\n", level, t[i]);
					bitmap_array_addvalue(C, level, t[i]);
				}
			}
	}
	return sw;
}


/*********************************************************************/
/* Recursive simmetric difference operation of two k2trees.                  */
/* It MUST be called from ktree_binary_operation (after initializing */
/* all pointers */
int rec_simmdiff(MREP *ta, MREP *tb, bitmap_array *C, bitmap_ptr *pA, bitmap_ptr *pB, int level){
	int sw=0, full=1;
	
	int i, bitA, bitB, t[K*K];

	for(i=0; i < K*K; i++){
		t[i]=0;
		
		bitA = getBitInLevel(ta, pA, level);
		bitB = getBitInLevel(tb, pB, level);
		//printf("Level %d values %d %d\n", level, bitA, bitB);
		if (level < ta->maxLevel){
			if ((bitA==0) && (bitB==0)){
				t[i] = 0;
			} else if ((bitA==0) && (bitB==1)){
				t[i] = 1;
				CopySubtree(C, tb, pB, level + 1, 1);
			} else if ((bitA==0) && (bitB==2)){
				t[i]=2;
			} else if ((bitA==1) && (bitB==0)){
				t[i] = 1;
				CopySubtree(C, ta, pA, level + 1, 1);
			} else if ((bitA==1) && (bitB==1)){
				t[i] = rec_simmdiff(ta, tb, C, pA, pB, level + 1);
			} else if ((bitA==1) && (bitB==2)){
				t[i]=1;
				CopyComplementedSubtree(C, ta, pA, level + 1, 1);
			} else if ((bitA==2) && (bitB==0)){
				t[i]=2;
			} else if ((bitA==2) && (bitB==1)){
				t[i]=1;
				CopyComplementedSubtree(C, tb, pB, level + 1, 1);
			} else if ((bitA==2) && (bitB==2)){
				t[i]=0;
			} 
			full = full && (t[i]==ALLONES);
		} else {
			t[i] = bitA ^ bitB; // Simmetric Difference = EXOR operation
			full = full && t[i];
		}
		sw = sw || t[i];
		//according to algorithm, increase pointers. It is done in getBitInLevel
	}
	if (sw){
			if (full && (level!=0))
				return ALLONES;
			else {
				for(i=0; i < K*K; i++){
					//printf("Adding level %d value %d\n", level, t[i]);
					bitmap_array_addvalue(C, level, t[i]);
				}
			}
	}
	return sw;
}



/******************************************************************/
/* Not listed in ktree_algebraic.h because is not "exported"      */
/* Performs the indicated binary operations on the source k2trees */
MREP *ktree_binary_operation(MREP *ta, MREP *tb, binary_op operation){
	//Basic checks first
	if (ta==NULL) return ktree_deep_copy(tb);
	if (tb==NULL) return ktree_deep_copy(ta);
	
	if (ta->numberOfNodes != tb->numberOfNodes){
		fprintf(stderr, "Trees have diferent number of nodes.\n");
		return (MREP *) NULL;
	}

	bitmap_ptr_array pA, pB;
	
	pA = bitmap_ptr_array_build(ta); 
	pB = bitmap_ptr_array_build(tb);

	//printf("Número de niveles : %d\n", ta->maxLevel+1);
	bitmap_array *C = bitmap_array_init(ta->maxLevel+1, ta->numberOfNodes, pA, pB, operation);
	//printf("bitmap_array_init finalizado\n\n");


	//bitmap_ptr_array_print(pA, ta->maxLevel); exit(0);

	switch (operation){
		case UNION: 
					rec_union(ta, tb, C, pA, pB,0); 
					//printf("Unión recursiva finalizada\n\n");
					break;
		case INTERSECTION: 
					rec_intersection(ta, tb, C, pA, pB,0); 
					break;
		case DIFFERENCE: 
					rec_difference(ta, tb, C, pA, pB,0); 
					break;
		case SIMMETRIC_DIFFERENCE: 
					rec_simmdiff(ta, tb, C, pA, pB,0); 
					break;
	}
	
	//Translate bitmap_array *C into a MREP
	MREP *dest = ktree_from_bitmap_array(C);
	//printf("Árbol generado\n\n");
	
	bitmap_array_destroy(C);
	//printf("bitmap_array_destroy() finalizado\n\n");
	bitmap_ptr_array_destroy(pA);
	bitmap_ptr_array_destroy(pB);
	
	return dest; 
}

