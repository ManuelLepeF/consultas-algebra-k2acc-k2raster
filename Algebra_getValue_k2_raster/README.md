Consultas del álgebra de mapa sobre la estructura de datos compacta k<sup>2</sup>-raster usando método get_cell.
=========================

## Sitios Web oficiales de k<sup>2</sup>-raster ##

* [Official Web Site](https://lbd.udc.es/research/k2-raster/)
* [Code](https://gitlab.lbd.org.es/fsilva/k2-raster)

## Contribución

Las consultas del álgebra de mapa sobre esta estructura se implementa en este [archivo](src/algebra_GetValue.cpp), además se agrega los Input, Output y algunos scripts para ejecutar experimentos, todo lo demás lo implementó el autor de k<sup>2</sup>-raster.

# Descargar raster utilizados 

Link para descargar Input y Output (estructuras creadas en k2-raster) utilizados en los experimentos con esta estructura: [Descargar](https://drive.google.com/drive/folders/1EDU3iGDU5Ni4rHUcrb6e9ziBJveYxoFw?usp=sharing)
