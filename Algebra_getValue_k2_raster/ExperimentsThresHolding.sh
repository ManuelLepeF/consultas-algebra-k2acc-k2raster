mkdir ExperimentsResults/ThresHolding

echo "------------------Experimentando con SantiagoMediano----------"
mkdir ExperimentsResults/ThresHolding/SantiagoMediano

echo "Experimentando con SantiagoMediano_9.................."
mkdir ExperimentsResults/ThresHolding/SantiagoMediano/9
for ej in 1 2 3
do
echo "SantiagoMediano_9 ejecucion ${ej}"
/usr/bin/time -v  ./build/bin/algebra_GetValue Output/SantiagoMediano/SantiagoMediano_9/SantiagoMediano_9.k2r 1 6  &> ExperimentsResults/ThresHolding/SantiagoMediano/9/GetValue_${ej}.txt
done

echo "Experimentando con SantiagoMediano_7..............."
mkdir ExperimentsResults/ThresHolding/SantiagoMediano/7
for ej in 1 2 3
do
echo "SantiagoMediano_7 ejecucion ${ej}"
/usr/bin/time -v  ./build/bin/algebra_GetValue Output/SantiagoMediano/SantiagoMediano_7/SantiagoMediano_7.k2r 1 26  &> ExperimentsResults/ThresHolding/SantiagoMediano/7/GetValue_${ej}.txt
done

echo "Experimentando con SantiagoMediano_6..............."
mkdir ExperimentsResults/ThresHolding/SantiagoMediano/6
for ej in 1 2 3
do
echo "SantiagoMediano_6 ejecucion ${ej}"
/usr/bin/time -v  ./build/bin/algebra_GetValue Output/SantiagoMediano/SantiagoMediano_6/SantiagoMediano_6.k2r 1 52  &> ExperimentsResults/ThresHolding/SantiagoMediano/6/GetValue_${ej}.txt
done

echo "Experimentando con SantiagoMediano_5..............."
mkdir ExperimentsResults/ThresHolding/SantiagoMediano/5
for ej in 1 2 3
do
echo "SantiagoMediano_5 ejecucion ${ej}"
/usr/bin/time -v  ./build/bin/algebra_GetValue Output/SantiagoMediano/SantiagoMediano_5/SantiagoMediano_5.k2r 1 105  &> ExperimentsResults/ThresHolding/SantiagoMediano/5/GetValue_${ej}.txt
done

echo "Experimentando con SantiagoMediano_4..............."
mkdir ExperimentsResults/ThresHolding/SantiagoMediano/4
for ej in 1 2 3
do
echo "SantiagoMediano_4 ejecucion ${ej}"
/usr/bin/time -v  ./build/bin/algebra_GetValue Output/SantiagoMediano/SantiagoMediano_4/SantiagoMediano_4.k2r 1 211  &> ExperimentsResults/ThresHolding/SantiagoMediano/4/GetValue_${ej}.txt
done

echo "Experimentando con SantiagoMediano_2..............."
mkdir ExperimentsResults/ThresHolding/SantiagoMediano/2
for ej in 1 2 3
do
echo "SantiagoMediano_2 ejecucion ${ej}"
/usr/bin/time -v  ./build/bin/algebra_GetValue Output/SantiagoMediano/SantiagoMediano_2/SantiagoMediano_2.k2r 1 846  &> ExperimentsResults/ThresHolding/SantiagoMediano/2/GetValue_${ej}.txt
done


################################################################################################################################################

echo "------------------Experimentando con SantiagoGrande----------"
mkdir ExperimentsResults/ThresHolding/SantiagoGrande

echo "Experimentando con SantiagoMediano_9..............."
mkdir ExperimentsResults/ThresHolding/SantiagoGrande/9
for ej in 1 2 3
do
echo "SantiagoGrande_9 ejecucion ${ej}"
/usr/bin/time -v  ./build/bin/algebra_GetValue Output/SantiagoGrande/SantiagoGrande_9/SantiagoGrande_9.k2r 1 7  &> ExperimentsResults/ThresHolding/SantiagoGrande/9/GetValue_${ej}.txt
done

echo "Experimentando con SantiagoMediano_7..............."
mkdir ExperimentsResults/ThresHolding/SantiagoGrande/7
for ej in 1 2 3
do
echo "SantiagoGrande_7 ejecucion ${ej}"
/usr/bin/time -v  ./build/bin/algebra_GetValue Output/SantiagoGrande/SantiagoGrande_7/SantiagoGrande_7.k2r 1 27  &> ExperimentsResults/ThresHolding/SantiagoGrande/7/GetValue_${ej}.txt
done

echo "Experimentando con SantiagoMediano_6..............."
mkdir ExperimentsResults/ThresHolding/SantiagoGrande/6
for ej in 1 2 3
do
echo "SantiagoGrande_6 ejecucion ${ej}"
/usr/bin/time -v  ./build/bin/algebra_GetValue Output/SantiagoGrande/SantiagoGrande_6/SantiagoGrande_6.k2r 1 54  &> ExperimentsResults/ThresHolding/SantiagoGrande/6/GetValue_${ej}.txt
done

echo "Experimentando con SantiagoMediano_5..............."
mkdir ExperimentsResults/ThresHolding/SantiagoGrande/5
for ej in 1 2 3
do
echo "SantiagoGrande_5 ejecucion ${ej}"
/usr/bin/time -v  ./build/bin/algebra_GetValue Output/SantiagoGrande/SantiagoGrande_5/SantiagoGrande_5.k2r 1 109  &> ExperimentsResults/ThresHolding/SantiagoGrande/5/GetValue_${ej}.txt
done

echo "Experimentando con SantiagoMediano_4..............."
mkdir ExperimentsResults/ThresHolding/SantiagoGrande/4
for ej in 1 2 3
do
echo "SantiagoGrande_4 ejecucion ${ej}"
/usr/bin/time -v  ./build/bin/algebra_GetValue Output/SantiagoGrande/SantiagoGrande_4/SantiagoGrande_4.k2r 1 218  &> ExperimentsResults/ThresHolding/SantiagoGrande/4/GetValue_${ej}.txt
done

echo "Experimentando con SantiagoMediano_3..............."
mkdir ExperimentsResults/ThresHolding/SantiagoGrande/3
for ej in 1 2 3
do
echo "SantiagoGrande_3 ejecucion ${ej}"
/usr/bin/time -v  ./build/bin/algebra_GetValue Output/SantiagoGrande/SantiagoGrande_3/SantiagoGrande_3.k2r 1 436  &> ExperimentsResults/ThresHolding/SantiagoGrande/3/GetValue_${ej}.txt
done

echo "Experimentando con MDT500..............."
mkdir ExperimentsResults/ThresHolding/MDT500/
for ej in 1 2 3
do
/usr/bin/time -v ./build/bin/algebra_GetValue Output/mdt500/mdt500.k2r 1 436 &> ExperimentsResults/ThresHolding/MDT500/GetValue_${ej}.txt
done


