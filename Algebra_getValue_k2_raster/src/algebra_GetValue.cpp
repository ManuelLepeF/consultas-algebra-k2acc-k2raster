#include <k2_raster.hpp>
#include <utils/query/query.hpp>
#include <utils/utils_time.hpp>
#include <k2_raster_heuristic.hpp>
struct timeval t_ini, t_fin, t_fin_sin_Crear;

#define NREPS 1

void print_help(char * argv0) {
    printf("Usage: %s <k2_raster_file_One> <option algebra> [<k2_raster_file_two> || <value ThresHolding>] \n", argv0);
}

/* retorna "a - b" en segundos */
double timeval_diff(struct timeval *a, struct timeval *b){
  return
    (double)(a->tv_sec + (double)a->tv_usec/1000000) -
    (double)(b->tv_sec + (double)b->tv_usec/1000000);
}

template<typename k2_raster_type>
void printK2r(k2_raster_type k2r){

    for (int i = 0; i < k2r.get_n_rows(); i++){
        for (int j = 0; j < k2r.get_n_cols(); j++){
            std::cout << k2r.get_cell(i,j) << " ";
        }
        std::cout << "\n";
    }
}


template<typename k2_raster_type>
void ThresHoldingGetValue(k2_raster_type sp, int value){
	int nRows = sp.get_n_rows();
    int nCols = sp.get_n_cols();

    std::vector<int> values(nRows * nCols);
    size_t n = 0;
    for (int i = 0; i < nRows; i++){
        for (int j = 0; j < nCols; j++){
            if(sp.get_cell(i,j)< value){
                values[n] = 0;
                n++;
            }else{
                values[n] = 1;
                n++;
            }
        }
    }

    gettimeofday(&t_fin_sin_Crear, NULL);
    k2_raster_type k2rOut(values, nRows, nCols, 2, 2, 0, 0);
    //printK2r<k2_raster_type>(k2rOut);

}

template<typename k2_raster_type>
void SumScaleGetValue(k2_raster_type sp, int value){
	int nRows = sp.get_n_rows();
    int nCols = sp.get_n_cols();

    std::vector<int> values(nRows * nCols);
    size_t n = 0;
    for (int i = 0; i < nRows; i++){
        for (int j = 0; j < nCols; j++){
            values[n] = sp.get_cell(i,j)+value;
            n++;
        }
    }

    gettimeofday(&t_fin_sin_Crear, NULL);
    k2_raster_type k2rOut(values, nRows, nCols, 2, 2, 0, 0);
    //printK2r<k2_raster_type>(k2rOut);

}

template<typename k2_raster_type>
void SumPointPointGetValue(k2_raster_type k2rOne, k2_raster_type k2rTwo){
	int nRows = k2rOne.get_n_rows();
    int nCols = k2rOne.get_n_cols();

    std::vector<int> values(nRows * nCols);
    size_t n = 0;
    for (int i = 0; i < nRows; i++){
        for (int j = 0; j < nCols; j++){
            values[n] = k2rOne.get_cell(i,j)+k2rTwo.get_cell(i,j);
            n++;
        }
    }

    gettimeofday(&t_fin_sin_Crear, NULL);
    k2_raster_type k2rOut(values, nRows, nCols, 2, 2, 0, 0);
    //printK2r<k2_raster_type>(k2rOut);

}

template<typename k2_raster_type>
void ZonalSumGetValue(k2_raster_type k2rOne, k2_raster_type k2rTwo){
	int nRows = k2rOne.get_n_rows();
    int nCols = k2rOne.get_n_cols();

    // VECTOR TEMPORAL PARA GUARDAR VALORES FINALES
    std::vector<int> valoresSuma(k2rTwo.max_value);
    for (size_t i = 0; i < k2rTwo.max_value ; i++)
    {
        valoresSuma[i] = 0;
    }
    
    // SUMA LOS VALORES Y LO GUARDA EN EL VECTOR
    int ** matrizAux = (int **)malloc (nRows *sizeof(int *));
    for (int i = 0; i < nRows; i++){
        matrizAux[i] = (int *)malloc (nCols *sizeof(int));
        for (int j = 0; j < nCols; j++){
            int position = k2rTwo.get_cell(i,j);
            matrizAux[i][j] = position;
            if (position!=0){
                valoresSuma[position] += k2rOne.get_cell(i,j);
            }
            
        }
    }

    // CREA LA MATRIZ FINAL
    std::vector<int> values(nRows * nCols);
    size_t n = 0;
    for (int x = 0; x < nRows; x++){
        for (int y = 0; y < nCols; y++){
            values[n] = valoresSuma[matrizAux[x][y]];
            n++;
        }
        free(matrizAux[x]);
    }
    free(matrizAux);
    gettimeofday(&t_fin_sin_Crear, NULL);
    
    // CREA EL K2R DE SALIDA
    k2_raster_type k2rOut(values, nRows, nCols, 2, 2, 0, 0);
    //printK2r<k2_raster_type>(k2rOut);

}


int main(int argc, char **argv) {
    
    //********************//
    // VERIFICA ENTRADA  //
    //******************//

    if (argc < 3) {
        print_help(argv[0]);
        exit(-1);
    }else{
        if (atoi(argv[2])== 2){
            if (argc != 3){
                print_help(argv[0]);
                exit(-1);
            }
        }else{
            if (argc != 4){
                print_help(argv[0]);
                exit(-1);
            }
        }
    }

    //**************************************//
    // VERIFICA TIPO DE LA ESTRUCTURA K2R  //
    //************************************//
    std::ifstream k2raster_file(argv[1]);
    assert(k2raster_file.is_open() && k2raster_file.good());
    ushort k2_raster_type;
    sdsl::read_member(k2_raster_type, k2raster_file);
    k2raster_file.close();
    if(k2_raster_type!=k2raster::K2_RASTER_TYPE){
        printf("La primera estructura k2r debe ser del tipo standard");
        exit(-1);
    }
    if (atoi(argv[2]) == 3 || atoi(argv[2]) == 4){
        std::ifstream k2raster_file(argv[3]);
        assert(k2raster_file.is_open() && k2raster_file.good());
        ushort k2_raster_type;
        sdsl::read_member(k2_raster_type, k2raster_file);
        k2raster_file.close();
        if(k2_raster_type!=k2raster::K2_RASTER_TYPE){
            printf("La segunda estructura k2r debe ser del tipo standard");
            exit(-1);
        }
    }
    

    //**********************************//
    // EJECUTA LA CONSULTA DEL ALGEBRA //
    //********************************//
    
    switch (atoi(argv[2])) {
        case 1:
        {
            printf("Probando ThresHolding...\n");
            k2raster::k2_raster<> spOne;
            sdsl::load_from_file(spOne, argv[1]);
            double t = 0, t_sin=0;
            gettimeofday(&t_ini, NULL);
            ThresHoldingGetValue<k2raster::k2_raster<>>(spOne,atoi(argv[3]));
            gettimeofday(&t_fin, NULL);
            t_sin = timeval_diff(&t_fin_sin_Crear, &t_ini);
            t_sin *=1000;
            t = timeval_diff(&t_fin, &t_ini);
            t *= 1000; // to milliseconds
            printf("La ejecución COMPLETA termino en: %f\n",t);
            printf("La ejecución SIN CREAR termino en: %f\n",t_sin);
            break;
        }
        case 2:
        { 
            printf("Probando Suma por un escalar...\n");
            k2raster::k2_raster<> spOne;
            sdsl::load_from_file(spOne, argv[1]);
            double t = 0, t_sin=0;
            gettimeofday(&t_ini, NULL);
            SumScaleGetValue<k2raster::k2_raster<>>(spOne,20);
            gettimeofday(&t_fin, NULL);
            t_sin = timeval_diff(&t_fin_sin_Crear, &t_ini);
            t_sin *=1000;
            t = timeval_diff(&t_fin, &t_ini);
            t *= 1000; // to milliseconds
            printf("La ejecución COMPLETA termino en: %f\n",t);
            printf("La ejecución SIN CREAR termino en: %f\n",t_sin);
            break;
        }
        case 3:
        {
            printf("Probando Suma punto a punto...\n");
            k2raster::k2_raster<> k2rOne;
            sdsl::load_from_file(k2rOne, argv[1]);
            k2raster::k2_raster<> k2rTwo;
            sdsl::load_from_file(k2rTwo, argv[3]);
            if (k2rOne.get_n_rows()!= k2rTwo.get_n_rows() ||  k2rOne.get_n_cols()!= k2rTwo.get_n_cols()){
                printf("Las matrices deben tener el mismo tamaño");
                exit(-1);
            }
            
            double t = 0, t_sin=0;
            gettimeofday(&t_ini, NULL);
            SumPointPointGetValue<k2raster::k2_raster<>>(k2rOne,k2rTwo);
            gettimeofday(&t_fin, NULL);
            t_sin = timeval_diff(&t_fin_sin_Crear, &t_ini);
            t_sin *=1000;
            t = timeval_diff(&t_fin, &t_ini);
            t *= 1000; // to milliseconds
            printf("La ejecución COMPLETA termino en: %f\n",t);
            printf("La ejecución SIN CREAR termino en: %f\n",t_sin);
            break;
        }
        case 4:
        {
            printf("Probando Suma Zonal...\n");
            k2raster::k2_raster<> k2r;
            sdsl::load_from_file(k2r, argv[1]);
            k2raster::k2_raster<> k2rZonas;
            sdsl::load_from_file(k2rZonas, argv[3]);
            if (k2r.get_n_rows()!= k2rZonas.get_n_rows() ||  k2r.get_n_cols()!= k2rZonas.get_n_cols()){
                printf("Las matrices deben tener el mismo tamaño");
                exit(-1);
            }
            double t = 0, t_sin=0;
            gettimeofday(&t_ini, NULL);
            ZonalSumGetValue<k2raster::k2_raster<>>(k2r,k2rZonas);
            gettimeofday(&t_fin, NULL);
            t_sin = timeval_diff(&t_fin_sin_Crear, &t_ini);
            t_sin *=1000;
            t = timeval_diff(&t_fin, &t_ini);
            t *= 1000; // to milliseconds
            printf("La ejecución COMPLETA termino en: %f\n",t);
            printf("La ejecución SIN CREAR termino en: %f\n",t_sin);
            break;
        }
        default:
            std::cout << "Consulta del algebra invalida " << std::endl;
            exit(-1);
    }
}

