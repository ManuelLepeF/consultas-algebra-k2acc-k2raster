#!/bin/bash
echo "Probando consulta del algebra sum zonal con los raster artificiales"
mkdir ExperimentsResults/SumZonal


echo "------Experimentando con SantiagoChico_4 264 valores diferentes------------------------"
mkdir ExperimentsResults/SumZonal/SantiagoChico_4
for Raster_2 in 10 50 100 200 500
do
    echo "Experimentando con SantiagoChico_4 raster 2: ${Raster_2}............."
    mkdir ExperimentsResults/SumZonal/SantiagoChico_4/${Raster_2} 
    for ej in 1 2 3
    do
        echo "Ejecución ${ej}"
        /usr/bin/time -v ./build/bin/algebra_GetValue Output/SantiagoChico/SantiagoChico_4/SantiagoChico_4.k2r 4 Output/SantiagoChicoZonas/${Raster_2}/${Raster_2}.k2r &> ExperimentsResults/SumZonal/SantiagoChico_4/${Raster_2}/GetValue_${ej}.txt
    done
done

echo "--------- Probando real mdt500 y ráster Random------------------------"
mkdir ExperimentsResults/SumZonal/mdt500
for Raster_2 in 10 50 100 200 500
do
    echo "Experimentando MDT500 con raster 2: ${Raster_2}-----"
    mkdir ExperimentsResults/SumZonal/mdt500/${Raster_2} 
    for ej in 1 2 3
    do
        echo "Ejecución ${ej}"
        /usr/bin/time -v ./build/bin/algebra_GetValue Output/mdt500/mdt500.k2r 4 Output/Zonal_mdt500/${Raster_2}/mdt500_${Raster_2}.k2r &> ExperimentsResults/SumZonal/mdt500/${Raster_2}/GetValue_${ej}.txt
    done
done