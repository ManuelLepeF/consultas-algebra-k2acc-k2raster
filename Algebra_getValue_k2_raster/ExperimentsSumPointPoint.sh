mkdir ExperimentsResults/SumPointPoint
echo "----------------------------Experimentando con SantiagoMediano------------------------"
mkdir ExperimentsResults/SumPointPoint/SantiagoMediano
for Raster_1 in 9 7 6   
do
    for Raster_2 in 9 7 6 5 4 2
    do
        if [ $Raster_2 -le $Raster_1 ] 
        then
            echo "-----Experimentando con SantiagoMediano ${Raster_1}x${Raster_2}-----"
            mkdir ExperimentsResults/SumPointPoint/SantiagoMediano/${Raster_1}x${Raster_2} 
            for ej in 1 2 3
            do
                echo "Ejecución ${ej}"
                /usr/bin/time -v ./build/bin/algebra_GetValue Output/SantiagoMediano/SantiagoMediano_${Raster_1}/SantiagoMediano_${Raster_1}.k2r 3 Output/SantiagoMediano/SantiagoMediano_${Raster_2}/SantiagoMediano_${Raster_2}.k2r &> ExperimentsResults/SumPointPoint/SantiagoMediano/${Raster_1}x${Raster_2}/GetValue_${ej}.txt
            done
        fi
    done
done

echo "----------------------------Experimentando con SantiagoMediano------------------------"
mkdir ExperimentsResults/SumPointPoint/SantiagoMedianoValdivia
for Raster_1 in 9 7 6   
do
    for Raster_2 in 9 5 3 2
    do
        echo "-----Experimentando con SantiagoMediano ${Raster_1}x${Raster_2}-----"
        mkdir ExperimentsResults/SumPointPoint/SantiagoMedianoValdivia/${Raster_1}x${Raster_2} 
        for ej in 1 2 3
        do
            echo "Ejecución ${ej}"
            /usr/bin/time -v ./build/bin/algebra_GetValue Output/SantiagoMediano/SantiagoMediano_${Raster_1}/SantiagoMediano_${Raster_1}.k2r 3 Output/ValdiviaMediano/ValdiviaMediano_${Raster_2}/ValdiviaMediano_${Raster_2}.k2r &> ExperimentsResults/SumPointPoint/SantiagoMedianoValdivia/${Raster_1}x${Raster_2}/GetValue_${ej}.txt
        done
    done
done


echo "----------------------------Experimentando MDT500 con SantiagoChicoMDT500 ------------------------"
mkdir ExperimentsResults/SumPointPoint/mdt500SantiagoMdt500
for valores in 9 6 5 4 3 2
do
    echo "------- Santiago chico mdt500 ${valores} -------------"
    mkdir ExperimentsResults/SumPointPoint/mdt500SantiagoMdt500/${valores}
    for ej in 1 2 3 
    do
        echo "Ejecución: ${ej}.."
        /usr/bin/time -v ./build/bin/algebra_GetValue Output/mdt500/mdt500.k2r 3 Output/SantiagoChicoMDT/SantiagoChicoMDT_${valores}/SantiagoChicoMDT_${valores}.k2r &> ExperimentsResults/SumPointPoint/mdt500SantiagoMdt500/${valores}/GetValue_${ej}.txt
    done 
done