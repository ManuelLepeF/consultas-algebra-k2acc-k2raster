#!/bin/bash
echo "Probando consulta del algebra sum zonal con los raster artificiales"
nombreRaster='mdt500'
corrimiento='4'
#mkdir ExperimentsResults/SumZonal

mkdir ExperimentsResults/SumZonal/${nombreRaster}_${corrimiento}
echo "Experimentando con ${nombreRaster}_${corrimiento}........."

for zonas in 10 50 100 200 500
do
    echo "${nombreRaster}_${corrimiento} con ${zonas} Zonas...."
    mkdir ExperimentsResults/SumZonal/${nombreRaster}_${corrimiento}/${zonas} 
    for ej in 1 2 3
    do
        echo "Ejecución ${ej}"
        /usr/bin/time -v ./build/bin/algebra_GetValue Output/${nombreRaster}_${corrimiento}/${nombreRaster}_${corrimiento}.k2r 4 Output/Zonal_mdt500/${zonas}/${nombreRaster}_${zonas}.k2r &> ExperimentsResults/SumZonal/${nombreRaster}_${corrimiento}/${zonas}/GetValue_${ej}.txt
        
    done
done
