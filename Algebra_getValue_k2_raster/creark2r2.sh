#!/bin/bash

echo "-------------Creando mdt500...--------------"
mkdir Output2/mdt500
./build/bin/encode_k2r Input/mdt500/mdt500.bin 4001 5841 Output2/mdt500/mdt500.k2r 1 1 2 &> Output2/mdt500/mdt500.txt

echo "-------------Creando k2r de Santiago Mediano...--------------"
mkdir Output2/SantiagoMediano
for valores in 9 8 7 6 5 4 3 2 1 0
do
    echo "SantiagoMediano_${valores}..."
    mkdir Output2/SantiagoMediano/SantiagoMediano_${valores}
    ./build/bin/encode_k2r Input/SantiagoMediano/SantiagoMediano_${valores}/SantiagoMediano_${valores}.bin 10603 11399 Output2/SantiagoMediano/SantiagoMediano_${valores}/SantiagoMediano_${valores}.k2r 1 1 2 &> Output2/SantiagoMediano/SantiagoMediano_${valores}/SantiagoMediano_${valores}.txt
done

echo "------------Creando k2r de Santiago Grande...--------------"
mkdir Output2/SantiagoGrande
for valores in 9 8 7 6 5 4 3 2 1 0
do
    echo "SantiagoGrande_${valores}..."
    mkdir Output2/SantiagoGrande/SantiagoGrande_${valores}
    ./build/bin/encode_k2r Input/SantiagoGrande/SantiagoGrande_${valores}/SantiagoGrande_${valores}.bin 26375 28332 Output2/SantiagoGrande/SantiagoGrande_${valores}/SantiagoGrande_${valores}.k2r 1 1 2 &> Output2/SantiagoGrande/SantiagoGrande_${valores}/SantiagoGrande_${valores}.txt
done

echo "Creando k2r de Vandivia Mediano..."
mkdir Output2/ValdiviaMediano
for valores in 9 8 7 6 5 4 3 2 1 0
do
    echo "ValdiviaMediano_${valores}..."
    mkdir Output2/ValdiviaMediano/ValdiviaMediano_${valores}
    ./build/bin/encode_k2r Input/ValdiviaMediano/ValdiviaMediano_${valores}/ValdiviaMediano_${valores}.bin 10603 11399 Output2/ValdiviaMediano/ValdiviaMediano_${valores}/ValdiviaMediano_${valores}.k2r 1 1 2 &> Output2/ValdiviaMediano/ValdiviaMediano_${valores}/ValdiviaMediano_${valores}.txt
done

echo "Creando k2r de Vandivia Grande..."
mkdir Output2/ValdiviaGrande
for valores in 9 8 7 6 5 4 3 2 1 0
do
    echo "ValdiviaGrande_${valores}..."
    mkdir Output2/ValdiviaGrande/ValdiviaGrande_${valores}
    ./build/bin/encode_k2r Input/ValdiviaGrande/ValdiviaGrande_${valores}/ValdiviaGrande_${valores}.bin 26375 28332 Output2/ValdiviaGrande/ValdiviaGrande_${valores}/ValdiviaGrande_${valores}.k2r 1 1 2 &> Output2/ValdiviaGrande/ValdiviaGrande_${valores}/ValdiviaGrande_${valores}.txt
done

echo "Creando k2r de SantiagoChicoMDT..."
mkdir Output2/SantiagoChicoMDT
for valores in 9 6 5 4 3 2
do
    echo "SantiagoChicoMDT_${valores}..."
    mkdir Output2/SantiagoChicoMDT/SantiagoChicoMDT_${valores}
    ./build/bin/encode_k2r Input/SantiagoChicoMDT/SantiagoChicoMDT_${valores}/SantiagoChicoMDT_${valores}.bin 4001 5841 Output2/SantiagoChicoMDT/SantiagoChicoMDT_${valores}/SantiagoChicoMDT_${valores}.k2r 1 1 2 &> Output2/SantiagoChicoMDT/SantiagoChicoMDT_${valores}/SantiagoChicoMDT_${valores}.txt
done

echo "Creando k2r de Zonal mdt..."
mkdir Output2/Zonal_mdt500
for valores in 10 50 100 200 500
do
    echo "Zonal_mdt500_${valores}..."
    mkdir Output2/Zonal_mdt500/${valores}
    ./build/bin/encode_k2r Input/Zonal_mdt500/${valores}/mdt500_${valores}.bin 4001 5841 Output2/Zonal_mdt500/${valores}/mdt500_${valores}.k2r 1 1 2 &> Output2/Zonal_mdt500/${valores}/mdt500_${valores}.txt
done

echo "Creando k2r de Santiago Chico Zonas..."
mkdir Output2/SantiagoChicoZonas
for valores in 10 50 100 200 500
do
    echo "SantiagoChicoZonas_${valores}..."
    mkdir Output2/SantiagoChicoZonas/${valores}
    ./build/bin/encode_k2r Input/SantiagoChicoZonas/${valores}/${valores}.bin 5964 6819 Output2/SantiagoChicoZonas/${valores}/${valores}.k2r 1 1 2 &> Output2/SantiagoChicoZonas/${valores}/${valores}.txt
done

echo "Creando k2r de Santiago Chico 264 valores distintos"
mkdir Output2/SantiagoChico/
mkdir Output2/SantiagoChico/SantiagoChico_4
./build/bin/encode_k2r Input/SantiagoChico/SantiagoChico_4/SantiagoChico_4.bin 5964 6819 Output2/SantiagoChico/SantiagoChico_4/SantiagoChico_4.k2r 1 1 2 &> Output2/SantiagoChico/SantiagoChico_4/SantiagoChico_4.txt
