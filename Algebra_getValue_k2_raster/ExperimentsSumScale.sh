mkdir ExperimentsResults/SumScale

echo "------------------Experimentando con SantiagoMediano----------"
mkdir ExperimentsResults/SumScale/SantiagoMediano

for valores in 9 7 6 5 4 2
do
    echo "Experimentando con SantiagoMediano_${valores}..........."
    mkdir ExperimentsResults/SumScale/SantiagoMediano/${valores}
    for ej in 1 2 3
    do
        echo "SantiagoMediano_${valores} ejecucion ${ej}"
        /usr/bin/time -v ./build/bin/algebra_GetValue Output/SantiagoMediano/SantiagoMediano_${valores}/SantiagoMediano_${valores}.k2r 2  &> ExperimentsResults/SumScale/SantiagoMediano/${valores}/GetValue_${ej}.txt
    done
done

echo "------------------Experimentando con SantiagoGrande----------"
mkdir ExperimentsResults/SumScale/SantiagoGrande
for valores in 9 7 6 5 4 3
do
    echo "Experimentando con SantiagoGrande_${valores}..........."
    mkdir ExperimentsResults/SumScale/SantiagoGrande/${valores}
    for ej in 1 2 3
    do
        echo "SantiagoGrande_${valores} ejecucion ${ej}"
        /usr/bin/time -v ./build/bin/algebra_GetValue Output/SantiagoGrande/SantiagoGrande_${valores}/SantiagoGrande_${valores}.k2r 2  &> ExperimentsResults/SumScale/SantiagoGrande/${valores}/GetValue_${ej}.txt
    done
done

echo "------------------Experimentando con MDT500-----------------"
mkdir ExperimentsResults/SumScale/MDT500/
for ej in 1 2 3
do
/usr/bin/time -v ./build/bin/algebra_GetValue Output/mdt500/mdt500.k2r 2 &> ExperimentsResults/SumScale/MDT500/GetValue_${ej}.txt
done

